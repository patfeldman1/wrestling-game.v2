// `.env.json` is generated by the `npm run build` command
// import * as env from './.env.json';

export const environment = {
  production: true,
  serverUrl: 'techfalling-2018.firebaseapp.com',
  defaultLanguage: 'en-US',
  supportedLanguages: [
    'en-US',
    'fr-FR'
  ],
  firebase: {
    apiKey: 'AIzaSyAvx4k242U1_XgWDwi9wi3nEgMc51lzBD0',
    authDomain: 'techfalling-2018.firebaseapp.com',
    databaseURL: 'https://techfalling-2018.firebaseio.com',
    projectId: 'techfalling-2018',
    storageBucket: 'techfalling-2018.appspot.com',
    messagingSenderId: '107647807780'
  }
};
