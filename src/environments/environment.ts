// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  version: '(dev)',
  serverUrl: '/api',
  defaultLanguage: 'en-US',
  supportedLanguages: [
    'en-US',
    'fr-FR'
  ],
  firebase: {
    apiKey: 'AIzaSyAvx4k242U1_XgWDwi9wi3nEgMc51lzBD0',
    authDomain: 'techfalling-2018.firebaseapp.com',
    databaseURL: 'https://techfalling-2018.firebaseio.com',
    projectId: 'techfalling-2018',
    storageBucket: 'techfalling-2018.appspot.com',
    messagingSenderId: '107647807780'
  }
};
