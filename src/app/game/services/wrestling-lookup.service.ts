import { TeamInfoById } from './../models/team-results.model';
import { PageBracket } from './../models/page-brackets.model';
import { browser } from 'protractor';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { map, catchError } from 'rxjs/operators';
import { AllBracketInfo, Bracket } from '../models/brackets.model';

import { convertBracketToPageBracket } from '../helper/bracket-creator.helper';
import { WrestlerHistory } from '../models/wrestler-history.model';
import { AllResultInfo } from '../models/results.model';
import { AllParticipantInfo } from '../models/participants.model';
import { AllTeamResultInfo, TeamFinishByYear } from '../models/team-results.model';

import * as brackets from '../../../data/brackets.json';
import * as participants from '../../../data/participants.json';
import * as results from '../../../data/results.json';
import * as teamResults from '../../../data/teams.json';
import { WrestlerInfo } from '../models/wrestler-info.model';

@Injectable()
export class WrestlingLookupService {
    allBrackets: AllBracketInfo;
    allResults: AllResultInfo;
    allParticipants: AllParticipantInfo;
    allTeams: AllTeamResultInfo;
    constructor() {
        this.allBrackets = brackets;
        this.allParticipants = participants;
        this.allResults = results;
        this.allTeams = teamResults;
    }
    getBracket(weight: string, year: string = '2018' ): PageBracket {
        return convertBracketToPageBracket(this.allBrackets[year][weight]);
    }
    getTeamHistory(gtid: string): TeamInfoById {
        return this.allTeams.team_info[gtid];
    }
    getTopTenTeams(year: string): TeamFinishByYear[] {
        return this.allTeams.team_results[year].slice(0, 10);
    }

    getCurrentScore(id: string, year: string = '2018'): number {
        if (this.allResults[id] === undefined) { return 0; }
        if (this.allResults[id].years[year] === undefined) { return 0; }
        const resultsInfo = this.allResults[id].years[year].points;
        return resultsInfo.total_points;
    }

    getWrestlerInfo(id: string, year: string = '2018'): WrestlerInfo {
        const participantInfo = this.allParticipants[id];
        const retVal = this.allParticipants[id][year];
        retVal.first_name = participantInfo.first_name;
        retVal.last_name = participantInfo.last_name;
        return retVal;
    }

    getWrestlerHistory(id: string): WrestlerHistory {
        const participantInfo = this.allParticipants[id];
        const resultsInfo = this.allResults[id];
        const participantYears = {};

        for (const key of Object.keys(participantInfo)) {
            if (isNaN(key as any)) { continue; }
            let yearInfo = {};
            if (!!resultsInfo && key in resultsInfo.years) {
                yearInfo = {
                    info: participantInfo[key],
                    matches: resultsInfo.years[key].matches,
                    points: resultsInfo.years[key].points,
                    placement: resultsInfo.years[key].placement
                };
            } else {
                yearInfo = {
                    info: participantInfo[key],
                    matches: [],
                    points: [],
                    placement: []
                };
            }
            participantYears[key] = yearInfo;
        }
        // if (!!resultsInfo) {
        //     for (const key of Object.keys(resultsInfo.years)) {
        //         const yearInfo = {
        //             info: participantInfo[key],
        //             matches: resultsInfo.years[key].matches,
        //             points: resultsInfo.years[key].points,
        //             placement: resultsInfo.years[key].placement
        //         };
        //         participantYears[key] = yearInfo;
        //         numYears++;
        //     }
        // }
        // if (numYears === 0) {
        //     const year = Object.keys(participantInfo)[0];
        //     const yearInfo = {
        //         info: participantInfo[year],
        //         matches: {},
        //         points: {},
        //         placement: {}
        //     };
        //     participantYears[year] = yearInfo;
        // }
        return {
            first_name: participantInfo.first_name,
            last_name: participantInfo.last_name,
            years: participantYears
        };
    }

    getAllWrestlersAtWeight(weight: string, year: string = '2018'): dictionary<WrestlerInfo> {
        return this.allBrackets[year][weight]['seeds'];
    }
}
