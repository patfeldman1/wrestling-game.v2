import { Bracket } from './../models/brackets.model';
import { Match, PageBracket, PageRound } from '../models/page-brackets.model';
import { DEFAULT_PACKAGE_URL_PROVIDER } from '@angular/platform-browser-dynamic/src/compiler_factory';
import { WrestlerInfo } from '../models/wrestler-info.model';
const CHAMPIONSHIP_MATCH_ROUND = 4;

export const matchups = {
    1: [1, 32],
    2: [17, 16],
    3: [9, 24],
    4: [25, 8],
    5: [5, 28],
    6: [21, 12],
    7: [13, 20],
    8: [29, 4],
    9: [3, 30],
    10: [19, 14],
    11: [11, 22],
    12: [27, 6],
    13: [7, 26],
    14: [23, 10],
    15: [15, 18],
    16: [31, 2]
};


export function convertBracketToPageBracket(bracket: Bracket): PageBracket {

    function getFinalsMatchups(champRounds: PageRound[], consoRounds: PageRound[]): Match[] {

        // FINALS
        const finals: Match[] = [];

        finals.push(champRounds[4].matches[0]);
        finals.push(consoRounds[6].matches[0]);

        // 5/6 matchup
        let topWrestler = (consoRounds[5].matches[0].winner === 'top')
            ? consoRounds[5].matches[0].bottomWrestler : consoRounds[5].matches[0].topWrestler;

        let bottomWrestler = (consoRounds[5].matches[1].winner === 'top')
            ? consoRounds[5].matches[1].bottomWrestler : consoRounds[5].matches[1].topWrestler;

        let winner = 'unknown';
        if (!!topWrestler) {
            winner = (topWrestler.bracket.placement_win > 0) ? 'top' : 'bottom';
        }
        finals.push({
            topWrestler: topWrestler,
            bottomWrestler: bottomWrestler,
            winner: winner,
            score: ''
        });

        // 7/8 matchup
        topWrestler = (consoRounds[4].matches[0].winner === 'top')
            ? consoRounds[4].matches[0].bottomWrestler : consoRounds[4].matches[0].topWrestler;

        bottomWrestler = (consoRounds[4].matches[1].winner === 'top')
            ? consoRounds[4].matches[1].bottomWrestler : consoRounds[4].matches[1].topWrestler;

        winner = 'unknown';
        if (!!topWrestler) {
            winner = (topWrestler.bracket.placement_win > 0) ? 'top' : 'bottom';
        }
        finals.push({
            topWrestler: topWrestler,
            bottomWrestler: bottomWrestler,
            winner: winner,
            score: ''
        });


        return finals;
    }

    function getConsoRounds(
        champRounds: PageRound[],
        prelim_at_seed: number,
        prelim_against: WrestlerInfo
    ): PageRound[] {
        // Get round1 conso
        const consoRounds: PageRound[] = [];
        let nextConsoMatches: Match[] = [];
        const round1 = champRounds[0].matches;
        for (let i = 0; i < round1.length; i = i + 2) {
            let loserWrestlerTop = null;
            let topProjectedConsoRound = 0;
            if (round1[1].winner !== 'unknown') {
                loserWrestlerTop = round1[i].winner === 'top'
                    ? round1[i].bottomWrestler : round1[i].topWrestler;
                topProjectedConsoRound = loserWrestlerTop.bracket.conso_wins;
            }
            let loserWrestlerBottom = null;
            let bottomProjectedConsoRound = 0;
            if (round1[i + 1].winner !== 'unknown') {
                loserWrestlerBottom = round1[i + 1].winner === 'top'
                    ? round1[i + 1].bottomWrestler : round1[i + 1].topWrestler;
                bottomProjectedConsoRound = loserWrestlerBottom.bracket.conso_wins;
            }
            if (!!prelim_against && prelim_against.bracket.conso_prelim_wins > 0) {
                if (loserWrestlerTop.seed === prelim_at_seed) {
                    loserWrestlerTop = prelim_against;
                }
                if (loserWrestlerBottom.seed === prelim_at_seed) {
                    loserWrestlerBottom = prelim_against;
                }
            }
            let winner = (topProjectedConsoRound > 0) ? 'top' : 'unknown';
            winner = (bottomProjectedConsoRound > 0) ? 'bottom' : 'unknown';
            nextConsoMatches.push({
                topWrestler: loserWrestlerTop,
                topProjectedConsoRound: topProjectedConsoRound,
                bottomWrestler: loserWrestlerBottom,
                bottomProjectedConsoRound: bottomProjectedConsoRound,
                winner: winner,
                score: ''
            });
        }
        consoRounds.push({ matches: nextConsoMatches });

        // get round2 conso
        let prevConsoRoundMatches = nextConsoMatches;
        const round2 = champRounds[1].matches.slice().reverse();
        nextConsoMatches = [];
        for (let i = 0; i < prevConsoRoundMatches.length; i++) {
            const topWrestler = round2[i].winner === 'top' ? round2[i].bottomWrestler : round2[i].topWrestler;
            let bottomWrestler, bottomProjection;
            if (prevConsoRoundMatches[i].winner === 'top') {
                bottomWrestler = prevConsoRoundMatches[i].topWrestler;
                bottomProjection = prevConsoRoundMatches[i].topProjectedConsoRound;
            } else {
                bottomWrestler = prevConsoRoundMatches[i].bottomWrestler;
                bottomProjection = prevConsoRoundMatches[i].bottomProjectedConsoRound;
            }

            let winner = 'unknown';
            let topProjectedConsoRound = 0;
            if (!!topWrestler) {
                winner = (topWrestler.bracket.conso_wins > 0) ? 'top' : 'bottom';
                topProjectedConsoRound = topWrestler.bracket.conso_wins + 1;
            }
            nextConsoMatches.push({
                topWrestler: topWrestler,
                topProjectedConsoRound: topProjectedConsoRound,
                bottomWrestler: bottomWrestler,
                bottomProjectedConsoRound: bottomProjection,
                winner: winner,
                score: ''
            });

        }
        consoRounds.push({ matches: nextConsoMatches });

        // get round3 conso
        prevConsoRoundMatches = nextConsoMatches;
        nextConsoMatches = [];
        for (let i = 0; i < prevConsoRoundMatches.length; i = i + 2) {
            let topWrestler, topProjection;
            if (prevConsoRoundMatches[i].winner === 'top') {
                topWrestler = prevConsoRoundMatches[i].topWrestler;
                topProjection = prevConsoRoundMatches[i].topProjectedConsoRound;
            } else {
                topWrestler = prevConsoRoundMatches[i].bottomWrestler;
                topProjection = prevConsoRoundMatches[i].bottomProjectedConsoRound;
            }

            let bottomWrestler, bottomProjection;
            if (prevConsoRoundMatches[i + 1].winner === 'top') {
                bottomWrestler = prevConsoRoundMatches[i + 1].topWrestler;
                bottomProjection = prevConsoRoundMatches[i + 1].topProjectedConsoRound;
            } else {
                bottomWrestler = prevConsoRoundMatches[i + 1].bottomWrestler;
                bottomProjection = prevConsoRoundMatches[i + 1].bottomProjectedConsoRound;
            }
            const winner = (topProjection > bottomProjection) ? 'top' : 'bottom';
            nextConsoMatches.push({
                topWrestler: topWrestler,
                topProjectedConsoRound: topProjection,
                bottomWrestler: bottomWrestler,
                bottomProjectedConsoRound: bottomProjection,
                winner: winner,
                score: ''
            });

        }
        consoRounds.push({ matches: nextConsoMatches });

        // get round4 conso
        prevConsoRoundMatches = nextConsoMatches;
        const round4 = champRounds[2].matches;
        const crossBracket = [1, 0, 3, 2];
        nextConsoMatches = [];
        for (let i = 0; i < prevConsoRoundMatches.length; i++) {
            const champIndex = crossBracket[i];
            const topWrestler = round4[champIndex].winner === 'top'
                ? round4[champIndex].bottomWrestler : round4[champIndex].topWrestler;

            let bottomWrestler, bottomProjection;
            if (prevConsoRoundMatches[i].winner === 'top') {
                bottomWrestler = prevConsoRoundMatches[i].topWrestler;
                bottomProjection = prevConsoRoundMatches[i].topProjectedConsoRound;
            } else {
                bottomWrestler = prevConsoRoundMatches[i].bottomWrestler;
                bottomProjection = prevConsoRoundMatches[i].bottomProjectedConsoRound;
            }

            let winner = 'unknown';
            let topProjectedConsoRound = 0;
            if (!!topWrestler) {
                winner = (topWrestler.bracket.conso_wins > 0) ? 'top' : 'bottom';
                topProjectedConsoRound = topWrestler.bracket.conso_wins + 3;
            }
            nextConsoMatches.push({
                topWrestler: topWrestler,
                topProjectedConsoRound: topProjectedConsoRound,
                bottomWrestler: bottomWrestler,
                bottomProjectedConsoRound: bottomProjection,
                winner: winner,
                score: ''
            });

        }
        consoRounds.push({ matches: nextConsoMatches });

        // get round5 conso
        prevConsoRoundMatches = nextConsoMatches;
        nextConsoMatches = [];
        for (let i = 0; i < prevConsoRoundMatches.length; i = i + 2) {
            let topWrestler, topProjection;
            if (prevConsoRoundMatches[i].winner === 'top') {
                topWrestler = prevConsoRoundMatches[i].topWrestler;
                topProjection = prevConsoRoundMatches[i].topProjectedConsoRound;
            } else {
                topWrestler = prevConsoRoundMatches[i].bottomWrestler;
                topProjection = prevConsoRoundMatches[i].bottomProjectedConsoRound;
            }

            let bottomWrestler, bottomProjection;
            if (prevConsoRoundMatches[i + 1].winner === 'top') {
                bottomWrestler = prevConsoRoundMatches[i + 1].topWrestler;
                bottomProjection = prevConsoRoundMatches[i + 1].topProjectedConsoRound;
            } else {
                bottomWrestler = prevConsoRoundMatches[i + 1].bottomWrestler;
                bottomProjection = prevConsoRoundMatches[i + 1].bottomProjectedConsoRound;
            }
            const winner = (topProjection > bottomProjection) ? 'top' : 'bottom';
            nextConsoMatches.push({
                topWrestler: topWrestler,
                topProjectedConsoRound: topProjection,
                bottomWrestler: bottomWrestler,
                bottomProjectedConsoRound: bottomProjection,
                winner: winner,
                score: ''
            });
        }
        consoRounds.push({ matches: nextConsoMatches });

        // get round6 conso
        prevConsoRoundMatches = nextConsoMatches;
        const round6 = champRounds[3].matches.slice().reverse();
        nextConsoMatches = [];
        for (let i = 0; i < prevConsoRoundMatches.length; i++) {
            const topWrestler = round6[i].winner === 'top'
                ? round6[i].bottomWrestler : round6[i].topWrestler;

            let bottomWrestler, bottomProjection;
            if (prevConsoRoundMatches[i].winner === 'top') {
                bottomWrestler = prevConsoRoundMatches[i].topWrestler;
                bottomProjection = prevConsoRoundMatches[i].topProjectedConsoRound;
            } else {
                bottomWrestler = prevConsoRoundMatches[i].bottomWrestler;
                bottomProjection = prevConsoRoundMatches[i].bottomProjectedConsoRound;
            }

            let winner = 'unknown';
            let topProjectedConsoRound = 0;
            if (!!topWrestler) {
                winner = (topWrestler.bracket.conso_wins > 0) ? 'top' : 'bottom';
                topProjectedConsoRound = topWrestler.bracket.conso_wins + 5;
            }
            nextConsoMatches.push({
                topWrestler: topWrestler,
                topProjectedConsoRound: topProjectedConsoRound,
                bottomWrestler: bottomWrestler,
                bottomProjectedConsoRound: bottomProjection,
                winner: winner,
                score: ''
            });

        }
        consoRounds.push({ matches: nextConsoMatches });

        // get round7 conso
        prevConsoRoundMatches = nextConsoMatches;
        nextConsoMatches = [];
        for (let i = 0; i < prevConsoRoundMatches.length; i = i + 2) {
            let topWrestler, topProjection;
            if (prevConsoRoundMatches[i].winner === 'top') {
                topWrestler = prevConsoRoundMatches[i].topWrestler;
                topProjection = prevConsoRoundMatches[i].topProjectedConsoRound;
            } else {
                topWrestler = prevConsoRoundMatches[i].bottomWrestler;
                topProjection = prevConsoRoundMatches[i].bottomProjectedConsoRound;
            }

            let bottomWrestler, bottomProjection;
            if (prevConsoRoundMatches[i + 1].winner === 'top') {
                bottomWrestler = prevConsoRoundMatches[i + 1].topWrestler;
                bottomProjection = prevConsoRoundMatches[i + 1].topProjectedConsoRound;
            } else {
                bottomWrestler = prevConsoRoundMatches[i + 1].bottomWrestler;
                bottomProjection = prevConsoRoundMatches[i + 1].bottomProjectedConsoRound;
            }
            let winner = 'unknown';
            if (!!topWrestler) {
                winner = (topWrestler.bracket.placement_win > 0) ? 'top' : 'bottom';
            }

            nextConsoMatches.push({
                topWrestler: topWrestler,
                topProjectedConsoRound: topProjection,
                bottomWrestler: bottomWrestler,
                bottomProjectedConsoRound: bottomProjection,
                winner: winner,
                score: ''
            });
        }
        consoRounds.push({ matches: nextConsoMatches });
        return consoRounds;
    }

    function getNextRoundMatch(round: number, topMatch: Match, bottomMatch: Match): Match {
        let winnerWrestlerTop = null;
        if (topMatch.winner !== 'unknown') {
            winnerWrestlerTop = topMatch.winner === 'top'
                ? topMatch.topWrestler : topMatch.bottomWrestler;
        }
        let winnerWrestlerBottom = null;
        if (bottomMatch.winner !== 'unknown') {
            winnerWrestlerBottom = bottomMatch.winner === 'top'
                ? bottomMatch.topWrestler : bottomMatch.bottomWrestler;
        }
        let winner = 'unknown';
        if (!!winnerWrestlerTop && !!winnerWrestlerBottom) {
            winner = (winnerWrestlerTop.bracket.champ_wins > round) ? 'top' : 'bottom';
            if (round === CHAMPIONSHIP_MATCH_ROUND) {
                winner = (winnerWrestlerTop.bracket.placement_win > 0) ? 'top' : 'bottom';
            }
        }
        return {
            topWrestler: winnerWrestlerTop,
            bottomWrestler: winnerWrestlerBottom,
            winner: winner,
            score: ''
        };
    }
    const pageBracket: PageBracket = {
        rounds: [],
        consoRounds: [],
        finalMatches: []
    };
    let matches: Match[] = [];
    // round 1
    let replacement = -1;
    if (bracket.seeds[33].seed33_replace_seed !== undefined) {
        replacement = bracket.seeds[33].seed33_replace_seed;
    }
    for (const key of Object.keys(matchups)) {
        let seed1 = matchups[key][0];
        let seed2 = matchups[key][1];
        seed1 = (replacement === seed1) ? 33 : seed1;
        seed2 = (replacement === seed2) ? 33 : seed2;
        const wrestlerTop = bracket.seeds[seed1];
        const wrestlerBottom = bracket.seeds[seed2];
        let winner = 'unknown';
        if (wrestlerTop.bracket.champ_wins > 0) {
            winner = 'top';
        } else if (wrestlerBottom.bracket.champ_wins > 0) {
            winner = 'bottom';
        }
        if (winner === 'unknown') {
            // console.log('WARNING', wrestlerTop.bracket, wrestlerBottom.bracket);
        }
        const match: Match = {
            topWrestler: wrestlerTop,
            bottomWrestler: wrestlerBottom,
            winner: winner,
            score: ''
        };
        matches.push(match);
    }
    pageBracket.rounds.push({ matches: matches });

    // round 2
    for (let champRound = 1; champRound <= 4; champRound++) {
        let nextRoundMatches: Match[] = [];
        for (let i = 0; i < matches.length; i = i + 2) {
            const match = getNextRoundMatch(champRound, matches[i], matches[i + 1]);
            nextRoundMatches.push(match);
        }
        matches = nextRoundMatches;
        nextRoundMatches = [];
        pageBracket.rounds.push({ matches: matches });
    }

    // getConsoRound
    pageBracket.consoRounds = getConsoRounds(
        pageBracket.rounds,
        bracket.conso_prelim_at_pos,
        bracket.seeds[bracket.conso_prelim_against_pos]
    );
    pageBracket.finalMatches = getFinalsMatchups(pageBracket.rounds, pageBracket.consoRounds);
    // console.log(pageBracket);
    return pageBracket;
}
