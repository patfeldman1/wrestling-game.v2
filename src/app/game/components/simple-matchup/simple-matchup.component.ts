import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Match } from '../../models/page-brackets.model';

@Component({
    selector: 'app-simple-matchup',
    templateUrl: './simple-matchup.component.html',
    styleUrls: ['./simple-matchup.component.scss']
})
export class SimpleMatchupComponent implements OnInit {
    @Input() match: Match;
    @Input() linkClass = 'font-12';
    @Input() isReverse = false;
    constructor() { }
    ngOnInit() { }
    GetTopWrestler() {
        return (this.isReverse) ? this.match.bottomWrestler : this.match.topWrestler;
    }
    GetBottomWrestler() {
        return (this.isReverse) ? this.match.topWrestler : this.match.bottomWrestler;
    }
}
