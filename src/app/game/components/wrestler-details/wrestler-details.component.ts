import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Match } from '../../models/page-brackets.model';
import { WrestlingLookupService } from '../../services/wrestling-lookup.service';
import { WrestlerHistory } from '../../models/wrestler-history.model';
import { WrestlerInfo } from '../../models/wrestler-info.model';

@Component({
    selector: 'app-wrestler-details',
    templateUrl: './wrestler-details.component.html',
    styleUrls: ['./wrestler-details.component.scss']
})
export class WrestlerDetailsComponent implements OnInit {
    @Input() goToMatches: boolean;
    @Input() id: string;
    wrestler: WrestlerHistory;
    info: WrestlerInfo = null;
    selectedIndex: number;
    infoYear = '2018';
    constructor(private lookup: WrestlingLookupService) { }
    ngOnInit() {
        this.wrestler = this.lookup.getWrestlerHistory(this.id);
        if (Object.keys(this.wrestler.years).length > 0) {
            this.infoYear = Object.keys(this.wrestler.years).sort().reverse()[0];
            this.info = this.wrestler.years[this.infoYear].info;
        }
        this.selectedIndex = (this.goToMatches) ? 1 : 0;
    }
    NumberOfAllAmericans(): number {
        let sum = 0;
        if (!this.wrestler.years) { return sum; }
        for (const key of Object.keys(this.wrestler.years)) {
            if (this.wrestler.years[key].placement > 0) {
                sum += 1;
            }
        }
        return sum;
    }
    NumberOfWins(year: string): number {
        let wins = 0;
        if (!this.wrestler.years) { return wins; }
        for (const match of this.wrestler.years[year].matches) {
            if (match.winner.pid === this.id) {
                wins += 1;
            }
        }
        return wins;
    }
}
