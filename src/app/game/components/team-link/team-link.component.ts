import { TeamDialogComponent } from './../../dialogs/team/team-dialog.component';
import { Directive, ElementRef, HostListener, Renderer2, Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
    selector: 'app-team-link',
    templateUrl: './team-link.component.html',
    styleUrls: ['./team-link.component.scss']
})
export class TeamLinkComponent {
    @Input() id: string;
    @Input() goToMatches: boolean;
    constructor(public dialog: MatDialog) { }
    @HostListener('click') onClick(btn: any) {
        const dialogRef = this.dialog.open(TeamDialogComponent, {
            width: '360px',
            height: '60vh',
            data: { id: this.id, goToMatches: this.goToMatches }
        });
    }
}
