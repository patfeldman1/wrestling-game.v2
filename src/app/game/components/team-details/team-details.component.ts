import { TeamInfoById, TeamYearInfo } from './../../models/team-results.model';
import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Match } from '../../models/page-brackets.model';
import { WrestlingLookupService } from '../../services/wrestling-lookup.service';

@Component({
    selector: 'app-team-details',
    templateUrl: './team-details.component.html',
    styleUrls: ['./team-details.component.scss']
})
export class TeamDetailsComponent implements OnInit {
    @Input() id: string;
    selectedIndex: number;
    infoYear: string;
    info: TeamYearInfo;
    team: TeamInfoById;
    constructor(private lookup: WrestlingLookupService) { }
    ngOnInit() {
        this.team = this.lookup.getTeamHistory(this.id);
        this.infoYear = Object.keys(this.team.years).sort().reverse()[0];
        this.info = this.team.years[this.infoYear];
    }
    IsTopTen() {
        return this.info.finish <= 10;
    }
}
