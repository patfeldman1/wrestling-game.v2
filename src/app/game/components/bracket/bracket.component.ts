import { BracketByWeight } from './../../models/brackets.model';
import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Input } from '@angular/core';
import { WrestlingLookupService } from '../../services/wrestling-lookup.service';
import { PageBracket, Match } from '../../models/page-brackets.model';
import { WrestlerInfo } from '../../models/wrestler-info.model';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { waitForMap } from '@angular/router/src/utils/collection';
import { Store } from '@ngrx/store';
import * as fromBase from '../../../reducers';
import { Observable } from '@firebase/util';
import { IBracketSeeds, IUpdateBracket } from '../../../core/models/bracket.model';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-bracket',
  templateUrl: './bracket.component.html',
  styleUrls: ['./bracket.component.scss']
})
export class BracketComponent implements OnInit, OnChanges, OnDestroy {
  @Input() year: string;
  @Input() weight: string;
  @Input() bracketType = 0;

  bracket: PageBracket;
  destroyThis: Subscription;
  rounds = [0, 1, 2, 3, 4];
  consoRounds = [0, 1, 2, 3, 4, 5, 6];
  constructor(
    private lookup: WrestlingLookupService,
    private _store: Store<fromBase.RootStoreState>
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if ('year' in changes || 'weight' in changes) {
      this.bracket = this.lookup.getBracket(this.weight, this.year);
      console.log(this.bracket);
    }
  }
  ngOnInit() {
    this.destroyThis = this._store.select(fromBase.getLiveBrackets).subscribe( liveBrackets => {
      console.log(liveBrackets);
    });
  }
  ngOnDestroy () {
    this.destroyThis.unsubscribe();
  }
  getWinner(): WrestlerInfo {
    if (!this.bracket) { return null; }
    const match = this.bracket.rounds[4].matches[0];
    if (match.winner === 'unknown') { return null; }
    const winner = (match.winner === 'top') ? match.topWrestler : match.bottomWrestler;
    return winner;
  }
  getConsoWinner(): WrestlerInfo {
    if (!this.bracket) { return null; }
    const match = this.bracket.consoRounds[6].matches[0];
    const winner = (match.winner === 'top') ? match.topWrestler : match.bottomWrestler;
    return winner;
  }
  getPlacementWinner(match: Match): WrestlerInfo {
    const winner = (match.winner === 'top') ? match.topWrestler : match.bottomWrestler;
    return winner;
  }
  getPlacementText(index: number): string {
    switch (index) {
      case 0: return '1st Place';
      case 1: return '3rd Place';
      case 2: return '5th Place';
      case 3: return '7th Place';
    }
  }
  getAllAmericans(): WrestlerInfo[] {
    const wrestlers: WrestlerInfo[] = [];
    for (let i = 0; i < this.bracket.finalMatches.length; i++) {
      const match = this.bracket.finalMatches[i];
      const winner = (match.winner === 'top') ? match.topWrestler : match.bottomWrestler;
      const loser = (match.winner === 'top') ? match.bottomWrestler : match.topWrestler;
      wrestlers.push(winner);
      wrestlers.push(loser);
    }
    return wrestlers;
  }
}
