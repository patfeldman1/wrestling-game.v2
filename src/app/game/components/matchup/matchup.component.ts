import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Match } from '../../models/page-brackets.model';

@Component({
    selector: 'app-matchup',
    templateUrl: './matchup.component.html',
    styleUrls: ['./matchup.component.scss']
})
export class MatchupComponent implements OnInit {
    @Input() match: Match;
    @Input() linkClass = 'font-11';
    @Input() linkTeamClass = 'font-10';
    constructor() { }
    ngOnInit() { }
}
