import { Directive, ElementRef, HostListener, Renderer2, Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { WrestlerDialogComponent } from '../../dialogs/wrestler/wrestler-dialog.component';

@Component({
    selector: 'app-wrestler-link',
    templateUrl: './wrestler-link.component.html',
    styleUrls: ['./wrestler-link.component.scss']
})
export class WrestlerLinkComponent {
    @Input() id: string;
    @Input() goToMatches: boolean;
    constructor(public dialog: MatDialog) { }
    @HostListener('click') onClick(btn: any) {
        const dialogRef = this.dialog.open(WrestlerDialogComponent, {
            width: '360px',
            height: '60vh',
            data: { id: this.id, goToMatches: this.goToMatches }
        });
    }
}
