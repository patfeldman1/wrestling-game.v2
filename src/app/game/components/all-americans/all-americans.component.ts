import { WrestlerInfo } from './../../models/wrestler-info.model';
import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Match } from '../../models/page-brackets.model';
import { MatDialog } from '@angular/material';
import { WrestlerDialogComponent } from '../../dialogs/wrestler/wrestler-dialog.component';

@Component({
    selector: 'app-all-americans',
    templateUrl: './all-americans.component.html',
    styleUrls: ['./all-americans.component.scss']
})
export class AllAmericansComponent implements OnInit {
    @Input() wrestlers: WrestlerInfo[];
    constructor(public dialog: MatDialog) { }
    ngOnInit() { }

    OpenDialog(id: string) {
        const dialogRef = this.dialog.open(WrestlerDialogComponent, {
            width: '100%',
            height: '100%',
            data: { id: id }
        });

    }
}
