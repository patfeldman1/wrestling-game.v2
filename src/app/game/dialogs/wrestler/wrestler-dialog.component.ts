import { Component, OnInit } from '@angular/core';
import { Input, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-wrestler-dialog',
    templateUrl: './wrestler-dialog.component.html',
    styleUrls: ['./wrestler-dialog.component.scss']
})
export class WrestlerDialogComponent implements OnInit {
    id: string;
    goToMatches: boolean;
    constructor(
        public dialogRef: MatDialogRef<WrestlerDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.id = data['id'];
        this.goToMatches = (data['goToMatches'] === undefined) ? false : data['goToMatches'];
    }
    ngOnInit() {
    }
}
