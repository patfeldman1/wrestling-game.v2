import { Component, OnInit } from '@angular/core';
import { Input, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-team-dialog',
    templateUrl: './team-dialog.component.html',
    styleUrls: ['./team-dialog.component.scss']
})
export class TeamDialogComponent implements OnInit {
    id: string;
    goToMatches: boolean;
    constructor(
        public dialogRed: MatDialogRef<TeamDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.id = data['id'];
    }
    ngOnInit() {
    }
}
