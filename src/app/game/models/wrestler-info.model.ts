import { TeamPoints } from './results.model';
import { BracketResults } from './brackets.model';

export interface WrestlerInfo {
    last_name: string;
    first_name: string;
    weight: string;
    grade: number;
    global_pid: string;
    team_id: string;
    global_tid: string;
    seed: number;
    wins: number;
    losses: number;
    team: string;
    wrestler_id: string;
    seed33_replace_seed?: number;
    bracket?: BracketResults;
    placement?: string;
    points?: TeamPoints;
}

