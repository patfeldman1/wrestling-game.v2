export interface AllResultInfo {
    result_by_id: { [pid: string]: ResultsById };
}
export interface ResultsById {
    participant_years: string[];
    first_name: string;
    last_name: string;
    yearly_summary: { [year: string]: ResultYearSummary };
    years: { [year: string]: ResultYearDetails };
}

export interface ResultYearSummary {
    points: number;
    place: number;
}
export interface ResultYearDetails {
    placement: number;
    points: TeamPoints;
    matches: WrestlerMatch[];
}
export interface TeamPoints {
    total_points: number;
    placement_points: number;
    advancement_points: number;
    pin_points: number;
}
export interface WrestlerMatch {
    decision_detail: string;
    decision: string;
    round: string;
    winner: MatchWrestlerInfo;
    loser: MatchWrestlerInfo;
    points: TeamPoints;
}
export interface MatchWrestlerInfo {
    first_name: string;
    last_name: string;
    pid: string;
    school: string;
    record: string;
}
