import { WrestlerInfo } from './wrestler-info.model';
import { TeamPoints } from './results.model';

export interface AllTeamResultInfo {
    team_results: { [year: string]: TeamFinishByYear[] };
    team_info: { [tid: string]: TeamInfoById };
}
export interface TeamFinishByYear {
    total_points: number;
    global_tid: string;
    participants: number;
    all_americans: number;
    team: string;
}
export interface TeamInfoById {
    team_id: string;
    team: string;
    years: { [year: string]: TeamYearInfo };
}

export interface TeamYearInfo {
    participants: WrestlerInfo[];
    points: TeamPoints;
    finish: number;
}
