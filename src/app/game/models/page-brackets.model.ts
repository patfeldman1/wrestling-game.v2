import { WrestlerInfo } from './wrestler-info.model';

export interface PageBracket {
    rounds: PageRound[];
    consoRounds: PageRound[];
    finalMatches: Match[];
}
export interface PageRound {
    matches: Match[];
}

export interface Match {
    topWrestler?: WrestlerInfo;
    topProjectedConsoRound?: number;
    bottomWrestler?: WrestlerInfo;
    bottomProjectedConsoRound?: number;
    winner: string;
    score: string;
}

