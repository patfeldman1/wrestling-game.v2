import { WrestlerInfo } from './wrestler-info.model';

export interface AllParticipantInfo {
    participant_by_id: { [pid: string]: ParticipantById };
}

export interface ParticipantById {
    first_name: string;
    last_name: string;
    years: { [year: string]: WrestlerInfo };
}
