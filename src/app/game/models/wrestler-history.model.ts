import { WrestlerInfo } from './wrestler-info.model';
import { TeamPoints, WrestlerMatch } from './results.model';

export interface WrestlerHistory {
    first_name: string;
    last_name: string;
    years: { [year: string]: WrestlerHistoryYear };
}

export interface WrestlerHistoryYear {
    info: WrestlerInfo;
    matches: WrestlerMatch[];
    points: TeamPoints;
    placement: number;
}



