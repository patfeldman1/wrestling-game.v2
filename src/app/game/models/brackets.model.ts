import { BracketResults } from './brackets.model';
import { WrestlerInfo } from './wrestler-info.model';

export interface AllBracketInfo {
    bracketByYear: { [year: string]: BracketByWeight };
}

export interface BracketByWeight {
    bracketByWeight: { [weight: string]: Bracket };
}

export interface Bracket {
    seeds: { [seed: number]: WrestlerInfo };
    conso_prelim_at_pos: number;
    conso_prelim_against_pos: number;
}

export interface BracketResults {
    conso_wins: number;
    champ_wins: number;
    conso_prelim_wins: number;
    champ_prelim_wins: number;
    placement_win: number;
}

