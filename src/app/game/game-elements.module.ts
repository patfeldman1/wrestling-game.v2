import { MaterialModule } from './../material.module';
import { SharedModule } from '../shared/shared.module';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';

import { WrestlingLookupService } from './services/wrestling-lookup.service';
import { WrestlerDialogComponent } from './dialogs/wrestler/wrestler-dialog.component';
import { TeamDialogComponent } from './dialogs/team/team-dialog.component';
import { BracketComponent } from './components/bracket/bracket.component';
import { MatchupComponent } from './components/matchup/matchup.component';
import { TeamDetailsComponent } from './components/team-details/team-details.component';
import { SimpleMatchupComponent } from './components/simple-matchup/simple-matchup.component';
import { AllAmericansComponent } from './components/all-americans/all-americans.component';
import { WrestlerDetailsComponent } from './components/wrestler-details/wrestler-details.component';
import { WrestlerLinkComponent } from './components/wrestler-link/wrestler-link.component';
import { TeamLinkComponent } from './components/team-link/team-link.component';

@NgModule({
  imports: [
    SharedModule,
    BrowserModule,
    MaterialModule
  ],
  declarations: [
    BracketComponent,
    MatchupComponent,
    SimpleMatchupComponent,
    AllAmericansComponent,
    WrestlerDetailsComponent,
    TeamDetailsComponent,
    WrestlerDialogComponent,
    TeamDialogComponent,
    WrestlerLinkComponent,
    TeamLinkComponent
    // DIRECTIVES
  ],
  entryComponents: [
    WrestlerDialogComponent,
    TeamDialogComponent
  ],
  providers: [
    WrestlingLookupService
  ],
  exports: [
    BracketComponent,
    MatchupComponent,
    WrestlerDetailsComponent,
    SimpleMatchupComponent,
    MatchupComponent
  ]
})
export class GameElementsModule { }
