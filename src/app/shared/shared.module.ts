import { SortPipe } from './pipes/sort.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '../material.module';
import { LoaderComponent } from './loader/loader.component';
import { KeysPipe } from './pipes/keys.pipe';
import { OrdinalPipe } from './pipes/ordinal.pipe';
import { GradesTextPipe } from './pipes/grade-text.pipe';

const PIPES = [KeysPipe, OrdinalPipe, GradesTextPipe, SortPipe];

@NgModule({
  imports: [
    FlexLayoutModule,
    MaterialModule,
    CommonModule
  ],
  declarations: [
    LoaderComponent,
    PIPES
  ],
  exports: [
    LoaderComponent,
    PIPES
  ]
})
export class SharedModule { }
