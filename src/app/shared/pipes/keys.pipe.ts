import { Injectable, Pipe } from '@angular/core';
import { PipeTransform } from '@angular/core/src/change_detection/pipe_transform';

@Pipe({ name: 'keys' })
@Injectable()
export class KeysPipe implements PipeTransform {
    transform(objectArray: {}, sort: null): string[] {
        if (!objectArray) { return []; }
        const retArr = Object.keys(objectArray);
        if (sort === 'desc') {
            return retArr.sort().reverse();
        } else if (sort === 'asc') {
            return retArr.sort();
        }
        return retArr;
    }
}
