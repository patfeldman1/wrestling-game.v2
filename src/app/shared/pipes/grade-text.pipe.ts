import { Injectable, Pipe } from '@angular/core';
import { PipeTransform } from '@angular/core/src/change_detection/pipe_transform';

@Pipe({ name: 'gradeText' })
@Injectable()
export class GradesTextPipe implements PipeTransform {
    transform(grade: number): string {
        if (!grade) { return ''; }
        switch (grade) {
            case 9: return 'freshman';
            case 10: return 'sophomore';
            case 11: return 'junior';
            case 12: return 'senior';
        }
        return '';
    }
}
