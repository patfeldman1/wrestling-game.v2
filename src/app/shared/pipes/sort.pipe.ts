import { Injectable, Pipe } from '@angular/core';
import { PipeTransform } from '@angular/core/src/change_detection/pipe_transform';

@Pipe({ name: 'sort' })
@Injectable()
export class SortPipe implements PipeTransform {
    transform(objectArray: any[], sortCategory: string, sortDirection: string = 'asc'): any[] {
        if (!objectArray) { return []; }
        return objectArray.sort( (a, b) =>
            (sortDirection === 'asc' ? a[sortCategory] - b[sortCategory] : b[sortCategory] - a[sortCategory])
        );
    }
}
