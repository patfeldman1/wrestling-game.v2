import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  StoreRouterConnectingModule,
  RouterStateSerializer
} from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { reducers } from './reducers';
import { CustomRouterStateSerializer } from './shared/utils';
import { environment } from '../environments/environment';
import { AuthEffects } from './core/store/auth.store/auth.effects';
import { LiveEffects } from './core/store/live.store/live.effects';

@NgModule({
  imports: [
    StoreModule.forRoot(reducers),
    StoreRouterConnectingModule,
    EffectsModule.forRoot([AuthEffects, LiveEffects]),
    StoreDevtoolsModule.instrument({
      name: 'NgRx wrestlingGame DevTools',
      logOnly: environment.production,
    }),
  ],
  exports: [StoreModule],
  providers: [{ provide: RouterStateSerializer, useClass: CustomRouterStateSerializer }]
})
export class AppStoreModule { }
