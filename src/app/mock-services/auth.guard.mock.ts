import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class AuthGuardServiceMock {
    constructor() { }
    canActivate(): Observable<boolean> { return Observable.of(true);}
}
