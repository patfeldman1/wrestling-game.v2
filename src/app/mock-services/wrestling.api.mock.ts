import { IWrestler } from './../league/models/wrestlers.interface';
import { IUserWithKey, IDraftUser, INewDraftUser } from './../league/models/user.interface';
import { IUserLeagueRef } from './../league/models/user-league.interface';
import { Store } from '@ngrx/store';
import { ICreateLeague } from './../league/models/league-create.interface';
import { ILeague, IDraftedWrestler, ILiveDraft, IDraftSetup } from './../league/models/league.interface';
import { IJoinLeague } from './../league/models/league-join.interface';
import { IWrestlers } from './../league/models/wrestlers.interface';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/bufferCount';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import * as _ from 'lodash';
import * as fromRoot from './../league/store/wrestling.store';
import * as userActions from './../league/store/user.store/user.actions';
import * as leaguesActions from './../league/store/leagues.store/leagues.actions';
import * as wrestlersActions from '../league/store/wrestlers.store/wrestlers.actions';
import { AppState } from './../league/store/wrestling.store';
import { IWrestlerHighlight } from '../league/models/highlights.class';
import * as mockdb from './techfalling-2018.json';
import { all_weights } from '../util/consts';

@Injectable()
export class WrestlingServiceMock {
  constructor(
    private http: Http,
    private af: AngularFireDatabase,
    private _store: Store<AppState>) { }


  public GetAllParticipants(): Observable<IWrestlers> {
    console.log('wrestling.GetAllParticipants');
    return this.http.get('assets/data/participants.2017.scores.json').map(res => res.json())
      .map(data => {
        data.id.forEach((wrestler, index, array) => {
          wrestler.id = index;
        });
        data.weight = {};
        all_weights.forEach(weight => {
          data.weight[weight] = _.map(data.id, (value, key) => value)
            .filter(wrestler => {
              return !!wrestler && wrestler['weight'] === weight;
            }).sort((a: IWrestler, b: IWrestler) => a.rank - b.rank).map(wrestler => wrestler['id']);
        });
        return {
          wrestlersById: data.id,
          wrestlersByWeight: data.weight
        };
      });
    // return this.http.get('assets/data/participants.2015.json').map(res => res.json()).map(data => {
    // 	return {
    // 		wrestlersById: data.id,
    // 		wrestlersByWeight: data.weight
    // 	}
    // });
  }

  public AddWrestlerToMyHighlights(
    myKey: string,
    wrestler: IWrestler,
    highlightId: number,
    priority: number
  ): Observable<IWrestlerHighlight> {
    console.log('wrestling.AddWrestlerToMyHighlights');
    return new Observable<IWrestlerHighlight>(observer => {
      const myHighlights = this.af.list('/users/' + myKey + '/highlights');
      const highlightedWrestler: IWrestlerHighlight = {
        wrestlerId: wrestler.id,
        wrestlerWeight: wrestler.weight,
        priority: priority,
        highlightId: highlightId,
        key: ''
      };
      myHighlights.push(highlightedWrestler).then(complete => {
        highlightedWrestler.key = complete.key;
        observer.next(highlightedWrestler);
        observer.complete();
      });
      //   .catch(error => {
      //   observer.complete();
      // });
    });
  }
  public RemoveWrestlerToMyHighlights(myKey: any, wrestlerId: any): Observable<number> {
    console.log('wrestling.RemoveWrestlerToMyHighlights');
    return new Observable<number>(observer => {
      const myHighlights = this.af.list('/users/' + myKey + '/highlights',
        ref => ref.limitToLast(1).orderByChild('wrestlerId').equalTo(wrestlerId));

      myHighlights.valueChanges().take(1).subscribe(foundHighlight => {
        if (!foundHighlight || foundHighlight.length === 0) {
          observer.complete();
        } else {
          myHighlights.remove(foundHighlight[0]['$key'])
            .then(complete => {
              observer.next(wrestlerId);
              observer.complete();
            }).catch(notComplete => {
              observer.complete();
            });
        }
      });
    });
  }

  public AddParticipantToDraft(league: ILeague, name: string): Observable<INewDraftUser> {
    console.log('wrestling.AddParticipantToDraft');
    return new Observable<INewDraftUser>(observer => {
      let draftPos = 0;
      if (!!league.draftSetup.users) {
        draftPos = Object.keys(league.draftSetup.users).length;
      }
      const directive = league.isPrivate ? 'private' : 'public';
      const users = this.af.list('/leagues/' + directive + '/' + league.$key + '/draftSetup/users');
      const newUser = {
        name: name,
        draftOrder: draftPos
      };
      users.push(newUser).then(complete => {
        observer.next(newUser);
        observer.complete();
      });
      //   .catch(notComplete => {
      //   console.log('error in push new users');
      //   console.log(notComplete);
      //   observer.complete();
      // });
    });
  }

  public DeleteParticipantFromDraft(league: ILeague, user: IDraftUser): Observable<IDraftUser> {
    console.log('wrestling.DeleteParticipantFromDraft');
    return new Observable<IDraftUser>(observer => {
      const directive = league.isPrivate ? 'private' : 'public';
      const users = this.af.list('/leagues/' + directive + '/' + league.$key + '/draftSetup/users',
        ref => ref.limitToLast(1).orderByChild('name').equalTo(user.name));
      users.valueChanges().take(1).subscribe(foundUsers => {
        users.remove(foundUsers[0]['$key'])
          .then(complete => {
            observer.next(user);
            observer.complete();
          }).catch(notComplete => {
            console.log('error in push new users');
            console.log(notComplete);
            observer.complete();
          });
      });
    });
  }


  public UpdateDraftOrder(league: ILeague, newUsers: IDraftUser[]): Observable<boolean> {
    console.log('wrestling.UpdateDraftOrder');
    return new Observable<boolean>(observer => {
      const directive = league.isPrivate ? 'private' : 'public';
      const $dbUserList = this.af.list('/leagues/' + directive + '/' + league.$key + '/draftSetup/users');
      $dbUserList.valueChanges().take(1).subscribe(currentUsers => {
        currentUsers.forEach((value, key) => {
          const newUser = newUsers.find(user => user.name === value['name']);
          if (newUser && value['draftOrder'] !== newUser.draftOrder) {
            $dbUserList.update(value['$key'], { draftOrder: newUser.draftOrder })
              .then(update => {
                observer.next(true);
                observer.complete();
              })
              .catch(error => {
                observer.next(false); // TODO Handle this error
                observer.complete();
              });
          }
        });
      });
    });
  }

  public UpdateDraftDate(league: ILeague, date: number): Observable<boolean> {
    console.log('wrestling.UpdateDraftDate');
    return new Observable<boolean>(observer => {
      const directive = league.isPrivate ? 'private' : 'public';
      const $draftDate = this.af.object('/leagues/' + directive + '/' + league.$key + '/draftSetup');
      $draftDate.update({ draftDate: date })
        .then(update => {
          observer.next(true);
          observer.complete();
        })
        .catch(error => {
          observer.next(false); // TODO Handle this error
          observer.complete();
        });
    });
  }
  public SelectWrestler(
    wrestler: IWrestler,
    draftedBy: IUserWithKey,
    leagueKey: string,
    isPrivate: boolean,
    round: number,
    slot: number
  ): void {
    console.log('wrestling.SelectWrestler');
    const directive = isPrivate ? 'private' : 'public';
    // update the number of people selected
    const numUpdated = this.af.object('/leagues/' + directive + '/' + leagueKey + '/liveDraft/numSelected');
    // numUpdated.$ref.transaction(currentCount => currentCount + 1);

    const draftedParticipantList =
      this.af.list('/leagues/' + directive + '/' + leagueKey + '/liveDraft/draftedParticipants');
    const wrestlerPickInfo: IDraftedWrestler = {
      wrestlerId: wrestler.id.toString(),
      wrestlerWeight: wrestler.weight.toString(),
      wrestlerRank: wrestler.rank,
      draftedRound: round,
      draftedByName: draftedBy.name,
      draftedByKey: draftedBy.$key,
      draftedById: draftedBy.id,
      draftedByOrderSlot: slot
    };
    draftedParticipantList.push(wrestlerPickInfo);
    const myTeam =
      this.af.list('/leagues/' + directive + '/' + leagueKey + '/liveDraft/draftedByTeam/' + draftedBy.$key);
    myTeam.push(wrestlerPickInfo);

  }

  public GetLiveDraftNumSelectedConnection(leagueKey: string, isPrivate: boolean): Observable<number> {
    const directive = isPrivate ? 'private' : 'public';
    const databaseLeague = mockdb['leagues'][directive][leagueKey]['liveDraft']['numSelected'];
    return Observable.of(databaseLeague);
  }

  public GetLiveDraftTeamsConnection(
    leagueKey: string,
    isPrivate: boolean
  ): Observable<dictionary<dictionary<IDraftedWrestler>>> {
    const directive = isPrivate ? 'private' : 'public';
    const databaseLeague = mockdb['leagues'][directive][leagueKey]['liveDraft']['draftedByTeam'];
    return Observable.of(databaseLeague);
  }

  public GetLiveDraftParticipantsConnection(
    leagueKey: string,
    isPrivate: boolean
  ): Observable<dictionary<IDraftedWrestler>> {
    const directive = isPrivate ? 'private' : 'public';
    const databaseLeague = mockdb['leagues'][directive][leagueKey]['liveDraft']['draftedParticipants'];
    return Observable.of(databaseLeague);
  }

  public GetLiveDraftConnection(leagueKey: string, isPrivate: boolean): AngularFireObject<ILiveDraft> {
    console.log('wrestling.GetLiveDraftConnection');
    const directive = isPrivate ? 'private' : 'public';
    const retVal = this.af.object<ILiveDraft>('/leagues/' + directive + '/' + leagueKey + '/liveDraft');
    return retVal;
  }

  public GetDraftSetup(leagueKey: string, isPrivate: boolean): Observable<IDraftSetup> {
    const directive = isPrivate ? 'private' : 'public';
    const databaseLeague = mockdb['leagues'][directive][leagueKey]['draftSetup'];
    return Observable.of(databaseLeague);
  }

  public GetCurrentLeagueConnection(league: IUserLeagueRef): Observable<ILeague> {
    const directive = league.isPrivate ? 'private' : 'public';
    const databaseLeague = mockdb['leagues'][directive][league.leagueKey];
    return Observable.of(Object.assign({}, databaseLeague, { $key: league.leagueKey }));
  }


  // USER FUNCTIONS
  public AddLeagueToCurrentUser(leagueRef: IUserLeagueRef): Observable<boolean> {
    console.log('wrestling.AddLeagueToCurrentUser');
    const retVal = new Observable<boolean>(observer => {
      this._store.select(fromRoot.getMyInfo).take(1).subscribe(myInfo => {
        const myLeagues = this.af.list('/users/' + myInfo.$key + '/leagues_ref');
        myLeagues.push(leagueRef)
          .then(leagueInfo => {
            observer.next(true);
            observer.complete();
          });
        // .catch(error => {
        //   observer.next(false);
        //   observer.complete();
        // });
      });
    });
    return retVal;
  }

  public SearchPublicLeagues(searchString: string): Observable<ILeague[]> {
    console.log('wrestling.SearchPublicLeagues');
    return this.af.list<ILeague>('/leagues/public',
      ref => ref.limitToLast(10).orderByChild('isFull').equalTo(false))
      .valueChanges();
  }
  public JoinLeague(leagueInfo: IJoinLeague): Observable<boolean> {
    console.log('wrestling.JoinLeague');
    if (!leagueInfo.isPrivate) {
      const retVal = new Observable<boolean>(observer => {
        this.JoinLeague_API({
          leagueKey: leagueInfo.leagueKey,
          isPrivate: leagueInfo.isPrivate,
          leagueName: leagueInfo.leagueName,
          draftDate: leagueInfo.leagueDraftDate,
          creator: leagueInfo.leagueCreator,
          creatorId: leagueInfo.leagueCreatorId
        }, observer);
      });
      return retVal;
    } else {
      const retVal = new Observable<boolean>(observer => {
        this.af.list('/leagues/private',
          ref => ref.limitToLast(1).orderByChild('name').equalTo(leagueInfo.leagueName)
        ).valueChanges().take(1).subscribe(leagues => {
          if ((leagues.length === 0) || (leagues[0]['password'] !== leagueInfo.leaguePassword)) {
            observer.next(false);
            observer.complete();
          } else {
            this.JoinLeague_API({
              leagueKey: leagues[0]['$key'],
              isPrivate: true,
              leagueName: leagues[0]['name'],
              draftDate: leagues[0]['draftSetup'].draftDate,
              creator: leagues[0]['creator'],
              creatorId: leagues[0]['creatorId']
            }, observer);
          }
        });
      });
      return retVal;
    }
  }

  private JoinLeague_API(leagueInfo: IUserLeagueRef, observer: Observer<boolean>): void {
    console.log('wrestling.JoinLeague_API');
    this._store.select(fromRoot.getMyInfo).take(1).subscribe(myInfo => {
      const fork = leagueInfo.isPrivate ? 'private' : 'public';
      const leagueDB = this.af.object('/leagues/' + fork + '/' + leagueInfo.leagueKey);
      leagueDB.valueChanges().skipWhile(data => !data).take(1).subscribe(league => {
        const leagueUsers =
          this.af.list('/leagues/' + fork + '/' + leagueInfo.leagueKey + '/draftSetup/users');
        const count: number =
          (!league['draftSetup'] || !league['draftSetup'].users) ? 0 : Object.keys(league['draftSetup'].users).length;
        // DON'T ADD 1 because the prototype of the object is a key
        leagueUsers.push({
          id: myInfo.id,
          key: myInfo.$key,
          name: myInfo.name,
          email: myInfo.email,
          photo: myInfo.photo,
          draftOrder: count
        }).then(pushedInfo => {
          this._store.dispatch(new userActions.AddUserLeagueAction(leagueInfo));
          observer.next(true);
          observer.complete();
        });
        //   .catch(error => {
        //   console.warn(error);
        //   observer.next(false);
        //   observer.complete();
        // });
      });
    });
  }

  public CreateLeague_Public(league: ICreateLeague): Observable<boolean> {
    console.log('wrestling.CreateLeague_Public');
    return this.CreateLeague_Generic(league, false);
  }
  public CreateLeague_Private(league: ICreateLeague): Observable<boolean> {
    console.log('wrestling.CreateLeague_Private');
    return this.CreateLeague_Generic(league, true);
  }

  private CreateLeague_Generic(league: ICreateLeague, isPrivate: boolean): Observable<boolean> {
    console.log('wrestling.CreateLeague_Generic');
    const retVal = new Observable<boolean>(observer => {
      this._store.select(fromRoot.getMyInfo).take(1).subscribe(myInfo => {
        const newLeague = {
          name: league.name,
          numUsers: 1,
          maxUsers: league.maxUsers,
          isFull: false,
          createDate: league.createDate,
          isPrivate: isPrivate,
          draftSetup: {
            draftDate: Date.parse('March 14, 2017')
          },
          liveDraft: {
            numSelected: 0
          },
          password: league.password,
          creator: myInfo.name,
          creatorKey: myInfo.$key,
          creatorId: myInfo.id
        };
        const fork = isPrivate ? 'private' : 'public';
        const $leagues = this.af.list('/leagues/' + fork);
        $leagues.push(newLeague).then((createdLeague) => {
          const joinLeague: IJoinLeague = {
            leagueName: league.name,
            leaguePassword: league.password,
            leagueDraftDate: league.draftDate,
            leagueKey: createdLeague.key,
            leagueCreator: myInfo.name,
            leagueCreatorId: myInfo.id,
            isPrivate: isPrivate
          };
          this._store.dispatch(new leaguesActions.JoinLeagueAction(joinLeague));
          observer.next(true);
          observer.complete();
        });
        //   .catch(error => {
        //   observer.next(false);
        //   observer.complete();
        // });
      });
    });
    return retVal;
  }
}
