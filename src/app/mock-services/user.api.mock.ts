import { IUserLeagueRef } from './../league/models/user-league.interface';
import { IWrestler } from './../league/models/wrestlers.interface';
import { INewDraftUser, IDraftUser, IUserWithKey, IUser } from './../league/models/user.interface';
import { ILeague, IDraftSetup, ILiveDraft, IDraftedWrestler } from './../league/models/league.interface';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/zip';
import 'rxjs/add/operator/take';
import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { AppState } from '../league/store/wrestling.store';
import * as firebase from 'firebase/app';
import * as mockdb from './techfalling-2018.json';

@Injectable()
export class UserServiceMock {
  constructor(){   }

  public GetUserInformation(user: firebase.User): Observable<IUserWithKey> {
    return new Observable<IUserWithKey>(observer => {      
      const meWithKey: IUserWithKey = mockdb['users']['-Kf9gaVO_fteX1e2NtGG'] as IUserWithKey;
      meWithKey.$key = '-Kf9gaVO_fteX1e2NtGG';
      observer.next(meWithKey);
      observer.complete();
    });
  }
}
