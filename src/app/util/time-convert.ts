	export const MS_TO_DAYS: number = (1/(1000*60*60*24));
	export const MS_TO_HOURS: number = (1/(1000*60*60));
	export const MS_TO_MINUTES: number = (1/(1000*60));
	export const MS_TO_SECONDS: number = (1/(1000));
