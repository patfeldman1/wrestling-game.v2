import { IRulesResult } from './rules.class';
import { IWrestler } from '../league/models/wrestlers.interface';
import { IDraftedWrestler } from '../league/models/league.interface';
import { all_weights } from '../util/consts';

export enum RuleType {
	NoDuplicates,
	EleventhPick
}
export interface IRulesResult {
	passes: boolean;
	reasons: string[];
}
export class Rules {

	public static NumberOrRounds(rule: RuleType): number {
		switch (rule) {
			case RuleType.NoDuplicates:
				return 10;
			case RuleType.EleventhPick:
				return 11;
		}
	}
	public static CheckRule(rule: RuleType, input: { selection: IWrestler, team: IDraftedWrestler[] }): IRulesResult {

		switch (rule) {
			case RuleType.NoDuplicates:
				return Rules.NoDuplicates(input.selection, input.team);
			case RuleType.EleventhPick:
				return Rules.EleventhPick(input.selection, input.team);
		}
		return { passes: true, reasons: [] };
	}

	public static NoDuplicates(selection: IWrestler, team: IDraftedWrestler[]): IRulesResult {
		return {
			passes: team.filter(wrestler => wrestler.wrestlerWeight === selection.weight).length > 0,
			reasons: ['Only one wrestler per weight class.']
		};
	}
	public static EleventhPick(selection: IWrestler, team: IDraftedWrestler[]): IRulesResult {
		let haveExtra = false;
		const retVal: IRulesResult = {
			passes: true,
			reasons: []
		};
		all_weights.forEach(weight => {
			if (team.filter(wrestler => wrestler.wrestlerWeight === weight).length > 1) {
				haveExtra = true;
			}
		});
		if (!retVal.passes) { return retVal; }
		const possibleConflicts = team.filter(wrestler => wrestler.wrestlerWeight === selection.weight);
		if (possibleConflicts.length > 1) {
			retVal.passes = false;
			retVal.reasons = ['Already have too many at this weight.'];
		} else if (possibleConflicts.length > 0 && selection.rank <= 16 && possibleConflicts[0].wrestlerRank <= 16) {
			retVal.passes = false;
			retVal.reasons = ['Cannot select two ranked wrestlers at one weight.'];
		} else if (haveExtra && possibleConflicts.length > 0) {
			retVal.passes = false;
			retVal.reasons = ['Already have extra wrestler.'];
		}
		return retVal;
	}
}
