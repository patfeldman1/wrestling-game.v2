import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '../../material.module';
import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';
import { LookupRoutingModule } from './lookup-routing.module';
import { LookupComponent } from './lookup.component';
import { GameElementsModule } from '../../game/game-elements.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    LookupRoutingModule,
    GameElementsModule
  ],
  declarations: [
    LookupComponent
  ],
  providers: []
})
export class LookupModule { }
