import { OnChanges, Component, OnInit, ApplicationRef, ChangeDetectorRef } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { MatTabChangeEvent } from '@angular/material';
import { all_weights, all_years } from '../../util/consts';

@Component({
  selector: 'app-lookup',
  templateUrl: './lookup.component.html',
  styleUrls: ['./lookup.component.scss']
})
export class LookupComponent implements OnInit {
  years = all_years;
  weights = all_weights;
  selectedYear = '2018';
  selectedWeight = '125';
  selectedYearIndex = 5;
  selectedWeightIndex = 0;
  bracketType = 0;
  constructor() { }
  ngOnInit() {

  }
  setYear(year: string) { this.selectedYear = year; }
  setWeight(weight: string) { this.selectedWeight = weight; }
  setBracketType(type: number) { this.bracketType = type; }
  onYearClick(index: number) { this.setYear(this.years[index]); }
  onWeightClick(index: number) { this.setWeight(this.weights[index]); }
  onBracketTypeClick(index: number) { this.setBracketType(index); }
}
