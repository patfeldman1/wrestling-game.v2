import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../../core/route.service';
import { extract } from '../../core/i18n.service';
import { LookupComponent } from './lookup.component';

const routes: Routes = Route.withShell([
  { path: '', redirectTo: '/history', pathMatch: 'full' },
  { path: 'history', component: LookupComponent, data: { title: extract('NCAA History') } }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class LookupRoutingModule { }
