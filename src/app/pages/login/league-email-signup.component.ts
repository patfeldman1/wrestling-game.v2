import { Component, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { RootStoreState } from '../../reducers';
import * as authActions from '../../core/store/auth.store/auth.actions';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-email-signup-form',
  templateUrl: './league-email-signup.component.html',
  styleUrls: ['./league-email-signup.component.scss']
})
export class LeagueEmailSignupComponent implements OnInit {
  signupForm: FormGroup;
  detailForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<LeagueEmailSignupComponent>,
    private _store: Store<RootStoreState>
  ) { }

  ngOnInit() {
    // First Step
    this.signupForm = this.formBuilder.group({
      'email': ['', [
        Validators.required,
        Validators.email
      ]],
      'password': ['', [
        Validators.pattern('^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$'),
        Validators.minLength(6),
        Validators.maxLength(25),
        Validators.required
      ]],
      'region': ['', []],
    });

    // Second Step
    this.detailForm = this.formBuilder.group({
      'catchPhrase': ['', [Validators.required]]
    });

  }

  // Using getters will make your code look pretty
  get email() { return this.signupForm.get('email'); }
  get password() { return this.signupForm.get('password'); }

  // Step 1
  signup() {
    this.dialogRef.close();
    this._store.dispatch(new authActions.CreateUserEmailAction({
      email: this.email.value,
      password: this.password.value
    }));
  }
}
