import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Route } from '../../core/route.service';
import { extract } from '../../core/i18n.service';
import { LeagueLoginComponent } from './league-login.component';

const routes: Routes = Route.withShell([
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LeagueLoginComponent, data: { title: extract('Login') } }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class LeagueLoginRoutingModule { }
