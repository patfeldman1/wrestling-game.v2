import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '../../material.module';
import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';
import { LeagueLoginComponent } from './league-login.component';
import { LeagueLoginRoutingModule } from './league-login-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { LeagueEmailSignupComponent } from './league-email-signup.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    BrowserModule,
    ReactiveFormsModule,
    LeagueLoginRoutingModule
  ],
  declarations: [
    LeagueLoginComponent,
    LeagueEmailSignupComponent
  ],
  entryComponents: [
    LeagueEmailSignupComponent
  ],
  providers: []
})
export class LeagueLoginModule { }
