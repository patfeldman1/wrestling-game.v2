import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import 'rxjs/add/operator/first';
import { Logger } from '../../core/logger.service';
import { environment } from '../../../environments/environment';
import { RootStoreState } from '../../reducers/index';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import * as authActions from '../../core/store/auth.store/auth.actions';
import * as fromRoot from '../../reducers';
import { Subscription } from 'rxjs/Subscription';
import { MatDialog } from '@angular/material';
import { LeagueEmailSignupComponent } from './league-email-signup.component';
import { ELogin } from '../../core/models/login.enum';
const log = new Logger('Login');

@Component({
  selector: 'app-league-login',
  templateUrl: './league-login.component.html',
  styleUrls: ['./league-login.component.scss']
})
export class LeagueLoginComponent implements OnInit, OnDestroy {
  error: string;
  loginForm: FormGroup;
  isLoading = true;
  $loginSubscriptionRef: Subscription;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private _store: Store<RootStoreState>
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.$loginSubscriptionRef = this._store.select(fromRoot.getLoginState).subscribe(loginState => {
      switch (loginState) {
        case ELogin.LoggedIn: {
          this.router.navigate(['/leagues']);
          break;
        }
        case ELogin.Attempting: {
          this.error = '';
          this.isLoading = true;
          break;
        }
        case ELogin.Error: {
          this.error = 'Invalid Email';
          this.isLoading = false;
          break;
        }
        case ELogin.NoUser: {
          this.error = '';
          this.isLoading = false;
          break;
        }
        default: {
          this.error = '';
          this.isLoading = true;
          break;
        }
      }
    });

  }

  ngOnDestroy() {
    this.$loginSubscriptionRef.unsubscribe();
  }

  loginWithFacebook() {
    this.isLoading = true;
    this._store.dispatch(new authActions.LoginUserFacebookAction());
  }
  loginWithGoogle() {
    this.isLoading = true;
    this._store.dispatch(new authActions.LoginUserGoogleAction());
  }
  login() {
    this.isLoading = true;
    this.error = null;
    this._store.dispatch(new authActions.LoginUserEmailAction({
      email: this.loginForm.value.username,
      password: this.loginForm.value.password
    }));
  }


  forgotPassword() {
    alert('FUCK YOU! I can\'t do everything');
  }
  signUp() {
    this.isLoading = true;
    const dialogRef = this.dialog.open(LeagueEmailSignupComponent, {
      width: '350px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
      } else {
      }
    });
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      remember: true
    });
  }

}
