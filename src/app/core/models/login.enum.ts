export enum ELogin {
  NotSet,
  Attempting,
  NoUser,
  LoggedIn,
  Error
}

export interface UserSignup {
  uid: string;
  email: string;
  photoURL?: string;
}
