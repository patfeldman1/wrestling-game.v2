export interface IUpdateBracket {
    brackets: dictionary<IBracketSeeds>;
}

export interface IBracketSeeds {
    seeds: dictionary<IWrestlerBracketUpdate>;
}

export interface IWrestlerBracketUpdate {
    id: string;
    cpw: number;
    cw: number;
    w: number;
    pw: number;
    plw: number;
}
