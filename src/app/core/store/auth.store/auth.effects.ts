import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as firebase from 'firebase/app';
import * as authActions from './auth.actions';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { RootStoreState } from '../../../reducers';


@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private afDb: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    private _store: Store<RootStoreState>
  ) {
    this.afAuth.authState.subscribe(firebaseUser => {
      this._store.dispatch(new authActions.UserLoggedInAction(firebaseUser));
    });
  }

  @Effect({ dispatch: false })
  facebookLogin$ = this.actions$
    .ofType(authActions.ActionTypes.LOGIN_USER_FACEBOOK)
    .map(action => {
      this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
        .then(newAuthState => { console.log('SUCCESS Facebook login'); })
        .catch(failure => { console.log('FAILED Facebook LOGIN'); });
    });

  @Effect({ dispatch: false })
  googleLogin$ = this.actions$
    .ofType(authActions.ActionTypes.LOGIN_USER_GOOGLE)
    .map(query => {
      this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
        .then(newAuthState => { console.log('SUCCESS G+ login'); })
        .catch(failure => { console.log('FAILED GOOGLE LOGIN'); });
    });

  @Effect({ dispatch: false })
  emailLogin$ = this.actions$
    .ofType(authActions.ActionTypes.LOGIN_USER_EMAIL)
    .map(userData => {
      this.afAuth.auth.signInWithEmailAndPassword(userData['payload'].email, userData['payload'].password)
        .then(newAuthState => { console.log('SUCCESS EMAIL login'); })
        .catch(failure => { this._store.dispatch(new authActions.LoginUserEmailFailedAction()); });
    });

  @Effect({ dispatch: false })
  emailSignup$ = this.actions$
    .ofType(authActions.ActionTypes.CREATE_USER_EMAIL)
    .map(userData => {
      this.afAuth.auth.createUserWithEmailAndPassword(userData['payload'].email, userData['payload'].password)
        .then(newAuthState => { console.log('SUCCESS EMAIL CREATION'); })
        .catch(failure => { this._store.dispatch(new authActions.LoginUserEmailFailedAction()); });
    });


  @Effect({ dispatch: false })
  logout$ = this.actions$
    .ofType(authActions.ActionTypes.LOGOUT_USER)
    .map(query => { this.afAuth.auth.signOut(); });
}
