import * as actions from './auth.actions';
import * as _ from 'lodash';
import * as firebase from 'firebase/app';
import { ELogin } from '../../models/login.enum';
export interface AppState {
  loginState: ELogin;
  user: firebase.User;
}

const initialState: AppState = {
  loginState: ELogin.NotSet,
  user: null
};
export function reducer(state: AppState = initialState, action: actions.Actions): AppState {
  switch (action.type) {
    case actions.ActionTypes.LOGIN_USER_EMAIL_FAILED: {
      return Object.assign({}, state, {
        loginState: ELogin.Error
      });
    }
    case actions.ActionTypes.LOGIN_USER_EMAIL ||
         actions.ActionTypes.LOGIN_USER_FACEBOOK ||
         actions.ActionTypes.CREATE_USER_EMAIL ||
         actions.ActionTypes.LOGIN_USER_GOOGLE : {
          return Object.assign({}, state, {loginState: ELogin.Attempting});
    }
    case actions.ActionTypes.USER_LOGGED_IN: {
      if (action.payload as firebase.User) {
        return Object.assign({}, state, {
          user: action.payload,
          loginState: ELogin.LoggedIn
        });
      } else {
        return Object.assign({}, initialState, {
          loginState: ELogin.NoUser
        });
      }
    }
    default:
      return state;
  }
}

export const getLoginState = (state: AppState) => state.loginState;
export const getFirebaseUser = (state: AppState) => state.user;
export const getFirebaseUserId = (state: AppState) => state.user.uid;
export const getFirebaseUserDisplayName = (state: AppState) => {
  if (!state.user) { return ''; }
  if (!!state.user.displayName) { return state.user.displayName; }
  return state.user.email.split('@')[0];
};
export const getFirebaseUserPhotoUrl = (state: AppState) => state.user.photoURL;
