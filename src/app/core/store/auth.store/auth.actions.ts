import { Action } from '@ngrx/store';
import { type } from '../../../util/type';
import * as firebase from 'firebase/app';
import { UserSignup } from '../../models/login.enum';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export const ActionTypes = {
  LOGIN_USER_FACEBOOK: type('[AUTH] Login User Facebook'),
  LOGIN_USER_GOOGLE: type('[AUTH] Login User Google'),
  LOGIN_USER_EMAIL: type('[AUTH] Login User Email'),
  LOGIN_USER_EMAIL_FAILED: type('[AUTH] Login User Email Failed'),
  CREATE_USER_EMAIL: type('[AUTH] Create User Email'),
  LOGOUT_USER: type('[AUTH] Logout User'),
  USER_LOGGED_IN: type('[AUTH] Logged In'),
};

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions =
  LoginUserGoogleAction |
  LoginUserFacebookAction |
  LoginUserEmailAction |
  LoginUserEmailFailedAction |
  CreateUserEmailAction |
  UserLoggedInAction |
  LogoutUserAction;

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
export class LoginUserFacebookAction implements Action {
  type = ActionTypes.LOGIN_USER_FACEBOOK;
  constructor(public payload: boolean = false) { }
}

export class LoginUserGoogleAction implements Action {
  type = ActionTypes.LOGIN_USER_GOOGLE;
  constructor(public payload: boolean = false) { }
}

export class LoginUserEmailAction implements Action {
  type = ActionTypes.LOGIN_USER_EMAIL;
  constructor(public payload: { email: string, password: string }) { } // TODO set up email
}

export class LoginUserEmailFailedAction implements Action {
  type = ActionTypes.LOGIN_USER_EMAIL_FAILED;
  constructor(public payload: boolean = false) { }
}

export class CreateUserEmailAction implements Action {
  type = ActionTypes.CREATE_USER_EMAIL;
  constructor(public payload: { email: string, password: string }) { } // TODO set up email
}

export class LogoutUserAction implements Action {
  type = ActionTypes.LOGOUT_USER;
  constructor(public payload: boolean = false) { }
}

export class UserLoggedInAction implements Action {
  type = ActionTypes.USER_LOGGED_IN;
  constructor(public payload: firebase.User) { }
}
