import { LayoutActionTypes, LayoutActions } from './layout.actions';

export interface AppState {
  showSidenav: boolean;
}

const initialState: AppState = {
  showSidenav: false,
};

export function reducer(
  state: AppState = initialState,
  action: LayoutActions
): AppState {
  switch (action.type) {
    case LayoutActionTypes.CloseSidenav:
      return {
        showSidenav: false,
      };

    case LayoutActionTypes.OpenSidenav:
      return {
        showSidenav: true,
      };

    default:
      return state;
  }
}

export const getShowSidenav = (state: AppState) => state.showSidenav;
