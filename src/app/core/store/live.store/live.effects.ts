import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as firebase from 'firebase/app';
import * as liveActions from './live.actions';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { RootStoreState } from '../../../reducers';


@Injectable()
export class LiveEffects {
  constructor(
    private actions$: Actions,
    private afDb: AngularFireDatabase,
    private _store: Store<RootStoreState>
  ) {
    this._store.dispatch(new liveActions.LiveUpdateBracketAction());
  }

  @Effect()
  FU$ = this.actions$
    .ofType(liveActions.ActionTypes.LIVE_BRACKETS)
    .map(action => {
      console.log('PLEASE');
    });
}
