import { Action } from '@ngrx/store';
import { type } from '../../../util/type';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export const ActionTypes = {
  LIVE_BRACKETS: type('[LIVE] Update Weight'),
  LIVE_BRACKETS_COMPLETE: type('[LIVE] Update Weight Complete')
};

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions =
  LiveUpdateBracketAction |
  LiveUpdateBracketCompleteAction;

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
export class LiveUpdateBracketAction implements Action {
  type = ActionTypes.LIVE_BRACKETS;
  constructor(public payload: boolean = false) { }
}
export class LiveUpdateBracketCompleteAction implements Action {
  type = ActionTypes.LIVE_BRACKETS_COMPLETE;
  constructor(public payload: {}) { }
}
