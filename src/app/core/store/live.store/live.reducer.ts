import * as _ from 'lodash';
import * as firebase from 'firebase/app';
import { ELogin } from '../../models/login.enum';
import { all_weights } from '../../../util/consts';
import * as actions from './live.actions';
import { IUpdateBracket } from '../../models/bracket.model';

export interface AppState {
  brackets: IUpdateBracket;
}

const initialState: AppState = {
  brackets: { brackets: {}}
};
export function reducer(state: AppState = initialState, action: actions.Actions): AppState {
  switch (action.type) {
    case actions.ActionTypes.LIVE_BRACKETS_COMPLETE: {

      return Object.assign({}, state, {
        brackets : action.payload
      });
    }
    default:
      return state;
  }
}

export const getLiveBrackets = (state: AppState) => state.brackets;
