import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { ELogin } from '../models/login.enum';
import { RootStoreState } from '../../reducers/index';
import * as fromRoot from '../../reducers';

@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(public _store: Store<RootStoreState>, public router: Router) { }

    canActivate(): Observable<boolean> {
        return this
            ._store
            .select(fromRoot.getLoginState)
            .map(authState => {
                if (authState === ELogin.LoggedIn) {
                    return true;
                } else {
                    this.router.navigate(['/login']);
                    return false;
                }
            });
    }
}
