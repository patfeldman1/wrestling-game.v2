import { Title } from '@angular/platform-browser';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MatSidenav } from '@angular/material';

import { I18nService } from '../../i18n.service';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as fromRoot from '../../../reducers/index';
import * as authActions from '../../store/auth.store/auth.actions';
import { ELogin } from '../../models/login.enum';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() sidenav: MatSidenav;
  $isLoggedIn: Observable<boolean>;
  $displayName: Observable<string>;

  constructor(
    private router: Router,
    private titleService: Title,
    private i18nService: I18nService,
    private _store: Store<fromRoot.RootStoreState>
  ) { }

  ngOnInit() {
    this.$isLoggedIn = this._store.select(fromRoot.getLoginState).map(state => state === ELogin.LoggedIn);
    this.$displayName = this._store.select(fromRoot.getUserDisplayName);
  }

  setLanguage(language: string) {
    this.i18nService.language = language;
  }

  logout() {
    this._store.dispatch(new authActions.LogoutUserAction());
    this.router.navigate(['/login']);
  }

  login() {
    this.router.navigate(['/login']);
  }

  get currentLanguage(): string {
    return this.i18nService.language;
  }

  get languages(): string[] {
    return this.i18nService.supportedLanguages;
  }

  get username(): string {
    return 'NOBODY';
  }

  get title(): string {
    return this.titleService.getTitle();
  }

}
