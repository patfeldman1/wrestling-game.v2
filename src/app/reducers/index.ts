import {
  ActionReducerMap,
  createSelector,
  createFeatureSelector,
  ActionReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import { RouterStateUrl } from '../shared/utils';
import * as fromRouter from '@ngrx/router-store';

/**
 * storeFreeze prevents state from being mutated. When mutation occurs, an
 * exception will be thrown. This is useful during development mode to
 * ensure that none of the reducers accidentally mutates the state.
 */
import { storeFreeze } from 'ngrx-store-freeze';

/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */

import * as fromLayout from '../core/store/layout.store/layout.reducer';
import * as fromAuth from '../core/store/auth.store/auth.reducer';
import * as fromLive from '../core/store/live.store/live.reducer';
import { createSecureContext } from 'tls';

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface RootStoreState {
  layout: fromLayout.AppState;
  auth: fromAuth.AppState;
  live: fromLive.AppState;
  routerReducer: fromRouter.RouterReducerState<RouterStateUrl>;
}

/**
 * Our state is composed of a map of action reducer functions.
 * These reducer functions are called with each dispatched action
 * and the current or initial state and return a new immutable state.
 */
export const reducers: ActionReducerMap<RootStoreState> = {
  layout: fromLayout.reducer,
  auth: fromAuth.reducer,
  live: fromLive.reducer,
  routerReducer: fromRouter.routerReducer,
};

// console.log all actions
export function logger(reducer: ActionReducer<RootStoreState>): ActionReducer<RootStoreState> {
  return function (state: RootStoreState, action: any): RootStoreState {
    console.log('state', state);
    console.log('action', action);

    return reducer(state, action);
  };
}

/**
 * Layout Reducers
 */
export const getLayoutState = createFeatureSelector<fromLayout.AppState>('layout');
export const getAuthState = createFeatureSelector<fromAuth.AppState>('auth');
export const getLiveState = createFeatureSelector<fromLive.AppState>('live');

export const getShowSidenav = createSelector(getLayoutState, fromLayout.getShowSidenav);
export const getLoginState = createSelector(getAuthState, fromAuth.getLoginState);
export const getUserDisplayName = createSelector(getAuthState, fromAuth.getFirebaseUserDisplayName);
export const getCurrentUser = createSelector(getAuthState, fromAuth.getFirebaseUser);

export const getLiveBrackets = createSelector(getLiveState, fromLive.getLiveBrackets);
