import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeagueBaseComponent } from './pages/base/league-base.component';
import { extract } from '../core/i18n.service';
import { Route } from '../core/route.service';
import { AuthGuardService } from '../core/services/auth.guard';
import { LeagueHomeComponent } from './pages/league-home/league-home.component';
// import { AuthOutGuardService } from "./services/auth-out.guard";


const routes: Routes = Route.withShell([
  { path: '', redirectTo: '/leagues', pathMatch: 'full' },
  {
    path: 'leagues',
    component: LeagueBaseComponent,
    data: { title: extract('Fantasy Leagues') },
    canActivate: [AuthGuardService]
  },
  {
    path: 'leaguedraft',
    component: LeagueHomeComponent,
    data: { title: extract('League Home') },
    canActivate: [AuthGuardService]
  }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class LeagueRoutingModule { }
