import { IUserLeagueRef } from './../models/user-league.interface';
import { IWrestler } from './../models/wrestlers.interface';
import { INewDraftUser, IDraftUser, IUserWithKey, IUser } from './../models/user.interface';
import { ILeague, IDraftSetup, ILiveDraft, IDraftedWrestler } from './../models/league.interface';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/zip';
import 'rxjs/add/operator/take';
import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { AppState } from '../store/wrestling.store';
import * as firebase from 'firebase/app';


@Injectable()
export class UserService {
  constructor(
    private afDb: AngularFireDatabase,
    private _store: Store<AppState>
  ) { }

  public GetUserInformation(user: firebase.User): Observable<IUserWithKey> {
    return new Observable<IUserWithKey>(observer => {
        this.afDb
          .list('/users', ref => ref.limitToLast(1).orderByChild('id').equalTo(user.uid))
          .snapshotChanges()
          .map(actions =>
            actions.map(snapshotAction => ({ $key: snapshotAction.key, ...snapshotAction.payload.val() }))
          )
          .first()
          .subscribe(dbUser => {
            if (dbUser.length === 0) {
              let myName = user.displayName;
              myName = (!!myName ? myName : user.email.split('@')[0]);
              const me: IUser = {
                id: user.uid,
                leagues_ref: {},
                highlights: [],
                name: myName,
                photo: user.photoURL,
                email: user.email
              };
              const users = this.afDb.list('/users');
              users.push(me).then((item) => {
                const meWithKey: IUserWithKey = {
                  id: me.id,
                  leagues_ref: me.leagues_ref,
                  highlights: me.highlights,
                  name: me.name,
                  photo: me.photo,
                  email: me.email,
                  $key: item.key
                };
                observer.next(meWithKey);
                observer.complete();
              });
            } else {
                const meWithKey = dbUser[0] as IUserWithKey;
                observer.next(meWithKey);
                observer.complete();
            }
          });
    });
  }

}
