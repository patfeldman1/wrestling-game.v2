import { WrestlerSelectComponent } from './../../dialogs/wrestler-select/wrestler-select.component';
import { Directive, ElementRef, HostListener, Renderer2, Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { WrestlerInfo } from '../../../game/models/wrestler-info.model';
import { TeamInfoDialog } from '../../dialogs/team-info.dialog/team-info.dialog';
@Component({
    selector: 'app-league-team-link',
    templateUrl: './league-team.link.html',
    styleUrls: ['./league-team.link.scss']
})
// tslint:disable-next-line:component-class-suffix
export class LeagueTeamLink {
    @Input() teamId: string;
    constructor(public dialog: MatDialog) {}
    @HostListener('click') onClick(btn: any) {
        const dialogRef = this.dialog.open(TeamInfoDialog, {
            width: '390px',
            maxHeight: '90vh',
            data: { teamId: this.teamId }
        });
    }
}
