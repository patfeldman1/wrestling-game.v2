import { WrestlingLookupService } from './../../../game/services/wrestling-lookup.service';
import { Component, OnInit } from '@angular/core';
import { Input, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-league-bracket-dialog',
    templateUrl: './league-bracket.dialog.html',
    styleUrls: ['./league-bracket.dialog.scss']
})
export class LeagueBracketDialogComponent implements OnInit {
    weight: string;
    constructor(
        public dialogRef: MatDialogRef<LeagueBracketDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
    ) {
        this.weight = data['weight'];
    }
    ngOnInit() {}
    closeDialog() { this.dialogRef.close(); }
}



