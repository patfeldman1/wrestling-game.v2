import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: 'confirm.dialog.html',
  styleUrls: ['./confirm.dialog.scss']
})
export class ConfirmDialog {
  wrestlerName: string;
  userName: string = null;
  myName: string;
  constructor(
    public dialogRef: MatDialogRef<ConfirmDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.wrestlerName = data['wrestlerName'];
    if (data['userName']) {
        this.userName = data['userName'];
    }
  }

  close(confirm: boolean): void {
    this.dialogRef.close(confirm);
  }

}
