import { WrestlerInfo } from './../../../game/models/wrestler-info.model';
import { WrestlerLinkComponent } from './../../../game/components/wrestler-link/wrestler-link.component';
import { all_weights } from './../../../util/consts';
import { IDraftUser } from './../../models/user.interface';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, Input, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Store } from '@ngrx/store';
import * as fromLeague from '../../store/wrestling.store';
import * as _ from 'lodash';
import { WrestlingLookupService } from '../../../game/services/wrestling-lookup.service';

@Component({
  selector: 'app-team-info-dialog',
  templateUrl: './team-info.dialog.html',
  styleUrls: ['./team-info.dialog.scss']
})
// tslint:disable-next-line:component-class-suffix
export class TeamInfoDialog implements OnInit {
  teamId: string;
  constructor(
    private _store: Store<fromLeague.AppState>,
    public dialogRef: MatDialogRef<TeamInfoDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.teamId = data['teamId'];
  }
  ngOnInit() {
  }
}
