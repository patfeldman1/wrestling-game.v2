import { IDraftUser } from './../../models/user.interface';
import { Store } from '@ngrx/store';
import { ConfirmationDialog } from './../confirmation.dialog/confirmation.dialog';
import { Component, OnInit } from '@angular/core';
import { Input, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { indexDebugNode } from '@angular/core/src/debug/debug_node';
import * as fromLeague from '../../store/wrestling.store';
import * as leagueActions from '../../store/leagues.store/leagues.actions';

@Component({
    selector: 'app-remove-drafter-dialog',
    templateUrl: './remove-drafter.dialog.html',
    styleUrls: ['./remove-drafter.dialog.scss']
})
export class RemoveDrafterDialogComponent implements OnInit {
    users: IDraftUser[];
    constructor(
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<RemoveDrafterDialogComponent>,
        private _store: Store<fromLeague.AppState>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.users = data['users'];
    }
    ngOnInit() {}
    closeDialog() {
        this.dialogRef.close();
    }

    removeDrafter(indexToRemove: number) {
        const user = this.users[indexToRemove];
        const questionStr =
            'Do you want to remove ' + user.name +
            ' from this draft? \n(NOTE: They will still be able to watch the draft and follow results)';
        const dialogRef = this.dialog.open(ConfirmationDialog, {
            width: '300px',
            height: '250px',
            data: { title: 'Remove Drafter', question: questionStr }
          });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this._store.dispatch(new leagueActions.RemoveDrafterAction(user));
                this.dialogRef.close();
            } else {
                this.dialogRef.close();
            }
        });

    }
}



