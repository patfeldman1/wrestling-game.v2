import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: 'confirmation.dialog.html',
  styleUrls: ['./confirmation.dialog.scss']
})
export class ConfirmationDialog {
  question: string;
  title: string;
  constructor(
    public dialogRef: MatDialogRef<ConfirmationDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.question = data['question'];
    this.title = data['title'];
  }

  close(confirm: boolean): void {
    this.dialogRef.close(confirm);
  }

}
