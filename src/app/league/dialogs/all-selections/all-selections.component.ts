import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import * as fromLeagues from '../../store/wrestling.store';
import { Observable } from 'rxjs/Observable';
import { IWrestler } from '../../models/wrestlers.interface';
import { IDraftedWrestler } from '../../models/league.interface';
import { ISubscription } from 'rxjs/Subscription';
import { IWrestlerHighlight } from '../../models/highlights.class';
import { WrestlingLookupService } from '../../../game/services/wrestling-lookup.service';
import { WrestlerInfo } from '../../../game/models/wrestler-info.model';
import { MatDialog } from '@angular/material';
import { WrestlerSelectComponent } from '../wrestler-select/wrestler-select.component';
import { all_weights } from '../../../util/consts';

@Component({
  selector: 'app-all-selections',
  templateUrl: './all-selections.component.html',
  styleUrls: ['./all-selections.component.scss']
})
export class AllSelectionsComponent implements OnInit {
  allWeights = all_weights;
  constructor(
    public route: ActivatedRoute,
    public _store: Store<fromLeagues.AppState>,
    public wrestlerLookup: WrestlingLookupService,
    public dialog: MatDialog
  ) { }
  ngOnInit() { }

  onSelect(wrestler: WrestlerInfo) {
    const dialogRef = this.dialog.open(WrestlerSelectComponent, {
      width: '360px',
      height: '60vh',
      data: { wrestlerId: wrestler.global_pid }
    });

  }
}
