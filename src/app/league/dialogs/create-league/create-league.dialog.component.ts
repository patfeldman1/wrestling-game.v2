import { Component, OnInit } from '@angular/core';
import { Input, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-create-league-dialog',
    templateUrl: './create-league.dialog.component.html',
    styleUrls: ['./create-league.dialog.component.scss']
})
export class CreateLeagueDialogComponent implements OnInit {
    specifiedTabIndex = 0;
    constructor(
        public dialogRef: MatDialogRef<CreateLeagueDialogComponent>,
        private _formBuilder: FormBuilder,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.specifiedTabIndex = data['tabType'] === 'create' ? 1 : 0;
    }
    ngOnInit() {}
    createLeague() {}
    closeDialog() {
        this.dialogRef.close();
    }
}



