import { WrestlerInfo } from './../../../game/models/wrestler-info.model';
import { WrestlerLinkComponent } from './../../../game/components/wrestler-link/wrestler-link.component';
import { all_weights } from './../../../util/consts';
import { IDraftUser } from './../../models/user.interface';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Store } from '@ngrx/store';
import * as fromLeague from '../../store/wrestling.store';
import * as _ from 'lodash';
import { WrestlingLookupService } from '../../../game/services/wrestling-lookup.service';

@Component({
  selector: 'app-all-teams-dialog',
  templateUrl: './all-teams.dialog.html',
  styleUrls: ['./all-teams.dialog.scss']
})
export class AllTeamsDialogComponent implements OnInit {
  $draftedTeams: Observable<{}>;
  $teams: Observable<IDraftUser[]>;
  $leagueName: Observable<string>;
  all_weights = all_weights;
  constructor(
    private _store: Store<fromLeague.AppState>,
    private wrestlerLookup: WrestlingLookupService,
    public dialogRef: MatDialogRef<AllTeamsDialogComponent>,
  ) { }
  ngOnInit() {
    this.$leagueName = this._store.select(fromLeague.getCurrentLeague).map(league => league.name);
    this.$teams = this._store
      .select(fromLeague.getDraftUsers)
      .map(dict => Object.values(dict).sort((a, b) => a.draftOrder - b.draftOrder));

    this.$draftedTeams = this._store.select(fromLeague.getLiveDraftedTeams).map(teams => {
      const table = {};
      _.forEach(teams, (value, key) => {
        value.forEach(wrestler => {
          if (!(wrestler.draftedByKey in table)) {
            table[wrestler.draftedByKey] = {};
          }
          table[wrestler.draftedByKey][wrestler.wrestlerWeight] =
            this.wrestlerLookup.getWrestlerInfo(wrestler.wrestlerId);
        });
      });
      return table;
    });
  }

  getWrestler(team: IDraftUser, weight: string, table: {}): WrestlerInfo {
    return (team.key in table && weight in table[team.key]) ? table[team.key][weight] : {};
  }
  closeDialog() {
    this.dialogRef.close();
  }
}
