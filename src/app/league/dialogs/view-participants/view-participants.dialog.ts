import { Component, OnInit } from '@angular/core';
import { Input, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-view-participants-dialog',
    templateUrl: './view-participants.dialog.html',
    styleUrls: ['./view-participants.dialog.scss']
})
export class ViewParticipantsDialogComponent implements OnInit {
    names: Array<string>;
    constructor(
        public dialogRef: MatDialogRef<ViewParticipantsDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        this.names = data['names'];
    }
    ngOnInit() {}
    closeDialog() {
        this.dialogRef.close();
    }
}



