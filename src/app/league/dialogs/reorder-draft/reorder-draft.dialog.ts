import { Component, OnInit } from '@angular/core';
import { Input, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromLeagues from '../../store/wrestling.store';
import * as draftActions from '../../store/draft.store/draft.actions';
import { ILeague } from '../../models/league.interface';

@Component({
    selector: 'app-reorder-draft-dialog',
    templateUrl: './reorder-draft.dialog.html',
    styleUrls: ['./reorder-draft.dialog.scss']
})
export class ReorderDraftDialogComponent implements OnInit {
    names: Array<string>;
    league: ILeague;
    constructor(
        public dialogRef: MatDialogRef<ReorderDraftDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        public _store: Store<fromLeagues.AppState>
    ) {
        this.names = data['names'];
        this.league = data['league'];
    }
    ngOnInit() {}
    saveSetup() {
        this._store.dispatch(new draftActions.UpdateDraftOrderAction({
            names: this.names,
            league: this.league
        }));
        this.dialogRef.close();
    }
}



