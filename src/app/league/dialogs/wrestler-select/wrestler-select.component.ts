import { getCurrentDraftTeam } from './../../store/wrestling.store';
import { WrestlingLookupService } from './../../../game/services/wrestling-lookup.service';
import { Component, OnInit, Inject } from '@angular/core';
import { Input } from '@angular/core';
import { WrestlerHistory } from '../../../game/models/wrestler-history.model';
import { WrestlerInfo } from '../../../game/models/wrestler-info.model';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import * as fromLeagues from '../../store/wrestling.store';
import * as draftActions from '../../store/draft.store/draft.actions';
import { IUser, IDraftUser, IUserWithKey } from '../../models/user.interface';
import { ConfirmDialog } from '../confirm/confirm.dialog';

@Component({
  selector: 'app-wrestler-select',
  templateUrl: './wrestler-select.component.html',
  styleUrls: ['./wrestler-select.component.scss']
})
export class WrestlerSelectComponent implements OnInit {
  wrestlerId: string;
  wrestler: WrestlerInfo;
  goToMatches: boolean;
  $isMyTurn: Observable<boolean>;
  $isAdmin: Observable<boolean>;
  $selectionUser: Observable<IDraftUser>;
  $myInfo: Observable<IUserWithKey>;
  $selectedWrestlers: Observable<string[]>;
  $currentTeamWeights: Observable<string[]>;

  constructor(
    public dialogRef: MatDialogRef<WrestlerSelectComponent>,
    public dialog: MatDialog,
    public _store: Store<fromLeagues.AppState>,
    private wrestlingLookupApi: WrestlingLookupService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.wrestlerId = data['wrestlerId'];
    this.wrestler = this.wrestlingLookupApi.getWrestlerInfo(this.wrestlerId);
  }
  ngOnInit() {
    this.$isAdmin = this._store.select(fromLeagues.isCurrentLeagueAdmin);
    this.$isMyTurn = this._store.select(fromLeagues.isMyTurn);
    this.$selectionUser = this._store.select(fromLeagues.getCurrentDraftUser);
    this.$myInfo = this._store.select(fromLeagues.getMyInfo);
    this.$selectedWrestlers = this._store.select(fromLeagues.getSelectedWrestlers).map(dict => Object.keys(dict));
    this.$currentTeamWeights = this._store
      .select(fromLeagues.getCurrentDraftTeam)
      .map(drafted => (!!drafted) ? drafted.map(wrestler => wrestler.wrestlerWeight) : []);
  }

  selectForMe(myName: string): void {
    const dialogRef = this.dialog.open(ConfirmDialog, {
      width: '250px',
      data: { wrestlerName: this.wrestler.last_name, myName: myName }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._store.dispatch(new draftActions.SelectWrestlerAction({
          id: this.wrestler.wrestler_id,
          weight: this.wrestler.weight,
          rank: this.wrestler.seed
        }));

        this.dialogRef.close();
      } else {
        this.dialogRef.close();
      }
    });
  }

  selectForUser(userName: string, myName: string) {
    const dialogRef = this.dialog.open(ConfirmDialog, {
      width: '250px',
      data: { wrestlerName: this.wrestler.last_name, myName: myName, userName: userName }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this._store.dispatch(new draftActions.SelectWrestlerToCurrentTeamAction({
          id: this.wrestler.wrestler_id,
          weight: this.wrestler.weight,
          rank: this.wrestler.seed
        }));
        this.dialogRef.close();
      } else {
        this.dialogRef.close();
      }
    });

  }
}
