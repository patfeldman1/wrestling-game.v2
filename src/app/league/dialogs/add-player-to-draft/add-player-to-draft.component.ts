import { Component, OnInit, Input, EventEmitter, Output, OnDestroy, Inject } from '@angular/core';
import { ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import * as draftActions from '../../store/draft.store/draft.actions';
import * as fromLeagues from '../../store/wrestling.store';
import { IJoinLeagueStatus, eJoinStatus } from '../../models/league-join.interface';
import { Subscription } from 'rxjs/Subscription';
import { ILeague } from '../../models/league.interface';

@Component({
  selector: 'app-add-player-to-draft',
  templateUrl: './add-player-to-draft.component.html',
  styleUrls: ['./add-player-to-draft.component.scss']
})
export class AddPlayerToDraftComponent implements OnInit {
  addPlayerForm: FormGroup;
  league: ILeague;
  names: Array<String>;

  constructor(
    private formBuilder: FormBuilder,
    private _store: Store<fromLeagues.AppState>,
    public dialogRef: MatDialogRef<AddPlayerToDraftComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data['league']) {
        this.league = data['league'];
    }

    if (data['names']) {
      this.names = data['names'];
    }

    // First Step
    this.addPlayerForm = this.formBuilder.group({
      'playerName': ['', [
        Validators.minLength(3),
        Validators.maxLength(25),
        Validators.required
      ]],
    });
  }

  ngOnInit() {}

  // Using getters will make your code look pretty
  get playerName() { return this.addPlayerForm.get('playerName'); }

  // Step 1
  addPlayer() {
    this._store.dispatch(new draftActions.AddParticipantToDraftAction({
        league: this.league,
        name: this.playerName.value
    }));
    this.dialogRef.close();
  }
}
