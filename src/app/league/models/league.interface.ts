import { IDraftUser } from './user.interface';
export interface ILeague {
  $key: string;
  name: string;
  creator: string;
  creatorId: string;
  numUsers: number;
  maxUsers: number;
  isFull: boolean;
  createDate: number;
  password: string;
  isPrivate: boolean;
  draftSetup: IDraftSetup;
  liveDraft: ILiveDraft;
  leagueKey?: string;
}

export interface ILiveDraft {
  draftedParticipants: IDraftedWrestler[];
  draftedTeams: dictionary<IDraftedWrestler[]>;
  numSelected: number;
}
// export interface IDraftedWrestlerRefArray{
//   [userKey: string]: IDraftedWrestler[];
// }
export interface IDraftSetup {
  users: dictionary<IDraftUser>;
  draftDate: number;
  draftComplete: boolean;
}
// export interface IDraftUserRefArray{
//   [userKey: string]: IDraftUser;
// }
export interface IDraftedWrestler {
  wrestlerId: string;
  wrestlerWeight: string;
  wrestlerRank: number;
  draftedByName: string;
  draftedByKey: string;
  draftedById: string;
  draftedByOrderSlot: number;
  draftedRound: number;
}
