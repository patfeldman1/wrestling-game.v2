import { IScore } from './scores.interface';
import { IDraftedWrestler } from './league.interface';
import { IWrestlerHighlight } from './highlights.class';
export interface IWrestlers {
  wrestlersById: dictionary<IWrestler>;
  wrestlersByWeight: dictionary<number[]>;
}

export interface IWrestler {
  id: number;
  weight: string;
  school: string;
  coach_rank: string;
  conf: string;
  conf_finish: number; // "4",
  first_name: string; // "Nicholas",
  flow_id: number; // "",
  flow_rank: number; // 0,
  intermat_id: number; // 293987107,
  intermat_rank: number; // 17,
  last_name: string; // "Gravina",
  losses: number; // 9,
  overall_rank: number; // 0,
  rank: number; // 15,
  tracker_id: number; // 1053983011,
  wins: number; // 21,
  year: string; // "Sophomore",
  year_abbr: string; // "SO"
  selected: IDraftedWrestler;
  highlight: IWrestlerHighlight;
  matches: IScore[];
}
