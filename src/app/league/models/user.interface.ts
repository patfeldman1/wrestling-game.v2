import { IUserLeagueRef } from './user-league.interface';
import { IWrestlerHighlight } from './highlights.class';
export interface IUser {
  name: string;
  id: string;
  email: string;
  photo: string;
  leagues_ref: dictionary<IUserLeagueRef>;
  highlights: IWrestlerHighlight[];
}
export interface IUserWithKey extends IUser {
  $key: string;
}

export interface IDraftUser extends IUser {
  draftOrder: number;
  key: string;
  teamScore?: number;
}

export interface INewDraftUser {
  name: string;
  draftOrder: number;
}
