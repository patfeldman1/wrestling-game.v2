export interface ICreateLeague {
  name: string;
  password: string;
  maxUsers: number;
  createDate: number;
  draftDate: number;
}
export interface ICreateLeagueStatus {
  status: eCreateStatus;
  leagueName: string;
}

export enum eCreateStatus {
  Nothing,
  Creating,
  Created,
  Error
}
