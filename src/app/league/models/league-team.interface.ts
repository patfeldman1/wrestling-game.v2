import { IWrestler } from './wrestlers.interface';
import { IDraftUser } from './user.interface';
export interface ILeagueTeam {
  user: IDraftUser;
  teamScore: number;
  team: IWrestler[];
}
