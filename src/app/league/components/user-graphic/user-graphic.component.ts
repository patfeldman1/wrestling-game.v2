import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-user-graphic',
  templateUrl: './user-graphic.component.html',
  styleUrls: ['./user-graphic.component.scss']

})
export class UserGraphicComponent {
  @Input() letter1: string;
  @Input() letter2: string;
  @Input() colorVal;

  constructor() {
    this.colorVal = Math.floor(Math.random() * 6) + 1;
  }

}
