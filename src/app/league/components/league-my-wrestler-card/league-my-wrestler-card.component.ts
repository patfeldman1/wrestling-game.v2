import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { WrestlerInfo } from './../../../game/models/wrestler-info.model';
import { WrestlingLookupService } from './../../../game/services/wrestling-lookup.service';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { MatDialog } from '@angular/material';
import * as fromLeague from '../../store/wrestling.store';

@Component({
  selector: 'app-league-my-wrestler-card',
  templateUrl: './league-my-wrestler-card.component.html',
  styleUrls: ['./league-my-wrestler-card.component.scss']
})
export class LeagueMyWrestlerComponent implements OnInit {
    @Input() wrestlerId: string;
    @Output() bracketSelect = new EventEmitter();
    $isDraftComplete: Observable<boolean>;
    wrestlerScore: number;
    wrestler: WrestlerInfo;
    constructor(
      private wrestlingLookupApi: WrestlingLookupService,
      private _store: Store<fromLeague.AppState>
    ) { }

  ngOnInit() {
    this.wrestler = this.wrestlingLookupApi.getWrestlerInfo(this.wrestlerId);
    this.$isDraftComplete = this._store.select(fromLeague.isDraftComplete);
    this.wrestlerScore = this.wrestlingLookupApi.getCurrentScore(this.wrestlerId);

  }
}
