import { IDraftedWrestler } from './../../models/league.interface';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { ILeague } from '../../models/league.interface';
import { Store } from '@ngrx/store';
import * as fromLeague from '../../store/wrestling.store';
import { IDraftUser } from '../../models/user.interface';
import { WrestlingLookupService } from '../../../game/services/wrestling-lookup.service';
import { WrestlerInfo } from '../../../game/models/wrestler-info.model';

@Component({
  selector: 'app-team-draft-summary',
  templateUrl: './team-draft-summary.component.html',
  styleUrls: ['./team-draft-summary.component.scss']
})
export class TeamDraftSummaryComponent implements OnInit {
  @Input() draftUser: IDraftUser;
  @Output() playSelected = new EventEmitter();
  $currentUser: Observable<IDraftUser>;
  $isDirectionDown: Observable<Boolean>;
  $nextUser: Observable<IDraftUser>;
  $draftedTeams: Observable<IDraftedWrestler[]>;
  $myUserKey: Observable<string>;

  constructor(
    public _store: Store<fromLeague.AppState>,
    public dialog: MatDialog,
    private wrestlerLookup: WrestlingLookupService
  ) { }
  ngOnInit() {
    this.$myUserKey = this._store.select(fromLeague.getMyKey);
    this.$currentUser = this._store.select(fromLeague.getCurrentDraftUser);
    this.$isDirectionDown = this._store.select(fromLeague.isDirectionDown);
    this.$nextUser = this._store.select(fromLeague.getNextDraftUser);
    this.$draftedTeams = this._store
      .select(fromLeague.getLiveDraftedTeams)
      .skipWhile(teams => !(this.draftUser.key in teams))
      .map(dictOfTeams => dictOfTeams[this.draftUser.key]);
  }

  isCurrentUser(currentUser: IDraftUser) {
    return this.draftUser.key === currentUser.key;
  }
  isNextUser(nextUser: IDraftUser) {
    return this.draftUser.key === nextUser.key;
  }
  getWrestler(wrestlerId: string): WrestlerInfo {
    // console.log( this.wrestlerLookup.getWrestlerInfo(wrestlerId));
    return this.wrestlerLookup.getWrestlerInfo(wrestlerId);
  }
}
