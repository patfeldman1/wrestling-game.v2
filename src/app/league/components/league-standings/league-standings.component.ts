import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { ILeague } from '../../models/league.interface';
import { Store } from '@ngrx/store';
import * as fromLeague from '../../store/wrestling.store';
import { IDraftUser } from '../../models/user.interface';
import * as draftActions from '../../store/draft.store/draft.actions';

@Component({
  selector: 'app-league-standings',
  templateUrl: './league-standings.component.html',
  styleUrls: ['./league-standings.component.scss']
})
export class LeagueStandingsComponent implements OnInit {
  $draftUsers: Observable<IDraftUser[]>;
  constructor(
    public _store: Store<fromLeague.AppState>,
    public dialog: MatDialog

  ) { }
  ngOnInit() {
    this._store.dispatch(new draftActions.UpdateTeamScoresAction);
    this.$draftUsers = this._store
      .select(fromLeague.getDraftUsers)
      .map(users =>
        Object.values(users)
              .sort( (a, b) => b.teamScore - a.teamScore));
  }
}

