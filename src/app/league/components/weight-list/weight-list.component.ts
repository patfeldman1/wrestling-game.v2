import { getSelectedWrestlersByWeight } from './../../store/wrestling.store';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import * as fromLeague from '../../store/wrestling.store';
import { WrestlingLookupService } from '../../../game/services/wrestling-lookup.service';
import { WrestlerInfo } from '../../../game/models/wrestler-info.model';

@Component({
  selector: 'app-weight-list',
  templateUrl: './weight-list.component.html',
  styleUrls: ['./weight-list.component.scss']
})
export class WeightListComponent implements OnInit {
  @Input() weight: string;
  @Output() select = new EventEmitter<WrestlerInfo>();
  wrestlers: dictionary<WrestlerInfo>;
  $selectedWrestlers: Observable<dictionary<string>>;

  constructor(
    public _store: Store<fromLeague.AppState>,
    public dialog: MatDialog,
    public wrestlerLookup: WrestlingLookupService
  ) { }
  ngOnInit() {
    this.wrestlers = this.wrestlerLookup.getAllWrestlersAtWeight(this.weight);
    this.$selectedWrestlers = this._store.select(fromLeague.getSelectedWrestlersByWeight(this.weight));
  }
}

