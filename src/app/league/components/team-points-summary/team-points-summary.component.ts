import { IDraftedWrestler } from './../../models/league.interface';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { ILeague } from '../../models/league.interface';
import { Store } from '@ngrx/store';
import * as fromLeague from '../../store/wrestling.store';
import { IDraftUser } from '../../models/user.interface';
import { WrestlingLookupService } from '../../../game/services/wrestling-lookup.service';
import { WrestlerInfo } from '../../../game/models/wrestler-info.model';

@Component({
  selector: 'app-team-points-summary',
  templateUrl: './team-points-summary.component.html',
  styleUrls: ['./team-points-summary.component.scss']
})
export class TeamPointsSummaryComponent implements OnInit {
  @Input() draftUser: IDraftUser;
  $totalPoints: Observable<number>;
  $myUserKey: Observable<string>;

  constructor(
    public _store: Store<fromLeague.AppState>,
    public dialog: MatDialog,
    private wrestlerLookup: WrestlingLookupService
  ) { }
  ngOnInit() {
    this.$myUserKey = this._store.select(fromLeague.getMyKey);
  }

  getWrestler(wrestlerId: string): WrestlerInfo {
    // console.log( this.wrestlerLookup.getWrestlerInfo(wrestlerId));
    return this.wrestlerLookup.getWrestlerInfo(wrestlerId);
  }
}
