import { IDraftedWrestler } from './../../models/league.interface';
import { getAllSelectedWrestlersByWeight, getSelectedWrestlers } from './../../store/wrestling.store';
import { WrestlingLookupService } from './../../../game/services/wrestling-lookup.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { IUserLeagueRef } from '../../models/user-league.interface';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/wrestling.store';
import { Router } from '@angular/router';
import { CreateLeagueDialogComponent } from '../../dialogs/create-league/create-league.dialog.component';
import { Observable } from 'rxjs/Observable';
import { WrestlerInfo } from '../../../game/models/wrestler-info.model';

import * as fromLeagues from '../../store/wrestling.store';

@Component({
  selector: 'app-league-bracket-summary',
  templateUrl: './league-bracket-summary.component.html',
  styleUrls: ['./league-bracket-summary.component.scss']
})
export class LeagueBracketSummaryComponent implements OnInit {
    @Input() weight: string;
    @Output() bracketSelect = new EventEmitter();
    $topThree: Observable<WrestlerInfo[]>;
    wrestlers: WrestlerInfo[];
    constructor(
        private _store: Store<AppState>,
        private wrestlingApi: WrestlingLookupService
    ) { }
    ngOnInit() {
        this.wrestlers = Object
            .values(this.wrestlingApi.getAllWrestlersAtWeight(this.weight))
            .sort( (a, b) => a.seed - b.seed);
        this.$topThree = this._store.select(fromLeagues.getLiveDraftedWrestlers)
            .map(wrestlers => wrestlers.filter(wrestler => wrestler.wrestlerWeight === this.weight))
            .map(allPicked => {
                const pickedIds = allPicked.map(pick => pick.wrestlerId);
                const topThree: WrestlerInfo[] = [];
                this.wrestlers.every(wrestler => {
                    if (pickedIds.indexOf(wrestler.global_pid) < 0) {
                        topThree.push(wrestler);
                    }
                    return (topThree.length >= 3) ? false : true;
                });
                return topThree;
            });
    }
}
