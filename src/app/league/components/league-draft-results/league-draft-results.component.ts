import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { IUserLeagueRef } from '../../models/user-league.interface';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/wrestling.store';
import * as leagueActions from '../../store/leagues.store/leagues.actions';
import { Router } from '@angular/router';
import { CreateLeagueDialogComponent } from '../../dialogs/create-league/create-league.dialog.component';

@Component({
  selector: 'app-league-draft-results',
  templateUrl: './league-draft-results.component.html',
  styleUrls: ['./league-draft-results.component.scss']
})
export class LeagueDraftResultsComponent implements OnInit {
  constructor(
    private _store: Store<AppState>,
    private router: Router
  ) { }
  ngOnInit() { }
  goToLeague() {
  }

}
