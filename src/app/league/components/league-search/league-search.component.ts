import { Component, OnInit, Input, EventEmitter, Output, OnDestroy } from '@angular/core';
import { ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { MatDialogRef } from '@angular/material';

import * as leagueActions from '../../store/leagues.store/leagues.actions';
import * as fromLeagues from '../../store/wrestling.store';
import { IJoinLeagueStatus, eJoinStatus } from '../../models/league-join.interface';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-league-search',
  templateUrl: './league-search.component.html',
  styleUrls: ['./league-search.component.scss']
})
export class LeagueSearchComponent implements OnInit, OnDestroy {
  statusEnum = eJoinStatus;
  joinForm: FormGroup;
  createForm: FormGroup;
  $joinStatus: Observable<IJoinLeagueStatus>;
  $statusRef: Subscription;
  @Input() selectedTabIndex = 0;
  @Output() isSuccess = new EventEmitter();

  constructor(
    private formBuilder: FormBuilder,
    private _store: Store<fromLeagues.AppState>
  ) {
    // First Step
    this.createForm = this.formBuilder.group({
      'leagueName': ['', [
        Validators.minLength(6),
        Validators.maxLength(25),
        Validators.required
      ]],
      'password': ['', [
        Validators.minLength(6),
        Validators.maxLength(25),
        Validators.required
      ]],
      'region': ['', []],
    });
    this.joinForm = this.formBuilder.group({
      'leagueName': ['', [
        Validators.minLength(6),
        Validators.maxLength(25),
        Validators.required
      ]],
      'password': ['', [
        Validators.minLength(6),
        Validators.maxLength(25),
        Validators.required
      ]],
      'region': ['', []],
    });

  }

  ngOnDestroy () {
    this.$statusRef.unsubscribe();
  }

  ngOnInit() {
    this.$joinStatus = this._store.select(fromLeagues.getJoinLeagueStatus);

    this.$statusRef = this.$joinStatus.subscribe(status => {
      if (status.leagueName === this.leagueName.value || status.leagueName === this.joinLeagueName.value) {
        if (status.status === eJoinStatus.Joined) {
          this.isSuccess.emit(true);
        }
      }
    });

  }

  // Using getters will make your code look pretty
  get leagueName() { return this.createForm.get('leagueName'); }
  get password() { return this.createForm.get('password'); }
  get joinLeagueName() { return this.joinForm.get('leagueName'); }
  get joinPassword() { return this.joinForm.get('password'); }

  // Step 1
  createNewLeague() {
    this._store.dispatch(new leagueActions.CreatePrivateLeagueAction({
      name: this.leagueName.value,
      password: this.password.value,
      maxUsers: 20,
      createDate: Date.now(),
      draftDate: Date.now()
    }));
  }

  joinLeague() {
    this._store.dispatch(new leagueActions.JoinLeagueAction({
      leagueName: this.joinLeagueName.value,
      leaguePassword: this.joinPassword.value,
      isPrivate: true
    }));
  }
}
