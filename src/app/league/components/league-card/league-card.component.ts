import { ReorderDraftDialogComponent } from './../../dialogs/reorder-draft/reorder-draft.dialog';
import { ViewParticipantsDialogComponent } from './../../dialogs/view-participants/view-participants.dialog';
import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { IUserLeagueRef } from '../../models/user-league.interface';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/wrestling.store';
import * as leagueActions from '../../store/leagues.store/leagues.actions';
import * as fromLeagues from '../../store/wrestling.store';
import { Router } from '@angular/router';
import { CreateLeagueDialogComponent } from '../../dialogs/create-league/create-league.dialog.component';
import { Observable } from 'rxjs/Observable';
import { AddPlayerToDraftComponent } from '../../dialogs/add-player-to-draft/add-player-to-draft.component';
import { IDraftSetup } from './../../models/league.interface';

@Component({
  selector: 'app-league-card',
  templateUrl: './league-card.component.html',
  styleUrls: ['./league-card.component.scss']
})
export class LeagueCardComponent implements OnInit {
  @Input() league: IUserLeagueRef;
  @Input() myId: string;
  @Input() draftSetup: IDraftSetup;

  $myId: Observable<string>;
  constructor(
    private _store: Store<AppState>,
    private router: Router,
    public dialog: MatDialog
  ) { }
  ngOnInit() {}

  get isAdmin () {
    return this.league.creatorId === this.myId;
  }
  goToLeague() {
    this._store.dispatch(new leagueActions.SetCurrentLeagueAction(this.league));
    this.router.navigate(['/leaguedraft']);
  }
  viewParticipants() {
    const names = Object
    .values(this.draftSetup.users)
    .sort((user1, user2) => user1.draftOrder - user2.draftOrder)
    .map(user => user.name);
  const dialogRef = this.dialog.open(ViewParticipantsDialogComponent, {
      width: '350px',
      data: {'names': names}
    });
  }
  draftAdd() {
    const names = Object
    .values(this.draftSetup.users)
    .sort((user1, user2) => user1.draftOrder - user2.draftOrder)
    .map(user => user.name);
    const dialogRef = this.dialog.open(AddPlayerToDraftComponent, {
      width: '400px',
      data: {'league': this.league, 'names': names }
    });
  }
  draftReorder() {
    const names = Object
    .values(this.draftSetup.users)
    .sort((user1, user2) => user1.draftOrder - user2.draftOrder)
    .map(user => user.name);
    const dialogRef = this.dialog.open(ReorderDraftDialogComponent, {
      width: '400px',
      data: {'league': this.league, 'names': names }
    });

  }

}
