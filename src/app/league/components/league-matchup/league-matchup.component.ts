import { Observable } from 'rxjs/Observable';
import { IDraftedWrestler } from './../../models/league.interface';
import { Store } from '@ngrx/store';
import { Match } from './../../../game/models/page-brackets.model';
import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import * as fromLeague from '../../store/wrestling.store';

@Component({
    selector: 'app-league-matchup',
    templateUrl: './league-matchup.component.html',
    styleUrls: ['./league-matchup.component.scss']
})
export class LeagueMatchupComponent implements OnInit {
    @Input() match: Match;
    @Input() linkClass = 'font-14';
    @Input() linkTeamClass = 'font-10';
    topSelectedBy = '';
    bottomSelectedBy = '';
    $selectedWrestlers: Observable<IDraftedWrestler[]>;

    constructor(
        private _store: Store<fromLeague.AppState>
    ) { }
    ngOnInit() {
        this.$selectedWrestlers = this._store.select(fromLeague.getLiveDraftedWrestlers);
    }

    getSelectedBy(wrestlerId: string, wrestlers: IDraftedWrestler[]): string {
        const wrestlerInfo = wrestlers.find(wrestler => wrestler.wrestlerId ===  wrestlerId);
        return (!!wrestlerInfo) ? wrestlerInfo.draftedByName : 'unowned';
    }
}
