import { IDraftSetup } from './../../models/league.interface';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ILeague } from '../../models/league.interface';
import { Store } from '@ngrx/store';
import * as fromLeague from '../../store/wrestling.store';
import { IUserLeagueRef } from '../../models/user-league.interface';
import { IUser } from '../../models/user.interface';
import { CreateLeagueDialogComponent } from '../../dialogs/create-league/create-league.dialog.component';

@Component({
  selector: 'app-league-list',
  templateUrl: './league-list.component.html',
  styleUrls: ['./league-list.component.scss']
})
export class LeagueListComponent implements OnInit {
  $myInfo: Observable<IUser>;
  $leagues: Observable<dictionary<IUserLeagueRef>>;
  $draftsSetup: Observable<dictionary<IDraftSetup>>;
  constructor(
    public _store: Store<fromLeague.AppState>,
    public dialog: MatDialog
  ) { }
  ngOnInit() {
    this.$leagues = this._store.select(fromLeague.getMyLeagues);
    this.$myInfo = this._store.select(fromLeague.getMyInfo);
    this.$draftsSetup = this._store.select(fromLeague.getMyLeaguesDraftSetup);
  }
  createLeague() {
    const dialogRef = this.dialog.open(CreateLeagueDialogComponent, {
      width: '450px',
      data: {'tabType': 'create'}
    });
  }
  findLeague() {
    const dialogRef = this.dialog.open(CreateLeagueDialogComponent, {
      width: '450px',
      data: {}
    });
  }

  getDraftSetup(allDraftSetups: dictionary<IDraftSetup>, leagueKey: string): IDraftSetup {
    return allDraftSetups[leagueKey];
  }
}

