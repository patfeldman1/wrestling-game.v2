import { LeagueBracketDialogComponent } from './../../dialogs/league-bracket/league-bracket.dialog';
import { IDraftedWrestler } from './../../models/league.interface';
import { all_weights } from './../../../util/consts';
import { Router } from '@angular/router';
import { WrestlingLookupService } from './../../../game/services/wrestling-lookup.service';
import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { ILeague } from '../../models/league.interface';
import { Store } from '@ngrx/store';
import * as fromLeague from '../../store/wrestling.store';
import { WrestlerInfo } from '../../../game/models/wrestler-info.model';

@Component({
  selector: 'app-league-team',
  templateUrl: './league-team.component.html',
  styleUrls: ['./league-team.component.scss']
})
export class LeagueTeamComponent implements OnInit {
  all_weights = all_weights;
  @Input() teamId: string;
  $wrestlers: Observable<IDraftedWrestler[]>;
  $teamName: Observable<string>;
  constructor(
    private _store: Store<fromLeague.AppState>,
    public dialog: MatDialog
  ) { }
  ngOnInit() {
    this.$teamName = this._store
      .select(fromLeague.getDraftUsers)
      .skipWhile(users => Object.keys(users).length <= 0 )
      .map(users =>
        Object.values(users)
          .find(user => user.id === this.teamId)
          .name
      );
    this.$wrestlers = this._store
      .select(fromLeague.getLiveDraftedWrestlers)
      .map(allWrestlers =>
        allWrestlers.filter(wrestler => wrestler.draftedById === this.teamId)
      );
  }

  openBracket(weight: string) {
    const dialogRef = this.dialog.open(LeagueBracketDialogComponent, {
      width: '360px',
      height: '625px',
      maxHeight: '99vh',
      data: { weight: weight }
    });

  }

}

