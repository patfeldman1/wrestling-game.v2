import { PageBracket } from './../../../game/models/page-brackets.model';
import { WrestlingLookupService } from './../../../game/services/wrestling-lookup.service';
import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Input } from '@angular/core';
import { } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'app-league-bracket',
  templateUrl: './league-bracket.component.html',
  styleUrls: ['./league-bracket.component.scss']
})
export class LeagueBracketComponent implements OnInit {
  @Input() weight: string;

  bracket: PageBracket;
  rounds = [0, 1, 2, 3, 4];
  constructor(private lookup: WrestlingLookupService) { }
  ngOnInit() {
    this.bracket = this.lookup.getBracket(this.weight);
  }
}
