import { AllTeamsDialogComponent } from './../../dialogs/all-teams.dialog/all-teams.dialog';
import { all_weights } from './../../../util/consts';
import { IDraftUser } from './../../models/user.interface';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Store } from '@ngrx/store';
import * as fromLeague from '../../store/wrestling.store';
import * as _ from 'lodash';
import { WrestlerInfo } from '../../../game/models/wrestler-info.model';
import { IDraftedWrestler } from '../../models/league.interface';

@Component({
  selector: 'app-league-all-teams',
  templateUrl: './league-all-teams.component.html',
  styleUrls: ['./league-all-teams.component.scss']
})
export class LeagueAllTeamsComponent implements OnInit {
  $draftedTeams: Observable<{}>;
  $teams: Observable<IDraftUser[]>;
  all_weights = all_weights;
  constructor(
    private _store: Store<fromLeague.AppState>,
    private dialog: MatDialog
  ) { }
  ngOnInit() {
    this.$teams = this._store
      .select(fromLeague.getDraftUsers)
      .map(dict => Object.values(dict).sort((a, b) => a.draftOrder - b.draftOrder));

    this.$draftedTeams = this._store.select(fromLeague.getLiveDraftedTeams).map(teams => {
      const table = {};
      _.forEach(teams, (value, key) => {
        value.forEach(wrestler => {
          if (!(wrestler.draftedByKey in table)) {
            table[wrestler.draftedByKey] = {};
          }
          table[wrestler.draftedByKey][wrestler.wrestlerWeight] = wrestler;
        });
      });
      return table;
    });
  }

  hasWrestler(team: IDraftUser, weight: string, table: {}): boolean {
    return (team.key in table && weight in table[team.key]) ? true : false;
  }
  getWrestler(team: IDraftUser, weight: string, table: {}): IDraftedWrestler {
    return (team.key in table && weight in table[team.key]) ? table[team.key][weight] : null;
  }
  openAllTeams() {
    const dialogRef = this.dialog.open(AllTeamsDialogComponent, {
      width: '90%',
      maxWidth: '1300px'
    });

  }

}
