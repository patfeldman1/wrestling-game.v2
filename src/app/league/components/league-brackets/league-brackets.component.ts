import { LeagueBracketDialogComponent } from './../../dialogs/league-bracket/league-bracket.dialog';
import { all_weights } from './../../../util/consts';
import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { IUserLeagueRef } from '../../models/user-league.interface';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/wrestling.store';
import * as leagueActions from '../../store/leagues.store/leagues.actions';
import { Router } from '@angular/router';
import { CreateLeagueDialogComponent } from '../../dialogs/create-league/create-league.dialog.component';

@Component({
  selector: 'app-league-brackets',
  templateUrl: './league-brackets.component.html',
  styleUrls: ['./league-brackets.component.scss']
})
export class LeagueBracketsComponent implements OnInit {
  all_weights = all_weights;
  constructor(
    private _store: Store<AppState>,
    private dialog: MatDialog
  ) { }
  ngOnInit() { }
  openBracket(weight: string) {
    const dialogRef = this.dialog.open(LeagueBracketDialogComponent, {
        width: '360px',
        height: '625px',
        maxHeight: '99vh',
        data: { weight: weight }
    });
  }
}
