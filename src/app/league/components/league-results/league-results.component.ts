import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { ILeague } from '../../models/league.interface';
import { Store } from '@ngrx/store';
import * as fromLeague from '../../store/wrestling.store';

@Component({
  selector: 'app-league-results',
  templateUrl: './league-results.component.html',
  styleUrls: ['./league-results.component.scss']
})
export class LeagueResultsComponent implements OnInit {
  constructor(
    public _store: Store<fromLeague.AppState>,
    public dialog: MatDialog
  ) { }
  ngOnInit() {
  }
}

