import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import * as fromLeague from '../../store/wrestling.store';
import { RemoveDrafterDialogComponent } from '../../dialogs/remove-drafter/remove-drafter.dialog';
import { IDraftUser } from '../../models/user.interface';

@Component({
  selector: 'app-league-draft-status-panel',
  templateUrl: './league-draft-status-panel.component.html',
  styleUrls: ['./league-draft-status-panel.component.scss']
})
export class LeagueDraftStatusPanelComponent implements OnInit {
  $isAdmin: Observable<boolean>;
  $users: Observable<IDraftUser[]>;
  constructor(
    public _store: Store<fromLeague.AppState>,
    public dialog: MatDialog
  ) { }
  ngOnInit() {
    this.$isAdmin = this._store.select(fromLeague.isCurrentLeagueAdmin).take(1);
    this.$users = this._store
      .select(fromLeague.getDraftUsers)
      .map(users => Object.values(users).sort( (a, b) => a.draftOrder - b.draftOrder) );
  }
  undoLastSelection() {
  }
  removeDrafter(allUsers: IDraftUser[]) {
    const dialogRef = this.dialog.open(RemoveDrafterDialogComponent, {
      width: '400px',
      height: '66%',
      data: { users: allUsers }
    });

  }
}

