import { WrestlerSelectComponent } from './../../dialogs/wrestler-select/wrestler-select.component';
import { Directive, ElementRef, HostListener, Renderer2, Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { WrestlerInfo } from '../../../game/models/wrestler-info.model';
@Component({
    selector: 'app-league-wrestler-link',
    templateUrl: './league-wrestler-link.component.html',
    styleUrls: ['./league-wrestler-link.component.scss']
})
export class LeagueWrestlerLinkComponent {
    @Input() id: string;
    constructor(public dialog: MatDialog) {}
    @HostListener('click') onClick(btn: any) {
        const dialogRef = this.dialog.open(WrestlerSelectComponent, {
            width: '360px',
            height: '60vh',
            data: { wrestlerId: this.id }
        });
    }
}
