import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { ILeague } from '../../models/league.interface';
import { Store } from '@ngrx/store';
import * as fromLeague from '../../store/wrestling.store';
import { IDraftUser } from '../../models/user.interface';
import { AllSelectionsComponent } from '../../dialogs/all-selections/all-selections.component';

@Component({
  selector: 'app-league-draft',
  templateUrl: './league-draft.component.html',
  styleUrls: ['./league-draft.component.scss']
})
export class LeagueDraftComponent implements OnInit {
  $draftUsers: Observable<dictionary<IDraftUser>>;
  $currentRound: Observable<Number>;
  constructor(
    public _store: Store<fromLeague.AppState>,
    public dialog: MatDialog
  ) { }
  ngOnInit() {
    this.$draftUsers = this._store.select(fromLeague.getDraftUsers);
    this.$currentRound = this._store.select(fromLeague.getCurrentRound);
  }
  openDraftSelections() {
    const dialogRef = this.dialog.open(AllSelectionsComponent, {
      height: '94vh',
      maxWidth: '96vw'
    });

  }
}

