import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import * as leaguesActions from '../../store/leagues.store/leagues.actions';
import * as wrestlersActions from '../../store/wrestlers.store/wrestlers.actions';
import * as draftActions from '../../store/draft.store/draft.actions';
import * as fromLeague from '../../store/wrestling.store';

enum LeagueState {
  Loading,
  HasLeague,
  NewUser
}

@Component({
  selector: 'app-league',
  templateUrl: './league-base.component.html',
  styleUrls: ['./league-base.component.scss']
})
export class LeagueBaseComponent implements OnInit {
  $leagueState: Observable<LeagueState>;
  LeagueState = LeagueState;
  constructor(
    public _store: Store<fromLeague.AppState>,
  ) { }

  ngOnInit() {
    // Reset every time we come to this page
    this._store.dispatch(new leaguesActions.ResetCurrentLeagueAction());
    this._store.dispatch(new draftActions.UnsubscribeToDraftAction());
    this._store.dispatch(new wrestlersActions.ResetAllSelectedWrestlers());
    this.$leagueState = this._store
      .select(fromLeague.getMyInfo)
      .map(info => {
        if (info.id === '') {
          return LeagueState.Loading;
        } else {
          return ('leagues_ref' in info && Object.keys(info.leagues_ref).length > 0
            ? LeagueState.HasLeague
            : LeagueState.NewUser);
        }
      });

  }
}
