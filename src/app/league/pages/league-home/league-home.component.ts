import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/wrestling.store';

import * as fromLeagues from '../../store/wrestling.store';
import { Observable } from 'rxjs/Observable';
import { IUserLeagueRef } from '../../models/user-league.interface';
import { ILeague } from '../../models/league.interface';
import { IUserWithKey } from '../../models/user.interface';

@Component({
  selector: 'app-league-home',
  templateUrl: './league-home.component.html',
  styleUrls: ['./league-home.component.scss']
})
export class LeagueHomeComponent implements OnInit {
  $league: Observable<ILeague>;
  $isDrafting: Observable<Boolean>;
  $isDraftComplete: Observable<Boolean>;
  $isAdmin: Observable<boolean>;
  $myInfo: Observable<IUserWithKey>;

  showStatusPanel = false;
  showDraftBoard = true;
  showMyTeam = true;
  showAllTeams = true;
  showBracketSummary = false;
  constructor(private route: ActivatedRoute, private _store: Store<AppState>) { }
  ngOnInit() {
    this.$isAdmin = this._store.select(fromLeagues.isCurrentLeagueAdmin);
    this.$league = this._store.select(fromLeagues.getCurrentLeague);
    this.$isDrafting = this._store.select(fromLeagues.isDrafting);
    this.$isDraftComplete = this._store.select(fromLeagues.isDraftComplete);
    this.$myInfo = this._store.select(fromLeagues.getMyInfo);
  }
}
