import * as actions from './draft.actions';
import * as _ from 'lodash';
import { IDraftSetup, IDraftedWrestler } from '../../models/league.interface';
import { IDraftUser, INewDraftUser } from '../../models/user.interface';
import { randomKey } from '../../../util/random-key';

export interface State {
  draftSetup: IDraftSetup;
  liveDraftedParticipants: IDraftedWrestler[];
  liveDraftedTeams: dictionary<IDraftedWrestler[]>;
  liveDraftNumSelected: number;
  liveDraftRound: number;
  isDrafting: boolean;
  isMyTurn: boolean;
  timeTillDraftInMS: number;
}

const initialState: State = {
  draftSetup: {
    users: {},
    draftDate: null,
    draftComplete: false
  },
  liveDraftedParticipants: [],
  liveDraftNumSelected: 0,
  liveDraftRound: 0,
  liveDraftedTeams: {},
  timeTillDraftInMS: null,
  isDrafting: false,
  isMyTurn: false,
};

export function reducer(state: State = initialState, action: actions.Actions): State {
  switch (action.type) {
    case (actions.ActionTypes.PROCESS_IS_MY_TURN): {
      const payload = action.payload as boolean;
      return Object.assign({}, state, {
        isMyTurn: payload
      });
    }
    case (actions.ActionTypes.UPDATE_NUMBER_DRAFTED): {
      const numSelected = action.payload as number;
      const numUsers = Object.keys(state.draftSetup.users).length;
      const round = Math.floor(numSelected / numUsers) + 1;
      return Object.assign({}, state, {
        liveDraftNumSelected: action.payload,
        liveDraftRound: round
      });
    }
    case (actions.ActionTypes.UNSUBSCRIBE_TO_DRAFT): {
      return initialState;
    }
    case (actions.ActionTypes.UPDATE_TEAM_SCORES_COMPLETE): {
      const updatedScores = action.payload as dictionary<number>;
      const updateUsers = Object.assign({}, state.draftSetup.users);
      _.forEach(updateUsers, (user, userKey) => {
        updateUsers[userKey].teamScore = updatedScores[user.key];
      });
      const newSetup = Object.assign({}, state.draftSetup, {
        users: updateUsers
      });
      return Object.assign({}, state, {
        draftSetup: newSetup
      });
    }
    case (actions.ActionTypes.DELETE_PARTICIPANT_FROM_DRAFT_COMPLETE): {
      const user: IDraftUser = action.payload as IDraftUser;
      const lessUsers = Object.assign({}, state.draftSetup.users);
      Object.keys(lessUsers).forEach((key) => {
        if (lessUsers[key].name === user.name) {
          delete lessUsers[key];
        }
      });
      const newDraftSetup = Object.assign({}, state.draftSetup, {
        users: lessUsers
      });
      return Object.assign({}, state, {
        draftSetup: newDraftSetup
      });
    }
    case (actions.ActionTypes.UPDATE_DRAFT_SETUP): {
      const load = action.payload as IDraftSetup;
      _.forEach(load.users, (user, key) => {
        if (!user.hasOwnProperty('key')) {
          user.key = key;
        }
      });

      return Object.assign({}, state, {
        draftSetup: action.payload
      });
    }
    case (actions.ActionTypes.PROCESS_LIVE_DRAFT_PARTICIPANTS_UPDATE): {
      const load = action.payload as dictionary<IDraftedWrestler>;
      const final: IDraftedWrestler[] = [];
      _.forEach(load, (wrestler, key) => {
        final.push(wrestler);
      });
      return Object.assign({}, state, {
        liveDraftedParticipants: final
      });
    }
    case (actions.ActionTypes.PROCESS_LIVE_DRAFT_TEAMS_UPDATE): {
      const load = action.payload as dictionary<dictionary<IDraftedWrestler>>;
      const final: dictionary<IDraftedWrestler[]> = {};
      _.forEach(load, (team, teamKey) => {
        final[teamKey] = [];
        _.forEach(team, (wrestler, wrestlerKey) => {
          final[teamKey].push(wrestler);
        });
      });
      return Object.assign({}, state, {
        liveDraftedTeams: final
      });
    }
    default: {
      return state;
    }
    case (actions.ActionTypes.CHECK_IF_DRAFTING): {
      const timeLeftInMS = state.draftSetup.draftDate - Date.now();
      const isDraftingNow = timeLeftInMS <= 0;
      if (!state.draftSetup.draftDate) { return state; }
      return Object.assign({}, state, {
        isDrafting: isDraftingNow,
        timeTillDraftInMS: timeLeftInMS
      });
    }
    case (actions.ActionTypes.ADD_PARTICIPANT_TO_DRAFT_COMPLETE): {
      const payload = action.payload as INewDraftUser;
      const user: IDraftUser = {
        name: payload.name,
        draftOrder: payload.draftOrder,
        key: randomKey(),
        id: '',
        email: '',
        photo: '',
        leagues_ref: null,
        highlights: []
      };
      const extraUsers = Object.assign({}, state.draftSetup.users);
      extraUsers[user.key] = user;
      const newDraftSetup = Object.assign({}, state.draftSetup, {
        users: extraUsers
      });
      return Object.assign({}, state, {
        draftSetup: newDraftSetup
      });
    }
  }
}

export const getDraftSetup = (state: State) => state.draftSetup;
export const getDraftUsers = (state: State) => state.draftSetup.users;
export const getLiveDraftedWrestlers = (state: State) => state.liveDraftedParticipants;
export const getLiveDraftedTeams = (state: State) => state.liveDraftedTeams;
export const getNumberOfWrestlersDrafted = (state: State) => state.liveDraftNumSelected;
export const getCurrentRoundOfDraft = (state: State) => state.liveDraftRound;
export const getStatus = (state: State) => state;
export const getTimeTillDraftInMS = (state: State) => state.timeTillDraftInMS;
export const isMyTurn = (state: State) => state.isMyTurn;
export const isDrafting = (state: State) => state.isDrafting;
export const isDraftComplete = (state: State) => state.draftSetup.draftComplete;
export const getUserDraftedTeam = (state: State, user: IDraftUser) => state.liveDraftedTeams[user.key];
export const getUserDraftedTeamByKey = (state: State, key: string) => state.liveDraftedTeams[key];
export const getCurrentDraftUser = (state: State) => {
  const users: IDraftUser[] = _.map(state.draftSetup.users, (value, key) => value);
  const clone = [...users.sort((a: IDraftUser, b: IDraftUser) => a.draftOrder - b.draftOrder)];
  const dupUsers = [...clone.slice(), ...clone.slice().reverse(), ...clone.slice()];
  const currentUp = state.liveDraftNumSelected % (users.length * 2);
  const currentUser: IDraftUser = dupUsers.slice(currentUp, currentUp + 1)[0];
  return currentUser;
};

export const getNextDraftUser = (state: State) => {
  const users: IDraftUser[] = _.map(state.draftSetup.users, (value, key) => value);
  const clone = [...users.sort((a: IDraftUser, b: IDraftUser) => a.draftOrder - b.draftOrder)];
  const dupUsers = [...clone.slice(), ...clone.slice().reverse(), ...clone.slice()];
  const nextUp = (state.liveDraftNumSelected + 1) % (users.length * 2);
  const nextUser: IDraftUser = dupUsers.slice(nextUp, nextUp + 1)[0];
  return nextUser;
};

export const isSelectionDirectionDown = (state: State) => {
  return (Math.floor(state.liveDraftNumSelected / Object.keys(state.draftSetup.users).length) % 2 === 0)
    ? true : false;
};

export const getUserDraftSlot = (state: State, userId: string) => {
  let retVal = 0;
  _.map(state.draftSetup.users, (user: IDraftUser, key) => {
    if (user.id === userId) { retVal = user.draftOrder; }
  });
  return retVal;
};
