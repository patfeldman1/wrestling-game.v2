import { Action } from '@ngrx/store';
import { type } from '../../../util/type';
import { INewDraftUser, IDraftUser } from '../../models/user.interface';
import { IDraftSetup, IDraftedWrestler, ILiveDraft, ILeague } from '../../models/league.interface';
import { IWrestler } from '../../models/wrestlers.interface';
/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export const ActionTypes = {
  ADD_PARTICIPANT_TO_DRAFT: type('[DRAFT] Add Participant To Draft'),
  ADD_PARTICIPANT_TO_DRAFT_COMPLETE: type('[DRAFT] Add Participant To Draft Complete'),
  DELETE_PARTICIPANT_FROM_DRAFT: type('[DRAFT] Delete Participant from Draft'),
  DELETE_PARTICIPANT_FROM_DRAFT_COMPLETE: type('[DRAFT] Delete Participant from Draft Complete'),
  UPDATE_DRAFT_ORDER: type('[DRAFT] Update Draft Order '),
  UPDATE_DRAFT_DATE: type('[DRAFT] Update Draft Date '),
  UPDATE_NUMBER_DRAFTED: type('[DRAFT] Update the Number of Wrestlers Drafted'),
  UPDATE_TEAM_SCORES: type('[DRAFT] Update team scores'),
  UPDATE_TEAM_SCORES_COMPLETE:  type('[DRAFT] Update team scores complete'),
  CHECK_IF_DRAFTING: type('[DRAFT] Are we drafting yet? lets check'),
  PROCESS_IS_MY_TURN: type('[DRAFT] Process Is My Turn'),
  PROCESS_LIVE_DRAFT_PARTICIPANTS_UPDATE: type('[DRAFT] Process Live Draft Participants Update'),
  PROCESS_LIVE_DRAFT_TEAMS_UPDATE: type('[DRAFT] Process Live Draft Teams Update'),
  PROCESS_LIVE_DRAFT_UPDATE: type('[DRAFT] Process Live Draft Update'),
  UPDATE_DRAFT_SETUP: type('[DRAFT] Update Draft Setup'),
  SUBSCRIBE_TO_DRAFT: type('[DRAFT] Subscribe To Draft'),
  UNSUBSCRIBE_TO_DRAFT: type('[DRAFT] Unsubscribe To Draft'),
  SELECT_WRESTLER: type('[DRAFT] Select Wrestler'),
  SELECT_WRESTLER_TO_CURRENT_TEAM: type('[DRAFT ADMIN] Select Wrestler To Current Team'),
};

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions =
  AddParticipantToDraftAction |
  AddParticipantToDraftCompleteAction |
  DeleteParticipantFromDraftAction |
  DeleteParticipantFromDraftCompleteAction |
  UpdateDraftOrderAction |
  UpdateDraftTimeAction |
  UpdateDraftSetupAction |
  UpdateNumberOfWrestlersDraftedAction |
  UpdateTeamScoresAction |
  UpdateTeamScoresCompleteAction |
  ProcessIsMyTurnAction |
  ProcessLiveDraftUpdateAction |
  ProcessLiveDraftTeamUpdateAction |
  ProcessLiveDraftParticipantsUpdateAction |
  CheckIfDraftingAction |
  SubscribeToDraftAction |
  UnsubscribeToDraftAction |
  SelectWrestlerAction;

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */

export class AddParticipantToDraftAction implements Action {
  type = ActionTypes.ADD_PARTICIPANT_TO_DRAFT;
  constructor(public payload: {league: ILeague, name: string}) { }
}

export class AddParticipantToDraftCompleteAction implements Action {
  type = ActionTypes.ADD_PARTICIPANT_TO_DRAFT_COMPLETE;
  constructor(public payload: INewDraftUser) { }
}

export class DeleteParticipantFromDraftAction implements Action {
  type = ActionTypes.DELETE_PARTICIPANT_FROM_DRAFT;
  constructor(public payload: IDraftUser) { }
}

export class DeleteParticipantFromDraftCompleteAction implements Action {
  type = ActionTypes.DELETE_PARTICIPANT_FROM_DRAFT_COMPLETE;
  constructor(public payload: IDraftUser) { }
}

export class UpdateDraftOrderAction implements Action {
  type = ActionTypes.UPDATE_DRAFT_ORDER;
  constructor(public payload: {names: string[], league: ILeague }) { } // Orderd Names
}

export class UpdateTeamScoresAction implements Action {
  type = ActionTypes.UPDATE_TEAM_SCORES;
  constructor(public payload: boolean = false) { }
}
export class UpdateTeamScoresCompleteAction implements Action {
  type = ActionTypes.UPDATE_TEAM_SCORES_COMPLETE;
  constructor(public payload: dictionary<number>) { }
}

export class UpdateDraftTimeAction implements Action {
  type = ActionTypes.UPDATE_DRAFT_DATE;
  constructor(public payload: number) { }
}

export class UpdateNumberOfWrestlersDraftedAction implements Action {
  type = ActionTypes.UPDATE_NUMBER_DRAFTED;
  constructor(public payload: number) { }
}

export class UpdateDraftSetupAction implements Action {
  type = ActionTypes.UPDATE_DRAFT_SETUP;
  constructor(public payload: IDraftSetup) { }
}

export class ProcessLiveDraftUpdateAction implements Action {
  type = ActionTypes.PROCESS_LIVE_DRAFT_UPDATE;
  constructor(public payload: ILiveDraft) { }
}

export class ProcessLiveDraftParticipantsUpdateAction implements Action {
  type = ActionTypes.PROCESS_LIVE_DRAFT_PARTICIPANTS_UPDATE;
  constructor(public payload: dictionary<IDraftedWrestler>) { }
}

export class ProcessLiveDraftTeamUpdateAction implements Action {
  type = ActionTypes.PROCESS_LIVE_DRAFT_TEAMS_UPDATE;
  constructor(public payload: dictionary<dictionary<IDraftedWrestler>>) { }
}

export class ProcessIsMyTurnAction implements Action {
  type = ActionTypes.PROCESS_IS_MY_TURN;
  constructor(public payload: boolean) { }
}

export class CheckIfDraftingAction implements Action {
  type = ActionTypes.CHECK_IF_DRAFTING;
  constructor(public payload: boolean = false) { }
}

export class SubscribeToDraftAction implements Action {
  type = ActionTypes.SUBSCRIBE_TO_DRAFT;
  constructor(public payload: { leagueKey: string, isPrivate: boolean }) { }
}

export class UnsubscribeToDraftAction implements Action {
  type = ActionTypes.UNSUBSCRIBE_TO_DRAFT;
  constructor(public payload: boolean = false) { }
}

export class SelectWrestlerAction implements Action {
  type = ActionTypes.SELECT_WRESTLER;
  constructor(public payload: {id: string, weight: string, rank: number}) { }
}

export class SelectWrestlerToCurrentTeamAction implements Action {
  type = ActionTypes.SELECT_WRESTLER_TO_CURRENT_TEAM;
  constructor(public payload: {id: string, weight: string, rank: number}) { }
}

