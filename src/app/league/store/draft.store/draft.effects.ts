import { ProcessAllSelectedInDraft } from './../wrestlers.store/wrestlers.actions';
import { ILeague, IDraftedWrestler } from './../../models/league.interface';
import { Observable } from 'rxjs/Observable';
import { ISubscription } from 'rxjs/Subscription';
import { timer } from 'rxjs/observable/timer';
import { zip } from 'rxjs/observable/zip';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { WrestlingService } from '../../services/wrestling.api';
import { IUserWithKey, IDraftUser } from '../../models/user.interface';
import { SortByDraftOrderPipe } from '../../pipes/sort-by-draft-order.pipe';
import * as _ from 'lodash';
import * as wrestlerActions from '../wrestlers.store/wrestlers.actions';
import * as draftActions from './draft.actions';
import * as userActions from '../user.store/user.actions';
import * as timeConvert from '../../../util/time-convert';
import * as fromRoot from '../../../reducers/index';
import * as fromDraft from '../wrestling.store';
import { WrestlingLookupService } from '../../../game/services/wrestling-lookup.service';



@Injectable()
export class DraftEffects {
  private $numSelected_subscription: ISubscription;
  private $draftedByTeam_subscription: ISubscription;
  private $draftedParticipants_subscription: ISubscription;
  private $timer: ISubscription;
  constructor(
    private draftApi: WrestlingService,
    private actions$: Actions,
    private wrestlerLookup: WrestlingLookupService,
    private _store: Store<fromRoot.RootStoreState>) { }

  @Effect({ dispatch: false })
  updateDraftOrder$ = this.actions$
    .ofType(draftActions.ActionTypes.UPDATE_DRAFT_ORDER)
    .map(action => {
      const leagueInfo = action['payload'].league as ILeague;
      const names = action['payload'].names;
      this.draftApi
        .UpdateDraftOrder(leagueInfo.leagueKey, names, leagueInfo.isPrivate)
        .subscribe(done => {
          this._store.dispatch(new userActions.LoadLeagueAction(action['payload'].league.leagueKey));
        });
    });

  @Effect({ dispatch: false })
  updateDraftTime$ = this.actions$
    .ofType(draftActions.ActionTypes.UPDATE_DRAFT_DATE)
    .withLatestFrom(this._store)
    .map(([action, storeState]) => {
      this.draftApi
        .UpdateDraftDate(storeState['leagues'].leagues.currentLeague, action['payload'])
        .subscribe(done => {
          console.log('Updated Draft Date', done);
        });
    });

  @Effect({ dispatch: false })
  addUser$ = this.actions$
    .ofType(draftActions.ActionTypes.ADD_PARTICIPANT_TO_DRAFT)
    .map(action => {
      this.draftApi
        .AddParticipantToDraft(action['payload'].league, action['payload'].name)
        .subscribe(user => {
          if (!!user) {
            this._store.dispatch(new draftActions.AddParticipantToDraftCompleteAction(user));
            this._store.dispatch(new userActions.LoadLeagueAction(action['payload'].league.leagueKey));
          }
        });
    });

  @Effect({ dispatch: false })
  deleteUser$ = this.actions$
    .ofType(draftActions.ActionTypes.DELETE_PARTICIPANT_FROM_DRAFT)
    .withLatestFrom(this._store)
    .map(([action, storeState]) => {
      this.draftApi
        .DeleteParticipantFromDraft(storeState['leagues'].leagues.currentLeague, action['payload'])
        .subscribe(user => {
          if (!!user) {
            this._store.dispatch(new draftActions.DeleteParticipantFromDraftCompleteAction(user));
          }
        });
    });

  @Effect({ dispatch: false })
  checkDate$ = this.actions$
    .ofType(draftActions.ActionTypes.CHECK_IF_DRAFTING)
    .withLatestFrom(this._store)
    .map(([action, storeState]) => {
      const timeCheckInMS = (storeState['leagues'].draft.draftSetup.draftDate - Date.now());
      const timeCheckInMinutes = Math.floor(timeCheckInMS * timeConvert.MS_TO_MINUTES);
      const timeCheckInHours = Math.floor(timeCheckInMS * timeConvert.MS_TO_HOURS);
      let timerVal = 0; // one hour
      if (timeCheckInMS > 0 && timeCheckInHours < 3) {
        if (timeCheckInHours > 1) {
          timerVal = 1000 * 60 * 30; // check every 30 min
        } else if (timeCheckInMinutes > 30) {
          timerVal = 1000 * 60 * 10; // check every 10 min
        } else if (timeCheckInMinutes > 10) {
          timerVal = 1000 * 10; // check every 2 min
        } else if (timeCheckInMinutes > 3) {
          timerVal = 1000 * 40; // check every 40 seconds
        } else if (timeCheckInMinutes > 1) {
          timerVal = 1000 * 20; // check every 20 seconds
        } else {
          timerVal = 1000 * 1; // check every 1 seconds
        }
        if (this.$timer) { this.$timer.unsubscribe(); }
        this.$timer = timer(timerVal).subscribe(time => {
          this._store.dispatch(new draftActions.CheckIfDraftingAction());
        });
      }
    });

  @Effect({ dispatch: false })
  persistDraft$ = this.actions$
    .ofType(draftActions.ActionTypes.SUBSCRIBE_TO_DRAFT)
    .withLatestFrom(this._store)
    .map(([action, storeState]) => {
      this.draftApi
        .GetDraftSetup(action['payload'].leagueKey, action['payload'].isPrivate)
        .take(1)
        .subscribe(draftSetup => {
          this._store.dispatch(new draftActions.UpdateDraftSetupAction(draftSetup));
          this._store.dispatch(new draftActions.CheckIfDraftingAction());
          this._store
          .select(fromDraft.isDrafting).skipWhile(isDrafting => !isDrafting).take(1).subscribe(isDrafting => {
            const draftSortPipe = new SortByDraftOrderPipe();
            const draftUserValues = _.map(draftSetup.users, (value, key) => value);
            this.unsubscribe();
            this._store.select(fromDraft.getMyInfo).take(1).subscribe(myInfo => {
              this.$numSelected_subscription = this.draftApi
                .GetLiveDraftNumSelectedConnection(action['payload'].leagueKey, action['payload'].isPrivate)
                .subscribe(numSelected => {
                  const isItMe = draftSortPipe.isItMe(draftUserValues, numSelected, myInfo.id);
                  this._store.dispatch(new draftActions.ProcessIsMyTurnAction(isItMe));
                  this._store.dispatch(new draftActions.UpdateNumberOfWrestlersDraftedAction(numSelected));
                });
              this.$draftedParticipants_subscription = this.draftApi
                .GetLiveDraftParticipantsConnection(action['payload'].leagueKey, action['payload'].isPrivate)
                .subscribe(draftedParticipants => {
                  // this._store.dispatch(new wrestlerActions.MarkAllSelectedAction(draftedParticipants));
                  this._store.dispatch(new wrestlerActions.ProcessAllSelectedInDraft(draftedParticipants));
                  this._store.dispatch(new draftActions.ProcessLiveDraftParticipantsUpdateAction(draftedParticipants));
                });
              this.$draftedByTeam_subscription = this.draftApi
                .GetLiveDraftTeamsConnection(action['payload'].leagueKey, action['payload'].isPrivate)
                .subscribe(draftedTeams => {
                  this._store.dispatch(new draftActions.ProcessLiveDraftTeamUpdateAction(draftedTeams));
                });
            });
          });
        });
    });

  @Effect({ dispatch: false })
  unsubscribe$ = this.actions$
    .ofType(draftActions.ActionTypes.UNSUBSCRIBE_TO_DRAFT)
    .map(() => {
      this.unsubscribe();
      if (this.$timer) { this.$timer.unsubscribe(); }
    });

  @Effect({ dispatch: false })
  teamScores$ = this.actions$
    .ofType(draftActions.ActionTypes.PROCESS_LIVE_DRAFT_TEAMS_UPDATE)
    .map(action => {
      const teamScores: dictionary<number> = {};
      const allTeamsDict = action['payload'] as dictionary<dictionary<IDraftedWrestler>>;
      _.forEach(allTeamsDict, (wrestlers, teamKey) => {
        const teamScore = Object.values(wrestlers)
          .map(wrestler => wrestler.wrestlerId)
          .map(id => this.wrestlerLookup.getCurrentScore(id))
          .reduce( (a, b) => a + b );
          teamScores[teamKey] = teamScore;
      });
      this._store.dispatch(new draftActions.UpdateTeamScoresCompleteAction(teamScores));
    });

  @Effect({ dispatch: false })
  selectWrestler$ = this.actions$
    .ofType(draftActions.ActionTypes.SELECT_WRESTLER)
    .withLatestFrom(this._store)
    .map(([action, storeState]) => {
      const myInfo = storeState['leagues'].currentUser.myInfo;
      const currentLeague = storeState['leagues'].leagues.currentLeague;
      const draftUser = Object
        .values(storeState['leagues'].draft.draftSetup.users)
        .filter(w => w.key === myInfo.$key);
      this.draftApi.SelectWrestler(
        action['payload'],
        myInfo,
        currentLeague.$key,
        currentLeague.isPrivate,
        storeState['leagues'].draft.liveDraftRound,
        draftUser[0].draftOrder
      );
    });


  @Effect({ dispatch: false })
  selectWrestlerAdmin$ = this.actions$
    .ofType(draftActions.ActionTypes.SELECT_WRESTLER_TO_CURRENT_TEAM)
    .withLatestFrom(this._store)
    .map(([action, storeState]) => {
      const currentLeague = storeState['leagues'].leagues.currentLeague;
      const draftUsers = storeState['leagues'].draft.draftSetup.users as dictionary<IDraftUser>;
      const draftSortPipe = new SortByDraftOrderPipe();
      const currentUser = draftSortPipe
        .getCurrentUser(Object.values(draftUsers), storeState['leagues'].draft.liveDraftNumSelected);
      const currentIUser: IUserWithKey = {
        name: currentUser.name,
        $key: currentUser.key,
        id: currentUser.id,
        email: '',
        photo: '',
        leagues_ref: {},
        highlights: []
      };
      this.draftApi.SelectWrestler(
        action['payload'],
        currentIUser,
        currentLeague.$key,
        currentLeague.isPrivate,
        storeState['leagues'].draft.liveDraftRound,
        currentUser.draftOrder
      );
    });


  // helper function name(params:type) {

  private unsubscribe() {
    if (this.$numSelected_subscription) { this.$numSelected_subscription.unsubscribe(); }
    if (this.$draftedByTeam_subscription) { this.$draftedByTeam_subscription.unsubscribe(); }
    if (this.$draftedParticipants_subscription) { this.$draftedParticipants_subscription.unsubscribe(); }
  }

}
