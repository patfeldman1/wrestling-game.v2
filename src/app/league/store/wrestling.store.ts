import { EffectsModule } from '@ngrx/effects';
import { createSelector } from 'reselect';
import { combineReducers, ActionReducer, createFeatureSelector } from '@ngrx/store';
import * as fromWrestlers from './wrestlers.store/wrestlers.reducer';
import * as fromUser from './user.store/user.reducer';
import * as fromLeague from './leagues.store/leagues.reducer';
import * as fromDraft from './draft.store/draft.reducer';

export interface AppState {
  currentUser: fromUser.State;
  leagues: fromLeague.State;
  draft: fromDraft.State;
  wrestlers: fromWrestlers.State;
}

const reducers = {
  currentUser: fromUser.reducer,
  leagues: fromLeague.reducer,
  draft: fromDraft.reducer,
  wrestlers: fromWrestlers.reducer
};

const productionReducer: ActionReducer<AppState> = combineReducers(reducers);

export function reducer(state: any, action: any) {
  return productionReducer(state, action);
}


// The store selects
export const getLeaguesBase = createFeatureSelector<AppState>('leagues');
export const getDraft = createSelector(getLeaguesBase, state => state.draft);
export const getDraftSetup = createSelector(getDraft, fromDraft.getDraftSetup);
export const getLiveDraftedWrestlers = createSelector(getDraft, fromDraft.getLiveDraftedWrestlers);
export const getLiveDraftedTeams = createSelector(getDraft, fromDraft.getLiveDraftedTeams);
export const getNumberOfWrestlersDrafted = createSelector(getDraft, fromDraft.getNumberOfWrestlersDrafted);
export const getCurrentRound = createSelector(getDraft, fromDraft.getCurrentRoundOfDraft);
export const getTimeTillDraftInMS = createSelector(getDraft, fromDraft.getTimeTillDraftInMS);
export const getDraftUsers = createSelector(getDraft, fromDraft.getDraftUsers);
export const isMyTurn = createSelector(getDraft, fromDraft.isMyTurn);
export const isDrafting = createSelector(getDraft, fromDraft.isDrafting);
export const isDraftComplete = createSelector(getDraft, fromDraft.isDraftComplete);
export const getCurrentDraftUser = createSelector(getDraft, fromDraft.getCurrentDraftUser);
export const getCurrentDraftTeam = createSelector(getDraft, getCurrentDraftUser, fromDraft.getUserDraftedTeam);
export const getNextDraftUser = createSelector(getDraft, fromDraft.getNextDraftUser);
export const isDirectionDown = createSelector(getDraft, fromDraft.isSelectionDirectionDown);
// export const getUserDraftedTeam = (state: fromDraft.State, userKey: string) => state.liveDraftedTeams[userKey];

export const getLeagues = createSelector(getLeaguesBase, state => state.leagues);
export const getPublicLeagues = createSelector(getLeagues, fromLeague.getSelectedPublicLeagues);
export const getCurrentLeague = createSelector(getLeagues, fromLeague.getCurrentLeague);
export const getJoinLeagueStatus = createSelector(getLeagues, fromLeague.getJoinStatus);
export const isCurrentLeagueAdmin = createSelector(getLeagues, fromLeague.getIsLeagueAdmin);

export const getUser = createSelector(getLeaguesBase, state => state.currentUser);
export const getMyLeagues = createSelector(getUser, fromUser.getMyLeagues);
export const getMyLeaguesDraftSetup = createSelector(getUser, fromUser.getMyLeaguesDraftSetup);
export const getMyInfo = createSelector(getUser, fromUser.getMyInfo);
export const getMyId = createSelector(getUser, fromUser.getMyId);
export const getMyKey = createSelector(getUser, fromUser.getMyKey);
export const getMyName = createSelector(getUser, fromUser.getMyName);
export const getMyHighlights = createSelector(getUser, fromUser.getMyHighlights);
export const getMyDraftedTeam = createSelector(getDraft, getMyKey, fromDraft.getUserDraftedTeamByKey);
export const getMyDraftSlot = createSelector(getDraft, getMyId, fromDraft.getUserDraftSlot);

export const getWrestlers = createSelector(getLeaguesBase, state => state.wrestlers);
export const getSelectedWrestlers = createSelector(getWrestlers, fromWrestlers.getSelectedWrestlers);
export const getAllSelectedWrestlersByWeight = createSelector(getWrestlers, fromWrestlers.getSelectedWrestlersByWeight);
export const getSelectedWrestlersByWeight = (weight: string) =>
  createSelector(getWrestlers, (wrestlerState: fromWrestlers.State) =>
    (weight in wrestlerState.pickedWrestlersByWeight) ? wrestlerState.pickedWrestlersByWeight[weight] : {}
  );

