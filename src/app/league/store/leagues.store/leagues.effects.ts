import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { WrestlingService } from '../../services/wrestling.api';
import { AppState } from './../wrestling.store';
import { ILeague } from '../../models/league.interface';
import * as fromRoot from '../../../reducers/index';
import * as leagues from './leagues.actions';
import * as draftActions from './../draft.store/draft.actions';


@Injectable()
export class LeaguesEffects {
  constructor(
    private actions$: Actions,
    private leagueApi: WrestlingService,
    private _store: Store<fromRoot.RootStoreState>) { }

  @Effect()
  setLeague$ = this.actions$
    .ofType(leagues.ActionTypes.SET_CURRENT_LEAGUE)
    .switchMap(action => this.leagueApi.GetCurrentLeagueConnection(action['payload']))
    .map(leagueResults => {
      this._store.dispatch(new draftActions.SubscribeToDraftAction
        ({ leagueKey: leagueResults.$key, isPrivate: leagueResults.isPrivate }));
      return new leagues.ProcessLeagueUpdateAction(leagueResults);
    });

  @Effect({ dispatch: false })
  isAdmin$ = this.actions$
    .ofType(leagues.ActionTypes.PROCESS_LEAGUE_UPDATE)
    .withLatestFrom(this._store)
    .map(([action, storeState]) => {
      const leagueResults: ILeague = action['payload'] as ILeague;
      this._store.dispatch(
        new leagues.SetIsLeagueAdminAction(leagueResults.creatorId === storeState.auth.user.uid));
    });

  @Effect()
  search$: Observable<Action> = this.actions$
    .ofType(leagues.ActionTypes.PUBLIC_SEARCH)
    .switchMap(action => this.leagueApi.SearchPublicLeagues(action['payload']))
    .map(leagueResults => new leagues.PublicSearchCompleteAction(leagueResults));

  @Effect()
  joinLeague$: Observable<Action> = this.actions$
    .ofType(leagues.ActionTypes.JOIN_LEAGUE)
    .switchMap(action => this.leagueApi.JoinLeague(action['payload']))
    .map(joinResults => new leagues.JoinLeagueCompleteAction(joinResults));

  @Effect()
  createLeague$: Observable<Action> = this.actions$
    .ofType(leagues.ActionTypes.CREATE_PUBLIC_LEAGUE)
    .switchMap(action => this.leagueApi.CreateLeague_Public(action['payload']))
    .map(joinResults => new leagues.JoinLeagueCompleteAction(joinResults));

  @Effect()
  createPrivateLeague$: Observable<Action> = this.actions$
    .ofType(leagues.ActionTypes.CREATE_PRIVATE_LEAGUE)
    .switchMap(action => this.leagueApi.CreateLeague_Private(action['payload']))
    .map(joinResults => new leagues.JoinLeagueCompleteAction(joinResults));

  // @Effect()
  // removeDrafter$: Observable<Action> = this.actions$
  //     .ofType(leagues.ActionTypes.REMOVE_DRAFTER)
  //     .switchMap(action => this.leagueApi.RemoveDrafter(action['payload']))
  //     .map(joinResults => new leagues.JoinLeagueCompleteAction(joinResults));

}
