import { Action } from '@ngrx/store';
import { type } from '../../../util/type';
import { IWrestlers } from '../../models/wrestlers.interface';
import { IDraftedWrestler } from '../../models/league.interface';
/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export const ActionTypes = {
  SELECT_WRESTLER: type('[WRESTLERS] SELECTED IN DRAFT'),
  PROCESS_ALL_SELECT_WRESTLER: type('[WRESTLERS] PROCESS ALL SELECTED IN DRAFT'),
  RESET_ALL_SELECT_WRESTLERS: type('[WRESTLERS] Reset All Selected Wrestlers'),

};

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions = SelectedInDraft |
ResetAllSelectedWrestlers |
ProcessAllSelectedInDraft;

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
export class SelectedInDraft implements Action {
  type = ActionTypes.SELECT_WRESTLER;
  constructor(public payload: {weight: string, id: string}) { }
}

export class ProcessAllSelectedInDraft implements Action {
  type = ActionTypes.PROCESS_ALL_SELECT_WRESTLER;
  constructor(public payload: dictionary<IDraftedWrestler>) { }
}

export class ResetAllSelectedWrestlers implements Action {
  type = ActionTypes.RESET_ALL_SELECT_WRESTLERS;
  constructor(public payload: boolean = false) { }
}
