import { all_weights } from './../../../util/consts';
import { createSelector } from 'reselect';
import * as actions from './wrestlers.actions';
import * as _ from 'lodash';
import { IWrestlers } from '../../models/wrestlers.interface';
import { IDraftedWrestler } from '../../models/league.interface';

export interface State {
  pickedWrestlers: dictionary<string>;
  pickedWrestlersByWeight: dictionary<dictionary<string>>;
}

const initialState: State = {
  pickedWrestlers : {},
  pickedWrestlersByWeight: {}
};

export function reducer(state: State = initialState, action: actions.Actions): State {
  switch (action.type) {
    case actions.ActionTypes.SELECT_WRESTLER: {
      const weight = action.payload['weight'];
      const id = action.payload['id'];
      const selectorKey = action.payload['selectorKey'];
      const pickedWrestlers = Object.assign({}, state.pickedWrestlers);
      const pickedWrestlersByWeight = Object.assign({}, state.pickedWrestlersByWeight);
      pickedWrestlers[id] = selectorKey;
      if (!(weight in pickedWrestlersByWeight)) {
        pickedWrestlersByWeight[weight] = {};
      }
      pickedWrestlersByWeight[weight][id] = selectorKey;
      return Object.assign({}, state, {
        pickedWrestlers: pickedWrestlers,
        pickedWrestlersByWeight: pickedWrestlersByWeight
      });
    }
    case actions.ActionTypes.PROCESS_ALL_SELECT_WRESTLER: {
      const wrestlers = action.payload as dictionary<IDraftedWrestler>;
      const pickedWrestlers = Object.assign({}, state.pickedWrestlers);
      const pickedWrestlersByWeight = Object.assign({}, state.pickedWrestlersByWeight);
      _.forEach(wrestlers, (wrestler, key) => {
        pickedWrestlers[wrestler.wrestlerId] = wrestler.draftedByKey;
        if (!(wrestler.wrestlerWeight in pickedWrestlersByWeight)) {
          pickedWrestlersByWeight[wrestler.wrestlerWeight] = {};
        }
        pickedWrestlersByWeight[wrestler.wrestlerWeight][wrestler.wrestlerId] = wrestler.draftedByKey;
      });
      return Object.assign({}, state, {
        pickedWrestlers: pickedWrestlers,
        pickedWrestlersByWeight: pickedWrestlersByWeight
      });
    }
    case actions.ActionTypes.RESET_ALL_SELECT_WRESTLERS: {
      return initialState;
    }
    default: {
      return state;
    }
  }
}

export const getSelectedWrestlers = (state: State) => state.pickedWrestlers;
export const getSelectedWrestlersByWeight = (state: State) => state.pickedWrestlersByWeight;
// export const getSelectedWrestlerId = (state: State) => state.selectedWrestlerId;
// export const getSelectedWeight = (state: State) => state.selectedWeight;
// export const getWrestlersById = (state: State) => state.wrestlers.wrestlersById;
// export const getAllWeights = createSelector(getWrestlers, (wrestlers) => {
//   return Object.keys(wrestlers.wrestlersByWeight);
// });
// export const getSelectedWrestler = createSelector(getWrestlers, getSelectedWrestlerId, (wrestlers, id) => {
//   return wrestlers.wrestlersById[id];
// });

// export const getAllWrestlersByWeight = createSelector(getWrestlers, (wrestlers) => {
//   const retVal: any = {};
//   if (Object.keys(wrestlers.wrestlersByWeight).length === 0) { return null; }
//   _.forIn(wrestlers.wrestlersByWeight, (value, key) => {
//     retVal[key] = (wrestlers.wrestlersByWeight[key].map(wrestlerId => wrestlers.wrestlersById[wrestlerId]));
//   });
//   return retVal;
// });

// export const getWrestlersAtSelectedWeight = createSelector(getWrestlers, getSelectedWeight, (wrestlers, weight) => {
//   return wrestlers.wrestlersByWeight[weight].map(wrestlerId => wrestlers.wrestlersById[wrestlerId]);
// });
