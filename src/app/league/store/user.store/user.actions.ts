import { IDraftSetup } from './../../models/league.interface';
import { Action } from '@ngrx/store';
import { type } from '../../../util/type';
import { IUserWithKey } from '../../models/user.interface';
import { IUserLeagueRef } from '../../models/user-league.interface';
import { IWrestler } from '../../models/wrestlers.interface';
import { IWrestlerHighlight, IHighlightDetails } from '../../models/highlights.class';
import * as firebase from 'firebase/app';
import { ILeague } from "../../models/league.interface";
/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export const ActionTypes = {
  UPDATE_USER: type('[USER] Update User'),
  SET_USER: type('[USER] Set User'),
  SET_NO_USER: type('[USER] Set NO User'),
  LOAD_LEAGUE: type('[USER] Load all league details'),
  LOAD_LEAGUE_COMPLETE: type('[USER] Load of league complete'),
  ADD_USER_LEAGUE: type('[USER] Add User League'),
  ADD_USER_LEAGUE_COMPLETE: type('[USER] Add User League Complete'),
  ADD_WRESTLER_HIGHLIGHT: type('[USER] Add Wrestler Highlight'),
  ADD_WRESTLER_HIGHLIGHT_COMPLETE: type('[USER] Add Wrestler Highlight Complete'),
  REMOVE_WRESTLER_HIGHLIGHT: type('[USER] Remove Wrestler Highlight'),
  REMOVE_WRESTLER_HIGHLIGHT_COMPLETE: type('[USER] Remove Wrestler Highlight Complete'),
};

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type Actions =
  UpdateUserAction |
  SetUserAction |
  SetNoUserAction |
  LoadLeagueAction |
  LoadLeagueCompleteAction |
  AddUserLeagueAction |
  AddUserLeagueCompleteAction |
  AddWrestlerHighlightAction |
  AddWrestlerHighlightCompleteAction |
  RemoveWrestlerHighlightAction |
  RemoveWrestlerHighlightCompleteAction;

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */

export class UpdateUserAction implements Action {
  type = ActionTypes.UPDATE_USER;
  constructor(public payload: boolean = false) { }
}

export class SetUserAction implements Action {
  type = ActionTypes.SET_USER;
  constructor(public payload: IUserWithKey) { }
}

export class SetNoUserAction implements Action {
  type = ActionTypes.SET_NO_USER;
  constructor(public payload: boolean = false) { }
}

export class LoadLeagueAction implements Action {
  type = ActionTypes.LOAD_LEAGUE;
  constructor(public payload: string) { }
}

export class LoadLeagueCompleteAction implements Action {
  type = ActionTypes.LOAD_LEAGUE_COMPLETE;
  constructor(public payload: {key: string, draft: IDraftSetup}) { }
}

export class AddUserLeagueAction implements Action {
  type = ActionTypes.ADD_USER_LEAGUE;
  constructor(public payload: IUserLeagueRef) { }
}

export class AddUserLeagueCompleteAction implements Action { // NOOP ACTION. TODO:: Delete league if false
  type = ActionTypes.ADD_USER_LEAGUE_COMPLETE;
  constructor(public payload: boolean) { }
}

export class AddWrestlerHighlightAction implements Action {
  type = ActionTypes.ADD_WRESTLER_HIGHLIGHT;
  constructor(public payload: { wrestler: IWrestler, highlight: IHighlightDetails }) { }
}

export class AddWrestlerHighlightCompleteAction implements Action {
  type = ActionTypes.ADD_WRESTLER_HIGHLIGHT_COMPLETE;
  constructor(public payload: IWrestlerHighlight) { }
}

export class RemoveWrestlerHighlightAction implements Action {
  type = ActionTypes.REMOVE_WRESTLER_HIGHLIGHT;
  constructor(public payload: IWrestler) { }
}

export class RemoveWrestlerHighlightCompleteAction implements Action {
  type = ActionTypes.REMOVE_WRESTLER_HIGHLIGHT_COMPLETE;
  constructor(public payload: number) { }
}
