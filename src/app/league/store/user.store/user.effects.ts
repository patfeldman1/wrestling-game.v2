import { ILeague } from './../../models/league.interface';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { WrestlingService } from '../../services/wrestling.api';
import { AngularFireDatabase } from 'angularfire2/database';
import { AppState } from '../wrestling.store';
import { AngularFireAuth } from 'angularfire2/auth';
import { IUser, IUserWithKey } from '../../models/user.interface';
import * as fromRoot from '../../../reducers/index';
import * as firebase from 'firebase/app';
import * as userActions from './user.actions';
import { UserService } from '../../services/user.api';


@Injectable()
export class UserEffects {
  constructor(
    private actions$: Actions,
    private leagueApi: WrestlingService,
    private userApi: UserService,
    private afDb: AngularFireDatabase,
    private _store: Store<fromRoot.RootStoreState>,
  ) {
    this._store.select(fromRoot.getCurrentUser).skipWhile(user => !user || user.uid === null).subscribe(user => {
      this._store.dispatch(new userActions.UpdateUserAction());
    });
  }

  @Effect()
  newLeague$ = this.actions$
    .ofType(userActions.ActionTypes.ADD_USER_LEAGUE)
    .switchMap(action => this.leagueApi.AddLeagueToCurrentUser(action['payload']))
    .map(complete => new userActions.AddUserLeagueCompleteAction(complete));

  @Effect({ dispatch: false })
  updateUser$ = this.actions$
    .ofType(userActions.ActionTypes.UPDATE_USER)
    .withLatestFrom(this._store)
    .map(([action, storeState]) => {
      const user = storeState.auth.user;
      if (!user || !user.uid) {
        this._store.dispatch(new userActions.SetNoUserAction);
      } else {
        this.userApi.GetUserInformation(user).take(1).subscribe(meWithKey => {
          this._store.dispatch(new userActions.SetUserAction(meWithKey));
        });
      }
    });
  @Effect({ dispatch: false })
  getLeagueInfo$ = this.actions$
    .ofType(userActions.ActionTypes.SET_USER)
    .map(action => {
      const user = action['payload'] as IUserWithKey;
      if ('leagues_ref' in user) {
        Object.values(user.leagues_ref).forEach( league => {
          this._store.dispatch(new userActions.LoadLeagueAction(league.leagueKey));
        });
      }
    });

  @Effect({ dispatch: false })
  loadLeagueInfo$ = this.actions$
    .ofType(userActions.ActionTypes.LOAD_LEAGUE)
    .map(action => {
      const key = action['payload'];
      this.leagueApi.GetLeagueDraftInformation(key).take(1).subscribe(draftSetup => {
        this._store.dispatch(new userActions.LoadLeagueCompleteAction({key: key, draft: draftSetup}));
      });
    });

  // @Effect()
  // addHighlight$ = this.actions$
  //   .ofType(userActions.ActionTypes.ADD_WRESTLER_HIGHLIGHT)
  //   .switchMap(query => this._store.select(fromRoot.getMyKey).take(1), (query, key) => ({ query: query, key: key }))
  //   .switchMap(keyAndQuery => {
  //     const priority = 1;
  //     return this.leagueApi.AddWrestlerToMyHighlights
  //       (keyAndQuery.key, keyAndQuery.query.payload.wrestler, keyAndQuery.query.payload.highlight.id, priority);
  //   })
  //   .map(wrestlerWithKey => new userActions.AddWrestlerHighlightCompleteAction(wrestlerWithKey));

  // @Effect()
  // removeHighlight$ = this.actions$
  //   .ofType(userActions.ActionTypes.REMOVE_WRESTLER_HIGHLIGHT)
  //   .switchMap(query => this._store.select(fromRoot.getMyKey).take(1), (query, key) => ({ query: query, key: key }))
  //   .switchMap(keyAndQuery =>
  //     this.leagueApi.RemoveWrestlerToMyHighlights(keyAndQuery.key, keyAndQuery.query.payload.id))
  //   .map(wrestlerId => new userActions.RemoveWrestlerHighlightCompleteAction(wrestlerId));

}
