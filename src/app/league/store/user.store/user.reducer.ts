import { IDraftSetup } from './../../models/league.interface';
import * as actions from './user.actions';
import * as _ from 'lodash';
import { IUserWithKey } from '../../models/user.interface';
import { IWrestlerHighlight } from '../../models/highlights.class';
import { IUserLeagueRef } from '../../models/user-league.interface';
import { ILeague } from '../../models/league.interface';
export interface State {
  myInfo: IUserWithKey;
  myDrafts: dictionary<IDraftSetup>;
}

const initialState: State = {
  myInfo: {
    name: '',
    photo: '',
    email: '',
    id: '',
    $key: '',
    leagues_ref: {},
    highlights: []
  },
  myDrafts: {}
};
export function reducer(state: State = initialState, action: actions.Actions): State {
  switch (action.type) {
    case actions.ActionTypes.SET_USER: {
      const myNewInfo = Object.assign({}, action.payload) as IUserWithKey;
      const payload = action.payload as IUserWithKey;
      myNewInfo.$key = payload.$key;
      myNewInfo.photo = (!!payload.photo) ? payload.photo : '';
      myNewInfo.highlights = _.map(payload.highlights, (value, key) => {
        return Object.assign({}, value, { key: key });
      });

      return Object.assign({}, state, {
        myInfo: myNewInfo
      });
    }
    // case actions.ActionTypes.SET_NO_USER: {
    //   return Object.assign({}, state, {
    //     authState: ELogin.NoUser
    //   });
    // }
    case actions.ActionTypes.LOAD_LEAGUE_COMPLETE: {
      const draftSetup = action.payload['draft'] as IDraftSetup;
      const key = action.payload['key'];
      const all_leagues = Object.assign({}, state.myDrafts);
      all_leagues[key] = draftSetup;
      return Object.assign({}, state, {
        myDrafts: all_leagues
      });
    }
    case actions.ActionTypes.ADD_USER_LEAGUE: {
      const newLeagueObj = (!state.myInfo.leagues_ref) ? {} : Object.assign({}, state.myInfo.leagues_ref);
      const leagueRef = action.payload as IUserLeagueRef;
      newLeagueObj[leagueRef.leagueKey] = leagueRef;
      const newInfo: IUserWithKey = Object.assign({}, state.myInfo, {
        leagues_ref: newLeagueObj
      });
      return Object.assign({}, state, {
        myInfo: newInfo
      });
    }
    case actions.ActionTypes.ADD_WRESTLER_HIGHLIGHT_COMPLETE: {
      const newHighlights: IWrestlerHighlight[] = [...state.myInfo.highlights, action.payload as IWrestlerHighlight];
      const newMyInfo = Object.assign({}, state.myInfo, {
        highlights: newHighlights
      });
      return Object.assign({}, state, {
        myInfo: newMyInfo
      });
    }
    case actions.ActionTypes.REMOVE_WRESTLER_HIGHLIGHT_COMPLETE: {
      const newHighlights: IWrestlerHighlight[] = [...state.myInfo.highlights]
        .filter(highlight => highlight.wrestlerId !== action.payload);
      const newMyInfo = Object.assign({}, state.myInfo, {
        highlights: newHighlights
      });
      return Object.assign({}, state, {
        myInfo: newMyInfo
      });
    }
    default: {
      return state;
    }
  }
}

export const getMyLeagues = (state: State) => state.myInfo.leagues_ref;
export const getMyInfo = (state: State) => state.myInfo;
export const getMyId = (state: State) => state.myInfo.id;
export const getMyKey = (state: State) => state.myInfo.$key;
export const getMyName = (state: State) => state.myInfo.name;
export const getMyHighlights = (state: State) => state.myInfo.highlights;
export const getMyLeaguesDraftSetup = (state: State) => state.myDrafts;
