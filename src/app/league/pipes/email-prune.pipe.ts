import { Injectable, Pipe } from '@angular/core';
import { PipeTransform } from '@angular/core/src/change_detection/pipe_transform';

@Pipe({
	name: 'emailPrune'
})
@Injectable()
export class EmailPrunePipe implements PipeTransform {
	transform(fullEmailAddress: string): string {
		if (!fullEmailAddress) { return ''; }
		return fullEmailAddress.split('@')[0];
	}
}
