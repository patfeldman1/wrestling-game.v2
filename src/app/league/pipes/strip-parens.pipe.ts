import { Injectable, Pipe, PipeTransform } from '@angular/core';
@Pipe({
	name: 'stripParens'
})
@Injectable()
export class StripParensPipe implements PipeTransform {
	transform(parenString: string): string {
		if (!parenString) { return ''; }
		return parenString.replace('(', '').replace(')', '');
	}
}
