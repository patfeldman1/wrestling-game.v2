import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { IWrestler } from '../models/wrestlers.interface';

@Pipe({
	name: 'totalScore'
})
@Injectable()
export class TotalScorePipe implements PipeTransform {
	transform(wrestler: IWrestler): number {
		if (!wrestler) { return 0; }
		let sum = 0;
		wrestler.matches.forEach(match => {
			sum += match.teamPoints;
		});
		return sum;
	}
}
