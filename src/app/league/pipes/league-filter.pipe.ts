import { Injectable, Pipe } from '@angular/core';
import { PipeTransform } from '@angular/core/src/change_detection/pipe_transform';

@Pipe({ name: 'leagueFilter' })
@Injectable()
export class LeagueFilterPipe implements PipeTransform {
    transform(objectArray: {}, leagueType: string): {} {
        if (!objectArray) { return []; }
        return objectArray;
    }
}
