import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { IDraftUser } from '../models/user.interface';
import { Rules, RuleType } from '../../util/rules.class';

@Pipe({
	name: 'sortByDraftOrder'
})
@Injectable()
export class SortByDraftOrderPipe implements PipeTransform {
	transform(users: IDraftUser[], numSelected: number, addExtra: boolean = false): IDraftUser[] {
		if (!users) { return []; }
		if (!numSelected) { numSelected = 0; }
		// TODO make the rule injected from league data
		const totalSelections = Rules.NumberOrRounds(RuleType.EleventhPick) * users.length;
		if (numSelected >= totalSelections) { return []; }

		let numReturned = addExtra ? users.length + 1 : users.length;
		const maxShow = totalSelections - numSelected;
		numReturned = Math.min(maxShow, numReturned);
		const clone = [...users.sort((a: IDraftUser, b: IDraftUser) => a.draftOrder - b.draftOrder)];
		const dupUsers = [...clone.slice(), ...clone.slice().reverse(), ...clone.slice()];
		const nextUp = numSelected % (users.length * 2);
		return dupUsers.slice(nextUp, nextUp + numReturned);
	}

	public isItMe(users: IDraftUser[], numSelected: number, myId: string): boolean {
		const userList = this.transform(users, numSelected);
		if (userList.length > 0) {
			return (userList[0].id === myId);
		}
		return false;
	}

	public getCurrentUser(users: IDraftUser[], numSelected: number): IDraftUser {
		const userList = this.transform(users, numSelected);
		if (userList.length > 0) {
			return userList[0];
		}
		return null;
	}
}
