import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { IDraftSetup } from '../models/league.interface';

@Pipe({
  name: 'dateHasHappened'
})
@Injectable()
export class DateHasHappenedPipe implements PipeTransform {
  transform(date: number): boolean {
    if (!date) { return false; } // FIX THIS PBF false;
    return (date < Date.now());
  }
}
