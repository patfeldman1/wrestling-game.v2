import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { ILeagueTeam } from '../models/league-team.interface';

@Pipe({
	name: 'orderByScore'
})
@Injectable()
export class OrderByScorePipe implements PipeTransform {
	transform(teams: ILeagueTeam[]): ILeagueTeam[] {
		if (!teams) { return []; }
		return teams.sort((a, b) => a.teamScore - b.teamScore);
	}
}
