import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { IScore } from '../models/scores.interface';
@Pipe({
	name: 'reverseMatchOrder'
})
@Injectable()
export class ReverseMatchOrderPipe implements PipeTransform {
	transform(matches: IScore[]): IScore[] {
		if (!matches) { return []; }
		// TODO, make this better
		if (matches[0].matchDesc.indexOf('Champ. Round 1') >= 0 || matches[0].matchDesc.indexOf('Prelim') >= 0) {
			return matches;
		} else {
			return matches.reverse();
		}

	}
}
