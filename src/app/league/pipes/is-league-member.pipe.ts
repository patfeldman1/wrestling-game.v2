import { Injectable, Pipe } from '@angular/core';
import * as _ from 'lodash';
import { PipeTransform } from '@angular/core/src/change_detection/pipe_transform';
import { ILeague } from '../models/league.interface';

@Pipe({
	name: 'isLeagueMember'
})
@Injectable()
export class IsLeagueMemberPipe implements PipeTransform {
	transform(league: ILeague, uid: string): boolean {
		if (!league || !league.draftSetup) { return false; }
		let isMember = false;
		_.forOwn(league.draftSetup.users, user => {
			if (user.id === uid) {
				isMember = true;
				return;
			}
		});
		return isMember;
	}
}
