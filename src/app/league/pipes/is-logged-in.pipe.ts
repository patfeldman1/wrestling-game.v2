import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { ELogin } from '../../core/models/login.enum';

@Pipe({
  name: 'isLoggedIn'
})
@Injectable()
export class IsLoggedInPipe implements PipeTransform {
  transform(value: ELogin): boolean {
    return value === ELogin.LoggedIn;
  }
}
