import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { IWrestler } from '../models/wrestlers.interface';

@Pipe({
	name: 'byWeight'
})
@Injectable()
export class WrestlersByWeightPipe implements PipeTransform {
	transform(wrestlers: IWrestler[], weight: string): IWrestler[] {
		if (!wrestlers) { return []; }
		return wrestlers.filter(wrestler => {
			return !!wrestler && wrestler.weight === weight;
		}).sort((a: IWrestler, b: IWrestler) => a.rank - b.rank);
	}
}
