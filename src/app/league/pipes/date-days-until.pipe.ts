import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { IDraftSetup } from '../models/league.interface';

@Pipe({
  name: 'dateDaysUntil'
})
@Injectable()
export class DateDaysUntilPipe implements PipeTransform {
  transform(draftDate: number): string {
    if (!draftDate) { return ''; } // FIX THIS PBF false;
    return (draftDate - Date.now()).toString();
  }
}
