import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { IWrestlerHighlight, defaultHighlights } from '../models/highlights.class';

@Pipe({
	name: 'highlightIcon'
})
@Injectable()
export class HighlightIconPipe implements PipeTransform {
	transform(highlight: IWrestlerHighlight): string {
		if (!highlight) { return ''; }
		return defaultHighlights.find(type => type.id === highlight.highlightId).icon;
	}
}
