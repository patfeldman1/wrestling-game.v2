import { Injectable, Pipe, PipeTransform } from '@angular/core';
@Pipe({
	name: 'roundToString'
})
@Injectable()
export class RoundToStringPipe implements PipeTransform {
	transform(desc: string): string {
		if (!desc) { return ''; }
		return desc;
	}
}
