import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'orderByDraft'
})
@Injectable()
export class OrderByDraftPipe implements PipeTransform {
	transform(users: any[]): any[] {
		if (!users) { return []; }
		return users.sort((a: any, b: any) => a.draftOrder - b.draftOrder);
	}
}
