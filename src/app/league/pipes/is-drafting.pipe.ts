import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { IDraftSetup } from '../models/league.interface';

@Pipe({
	name: 'isDrafting'
})
@Injectable()
export class IsDraftingPipe implements PipeTransform {
	transform(draft: IDraftSetup): boolean {
		if (!draft || !draft.draftDate) { return false; } // FIX THIS PBF false;
		return (draft.draftDate < Date.now());
	}
}
