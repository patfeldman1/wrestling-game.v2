import { GetMyWrestlerByWeightPipe } from './get-my-wrestler-by-weight.pipe';
import { TopThreeWrestlersPipe } from './top-three-wrestlers.pipe';
import { LeagueFilterPipe } from './league-filter.pipe';
import { IsLoggedInPipe } from './is-logged-in.pipe';
import { IsLeagueMemberPipe } from './is-league-member.pipe';
import { ValuesPipe } from './values.pipe';
import { WrestlersByWeightPipe } from './wrestlers-by-weight.pipe';
import { OrderByDraftPipe } from './order-by-draft.pipe';
import { EmailPrunePipe } from './email-prune.pipe';
import { IsDraftingPipe } from './is-drafting.pipe';
import { SortByDraftOrderPipe } from './sort-by-draft-order.pipe';
import { TimeRemainingInHoursPipe } from './time-remaining.pipe';
import { LeagueTeamByKeyPipe } from './league-team-by-key.pipe';
import { HighlightIconPipe } from './highlight-icon.pipe';
import { HighlightBgPipe } from './highlight-bg.pipe';
import { HighlightTextPipe } from './highlight-text.pipe';
import { CharAtPipe } from './char-at.pipe';
import { TotalScorePipe } from './total-score.pipe';
import { TotalTeamScorePipe } from './total-team-score.pipe';
import { JustNamePipe } from './just-name.pipe';
import { StripParensPipe } from './strip-parens.pipe';
import { RoundToStringPipe } from './round-to-string.pipe';
import { ReverseMatchOrderPipe } from './reverse-match-order.pipe';
import { OrderByScorePipe } from './team-order-by-score.pipe';
import { DateDaysUntilPipe } from './date-days-until.pipe';
import { DateHasHappenedPipe } from './date-has-happened.pipe';

export const PIPES = [
  LeagueFilterPipe,
  IsLoggedInPipe,
  IsLeagueMemberPipe,
  ValuesPipe,
  WrestlersByWeightPipe,
  OrderByDraftPipe,
  EmailPrunePipe,
  IsDraftingPipe,
  SortByDraftOrderPipe,
  TimeRemainingInHoursPipe,
  LeagueTeamByKeyPipe,
  HighlightIconPipe,
  HighlightBgPipe,
  HighlightTextPipe,
  CharAtPipe,
  TotalScorePipe,
  TotalTeamScorePipe,
  JustNamePipe,
  StripParensPipe,
  RoundToStringPipe,
  ReverseMatchOrderPipe,
  OrderByScorePipe,
  DateDaysUntilPipe,
  DateHasHappenedPipe,
  TopThreeWrestlersPipe,
  GetMyWrestlerByWeightPipe
];
