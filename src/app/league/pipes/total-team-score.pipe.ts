import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { IWrestler } from '../models/wrestlers.interface';

@Pipe({
	name: 'totalTeamScore'
})
@Injectable()
export class TotalTeamScorePipe implements PipeTransform {
	transform(team: IWrestler[]): number {
		if (!team) { return 0; }
		let sum = 0;
		team.forEach(wrestler => {
			wrestler.matches.forEach(match => {
				sum += match.teamPoints;
			});
		});
		return sum;
	}
}
