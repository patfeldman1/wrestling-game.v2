import { WrestlerInfo } from './../../game/models/wrestler-info.model';
import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { IWrestler } from '../models/wrestlers.interface';

@Pipe({
	name: 'topThree'
})
@Injectable()
export class TopThreeWrestlersPipe implements PipeTransform {
	transform(filteredList: dictionary<String>, allWrestlers: dictionary<WrestlerInfo> ): WrestlerInfo[] {
		const filterThese = Object.keys(filteredList);
		if (!filterThese) { return []; }
		return Object
			.values(allWrestlers)
			.filter(wrestler =>  filterThese.indexOf(wrestler.global_pid) < 0)
			.sort((a, b) => a.seed - b.seed)
			.slice(0, 3);
	}
}
