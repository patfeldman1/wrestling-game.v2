import { Injectable, Pipe } from '@angular/core';
import { PipeTransform } from '@angular/core/src/change_detection/pipe_transform';

@Pipe({
	name: 'charAt'
})
@Injectable()
export class CharAtPipe implements PipeTransform {
	transform(fullString: string, charAt: number = 0): string {
		if (!fullString) { return ''; }
		return fullString.slice(charAt, charAt + 1);
	}
}
