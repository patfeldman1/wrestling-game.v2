import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { IDraftedWrestler } from '../models/league.interface';

@Pipe({
	name: 'teamByKey'
})
@Injectable()
export class LeagueTeamByKeyPipe implements PipeTransform {
	transform(team: dictionary<IDraftedWrestler[]>, teamKey: string): IDraftedWrestler[] {
		if (!team) { return []; }
		return team[teamKey];
	}
}
