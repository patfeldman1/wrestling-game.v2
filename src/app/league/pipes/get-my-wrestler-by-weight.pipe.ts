import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { IWrestler } from '../models/wrestlers.interface';
import { IDraftedWrestler } from '../models/league.interface';

@Pipe({
	name: 'getMyWrestlerByWeight'
})
@Injectable()
export class GetMyWrestlerByWeightPipe implements PipeTransform {
	transform(wrestlers: IDraftedWrestler[], weight: string): IDraftedWrestler {
		if (!wrestlers) { return null; }
		return wrestlers.find(wrestler => wrestler.wrestlerWeight === weight);
	}
}
