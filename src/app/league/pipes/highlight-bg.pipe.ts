import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { IWrestlerHighlight, defaultHighlights } from '../models/highlights.class';

@Pipe({
	name: 'highlightBg'
})
@Injectable()
export class HighlightBgPipe implements PipeTransform {
	transform(highlight: IWrestlerHighlight): string {
		if (!highlight) { return ''; }
		return defaultHighlights.find(type => type.id === highlight.highlightId).color;
	}
}
