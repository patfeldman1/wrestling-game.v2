import { Injectable, Pipe, PipeTransform } from '@angular/core';
import * as timeConvert from '../../util/time-convert';
@Pipe({
	name: 'timeRemainingInHours'
})
@Injectable()
export class TimeRemainingInHoursPipe implements PipeTransform {
	transform(timeLeftInMS: number, timeComing: number): string {
		if (!timeLeftInMS) { return 'Coming Soon'; }
		if (!timeComing) { timeComing = Date.now(); }
		if (timeLeftInMS < 0) { return 'DRAFTING'; }
		if ((timeLeftInMS * timeConvert.MS_TO_DAYS) > 3) {
			return Math.floor(timeLeftInMS * timeConvert.MS_TO_DAYS).toString() + ' Days';
		} else if ((timeLeftInMS * timeConvert.MS_TO_DAYS) > 2) {
			return Math.floor(timeLeftInMS * timeConvert.MS_TO_DAYS).toString() + ' Days, '
				+ Math.floor(timeLeftInMS * timeConvert.MS_TO_HOURS).toString() + ' Hours';
		} else if ((timeLeftInMS * timeConvert.MS_TO_HOURS) > 2) {
			return Math.floor(timeLeftInMS * timeConvert.MS_TO_HOURS).toString() + ' Hours';
		} else if ((timeLeftInMS * timeConvert.MS_TO_MINUTES) > 2) {
			return Math.floor(timeLeftInMS * timeConvert.MS_TO_MINUTES).toString() + ' Minutes';
		} else {
			return Math.floor(timeLeftInMS * timeConvert.MS_TO_SECONDS).toString() + ' Seconds';
		}
	}
}
