import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { IWrestlerHighlight, defaultHighlights } from '../models/highlights.class';

@Pipe({
	name: 'highlightText'
})
@Injectable()
export class HighlightTextPipe implements PipeTransform {
	transform(highlight: IWrestlerHighlight): string {
		if (!highlight) { return ''; }
		return defaultHighlights.find(type => type.id === highlight.highlightId).textColor;
	}
}
