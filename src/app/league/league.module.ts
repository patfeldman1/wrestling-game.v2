import { AllTeamResultInfo } from './../game/models/team-results.model';
import { LeagueMatchupComponent } from './components/league-matchup/league-matchup.component';
import { LeagueBracketComponent } from './components/league-bracket/league-bracket.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { BrowserModule } from '@angular/platform-browser';
import { LeagueRoutingModule } from './league-routing.module';
import { LeagueBaseComponent } from './pages/base/league-base.component';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../material.module';
import { LeagueIntroComponent } from './components/league-intro/league-intro.component';
import { LeagueListComponent } from './components/league-list/league-list.component';
import { LeagueCardComponent } from './components/league-card/league-card.component';
import { UserGraphicComponent } from './components/user-graphic/user-graphic.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { DraftEffects } from './store/draft.store/draft.effects';
import { UserEffects } from './store/user.store/user.effects';
import { WrestlingService } from './services/wrestling.api';
import { LeagueHomeComponent } from './pages/league-home/league-home.component';
import { LeaguesEffects } from './store/leagues.store/leagues.effects';
import * as pipes from './pipes';
import * as wrestlingStore from './store/wrestling.store';
import { CreateLeagueDialogComponent } from './dialogs/create-league/create-league.dialog.component';
import { LeagueStandingsComponent } from './components/league-standings/league-standings.component';
import { LeagueResultsComponent } from './components/league-results/league-results.component';
import { LeagueTeamComponent } from './components/league-team/league-team.component';
import { LeagueDraftComponent } from './components/league-draft/league-draft.component';
import { LeagueBracketsComponent } from './components/league-brackets/league-brackets.component';
import { LeagueDraftResultsComponent } from './components/league-draft-results/league-draft-results.component';
import { LeagueAllTeamsComponent } from './components/league-all-teams/league-all-teams.component';
import { TeamDraftSummaryComponent } from './components/team-draft-summary/team-draft-summary.component';
import { WrestlingServiceMock } from '../mock-services/wrestling.api.mock';
import { UserService } from './services/user.api';
import { UserServiceMock } from '../mock-services/user.api.mock';
import { WeightListComponent } from './components/weight-list/weight-list.component';
import { AllSelectionsComponent } from './dialogs/all-selections/all-selections.component';
import { WrestlerSelectComponent } from './dialogs/wrestler-select/wrestler-select.component';
import { GameElementsModule } from '../game/game-elements.module';
import { ConfirmDialog } from './dialogs/confirm/confirm.dialog';
import { LeagueSearchComponent } from './components/league-search/league-search.component';
import { AddPlayerToDraftComponent } from './dialogs/add-player-to-draft/add-player-to-draft.component';
import { ViewParticipantsDialogComponent } from './dialogs/view-participants/view-participants.dialog';
import { DragulaModule } from 'ng2-dragula/ng2-dragula';
import { ReorderDraftDialogComponent } from './dialogs/reorder-draft/reorder-draft.dialog';
import { LeagueBracketSummaryComponent } from './components/league-bracket-summary/league-bracket-summary.component';
import { LeagueWrestlerLinkComponent } from './components/league-wrestler-link/league-wrestler-link.component';
import { LeagueMyWrestlerComponent } from './components/league-my-wrestler-card/league-my-wrestler-card.component';
import { LeagueBracketDialogComponent } from './dialogs/league-bracket/league-bracket.dialog';
import { AllTeamsDialogComponent } from './dialogs/all-teams.dialog/all-teams.dialog';
import { ConfirmationDialog } from './dialogs/confirmation.dialog/confirmation.dialog';
// tslint:disable-next-line:max-line-length
import { LeagueDraftStatusPanelComponent } from './components/league-draft-status-panel/league-draft-status-panel.component';
import { RemoveDrafterDialogComponent } from './dialogs/remove-drafter/remove-drafter.dialog';
import { TeamInfoDialog } from './dialogs/team-info.dialog/team-info.dialog';
import { LeagueTeamLink } from './links/league-team.link/league-team.link';
import { TeamPointsSummaryComponent } from './components/team-points-summary/team-points-summary.component';

@NgModule({
  imports: [
    SharedModule,
    BrowserModule,
    MaterialModule,
    FormsModule,
    LeagueRoutingModule,
    ReactiveFormsModule,
    DragulaModule,
    GameElementsModule,
    StoreModule.forFeature('leagues', wrestlingStore.reducer),
    EffectsModule.forFeature([DraftEffects, UserEffects, LeaguesEffects])
  ],
  declarations: [
    LeagueBaseComponent,
    LeagueIntroComponent,
    AllSelectionsComponent,
    WrestlerSelectComponent,
    ConfirmDialog,
    WeightListComponent,
    LeagueListComponent,
    LeagueCardComponent,
    UserGraphicComponent,
    LeagueHomeComponent,
    CreateLeagueDialogComponent,
    LeagueStandingsComponent,
    LeagueResultsComponent,
    LeagueDraftComponent,
    LeagueTeamComponent,
    LeagueDraftResultsComponent,
    LeagueBracketsComponent,
    LeagueAllTeamsComponent,
    TeamDraftSummaryComponent,
    LeagueSearchComponent,
    AddPlayerToDraftComponent,
    ViewParticipantsDialogComponent,
    ReorderDraftDialogComponent,
    LeagueBracketSummaryComponent,
    LeagueWrestlerLinkComponent,
    LeagueMyWrestlerComponent,
    LeagueBracketDialogComponent,
    LeagueBracketComponent,
    LeagueMatchupComponent,
    AllTeamsDialogComponent,
    LeagueDraftStatusPanelComponent,
    ConfirmationDialog,
    RemoveDrafterDialogComponent,
    TeamInfoDialog,
    LeagueTeamLink,
    TeamPointsSummaryComponent,
    // PIPES
    ...pipes.PIPES
  ],
  entryComponents: [
    AddPlayerToDraftComponent,
    ReorderDraftDialogComponent,
    CreateLeagueDialogComponent,
    AllSelectionsComponent,
    AllTeamsDialogComponent,
    WrestlerSelectComponent,
    ConfirmDialog,
    ConfirmationDialog,
    ViewParticipantsDialogComponent,
    LeagueBracketDialogComponent,
    RemoveDrafterDialogComponent,
    TeamInfoDialog
  ],
  providers: [
    { provide: WrestlingService, useClass: WrestlingService },
    { provide: UserService, useClass: UserService }
  ]
})

export class LeagueModule { }
