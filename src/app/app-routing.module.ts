import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeagueLoginComponent } from './pages/login/league-login.component';
import { extract } from './core/i18n.service';

const routes: Routes = [
  // Fallback when no prior route is matched
  { path: '**', redirectTo: '', pathMatch: 'full' },
  {
    path: 'login',
    component: LeagueLoginComponent,
    data: { title: extract('Login') }
    // canActivate: [AuthOutGuardService]
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
